# Contribution guidelines for BayesValidRox
Thank you for investing your time in contributing to our project!
In this guide you will get an overview of the contribution workflow from opening an issue, creating a PR, reviewing, and merging the PR.

To get an overview of the project you can read the [README](https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/blob/master/README.md?ref_type=heads) file and have a look at the BayesValidRox website.

## Issues
If you know what you’re going to work on, see if an issue exists. 
If a related issue doesn't exist, you can open a new issue using a relevant issue form.

Scan through existing issues to find one that interests you. You can narrow down the search using labels as filters. 
When you find an issue, leave a comment on the issue you want to work on. 
This helps to let others know that you will be working on that issue.

## Testing and Examples
Any changes that you make while working on your chosen issue should be reflected in the [tests](https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/tree/master/tests?ref_type=heads).
This includes updating the tests that are affected by changes in the source code, as well as adding tests for new features.

The same holds true for the [examples](https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/tree/master/examples?ref_type=heads).
If you are adding new functionalities and features, a new example should be added that shows how these features are used.


## Submitting changes
Please send a GitHub Pull Request with a clear list of what you've done. 
When you send a pull request, it would be great if you include tests and examples that reflect your changes. 
We can always use more test coverage. 
Please follow our coding conventions (below) and make sure all of your commits are atomic (one feature per commit).

Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should look like this:

$ git commit -m "A brief summary of the commit
> 
> A paragraph describing what changed and its impact."


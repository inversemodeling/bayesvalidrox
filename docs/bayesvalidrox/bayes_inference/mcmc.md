Module bayesvalidrox.bayes_inference.mcmc
=========================================

Classes
-------

`MCMC(BayesOpts)`
:   A class for bayesian inference via a Markov-Chain Monte-Carlo (MCMC)
    Sampler to approximate the posterior distribution of the Bayes theorem:
    $$p(\theta|\mathcal{y}) = \frac{p(\mathcal{y}|\theta) p(\theta)}
                                         {p(\mathcal{y})}.$$
    
    This class make inference with emcee package [1] using an Affine Invariant
    Ensemble sampler (AIES) [2].
    
    [1] Foreman-Mackey, D., Hogg, D.W., Lang, D. and Goodman, J., 2013.emcee:
        the MCMC hammer. Publications of the Astronomical Society of the
        Pacific, 125(925), p.306. https://emcee.readthedocs.io/en/stable/
    
    [2] Goodman, J. and Weare, J., 2010. Ensemble samplers with affine
        invariance. Communications in applied mathematics and computational
        science, 5(1), pp.65-80.
    
    
    Attributes
    ----------
    BayesOpts : obj
        Bayes object.

    ### Methods

    `run_sampler(self, observation, total_sigma2)`
    :

    `log_prior(self, theta)`
    :   Calculates the log prior likelihood \( p(\theta)\) for the given
        parameter set(s) \( \theta \).
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter sets, i.e. proposals of MCMC chains.
        
        Returns
        -------
        logprior: float or array of shape n_samples
            Log prior likelihood. If theta has only one row, a single value is
            returned otherwise an array.

    `log_likelihood(self, theta)`
    :   Computes likelihood \( p(\mathcal{Y}|\theta)\) of the performance
        of the (meta-)model in reproducing the observation data.
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter set, i.e. proposals of the MCMC chains.
        
        Returns
        -------
        log_like : array of shape (n_samples)
            Log likelihood.

    `log_posterior(self, theta)`
    :   Computes the posterior likelihood \(p(\theta| \mathcal{Y})\) for
        the given parameterset.
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter set, i.e. proposals of the MCMC chains.
        
        Returns
        -------
        log_like : array of shape (n_samples)
            Log posterior likelihood.

    `eval_model(self, theta)`
    :   Evaluates the (meta-) model at the given theta.
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter set, i.e. proposals of the MCMC chains.
        
        Returns
        -------
        mean_pred : dict
            Mean model prediction.
        std_pred : dict
            Std of model prediction.

    `train_error_model(self, sampler)`
    :   Trains an error model using a Gaussian Process Regression.
        
        Parameters
        ----------
        sampler : obj
            emcee sampler.
        
        Returns
        -------
        error_MetaModel : obj
            A error model.

    `gelman_rubin(self, chain, return_var=False)`
    :   The potential scale reduction factor (PSRF) defined by the variance
        within one chain, W, with the variance between chains B.
        Both variances are combined in a weighted sum to obtain an estimate of
        the variance of a parameter \( \theta \).The square root of the
        ratio of this estimates variance to the within chain variance is called
        the potential scale reduction.
        For a well converged chain it should approach 1. Values greater than
        1.1 typically indicate that the chains have not yet fully converged.
        
        Source: http://joergdietrich.github.io/emcee-convergence.html
        
        https://github.com/jwalton3141/jwalton3141.github.io/blob/master/assets/posts/ESS/rwmh.py
        
        Parameters
        ----------
        chain : array (n_walkers, n_steps, n_params)
            The emcee ensamples.
        
        Returns
        -------
        R_hat : float
            The Gelman-Robin values.

    `marginal_llk_emcee(self, sampler, nburn=None, logp=None, maxiter=1000)`
    :   The Bridge Sampling Estimator of the Marginal Likelihood based on
        https://gist.github.com/junpenglao/4d2669d69ddfe1d788318264cdcf0583
        
        Parameters
        ----------
        sampler : TYPE
            MultiTrace, result of MCMC run.
        nburn : int, optional
            Number of burn-in step. The default is None.
        logp : TYPE, optional
            Model Log-probability function. The default is None.
        maxiter : int, optional
            Maximum number of iterations. The default is 1000.
        
        Returns
        -------
        marg_llk : dict
            Estimated Marginal log-Likelihood.
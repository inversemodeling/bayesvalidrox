Module bayesvalidrox.bayes_inference
====================================

Sub-modules
-----------
* bayesvalidrox.bayes_inference.bayes_inference
* bayesvalidrox.bayes_inference.discrepancy
* bayesvalidrox.bayes_inference.mcmc

Classes
-------

`BayesInference(MetaModel, discrepancy=None, emulator=True, name='Calib', bootstrap=False, req_outputs=None, selected_indices=None, samples=None, n_samples=500000, measured_data=None, inference_method='rejection', mcmc_params=None, bayes_loocv=False, n_bootstrap_itrs=1, perturbed_data=[], bootstrap_noise=0.05, plot_post_pred=True, plot_map_pred=False, max_a_posteriori='mean', corner_title_fmt='.3e')`
:   A class to perform Bayesian Analysis.
    
    
    Attributes
    ----------
    MetaModel : obj
        Meta model object.
    discrepancy : obj
        The discrepancy object for the sigma2s, i.e. the diagonal entries
        of the variance matrix for a multivariate normal likelihood.
    name : str, optional
        The type of analysis, either calibration (`Calib`) or validation
        (`Valid`). The default is `'Calib'`.
    emulator : bool, optional
        Analysis with emulator (MetaModel). The default is `True`.
    bootstrap : bool, optional
        Bootstrap the analysis. The default is `False`.
    req_outputs : list, optional
        The list of requested output to be used for the analysis.
        The default is `None`. If None, all the defined outputs for the model
        object is used.
    selected_indices : dict, optional
        A dictionary with the selected indices of each model output. The
        default is `None`. If `None`, all measurement points are used in the
        analysis.
    samples : array of shape (n_samples, n_params), optional
        The samples to be used in the analysis. The default is `None`. If
        None the samples are drawn from the probablistic input parameter
        object of the MetaModel object.
    n_samples : int, optional
        Number of samples to be used in the analysis. The default is `500000`.
        If samples is not `None`, this argument will be assigned based on the
        number of samples given.
    measured_data : dict, optional
        A dictionary containing the observation data. The default is `None`.
        if `None`, the observation defined in the Model object of the
        MetaModel is used.
    inference_method : str, optional
        A method for approximating the posterior distribution in the Bayesian
        inference step. The default is `'rejection'`, which stands for
        rejection sampling. A Markov Chain Monte Carlo sampler can be simply
        selected by passing `'MCMC'`.
    mcmc_params : dict, optional
        A dictionary with args required for the Bayesian inference with
        `MCMC`. The default is `None`.
    
        Pass the mcmc_params like the following:
    
            >>> mcmc_params:{
                'init_samples': None,  # initial samples
                'n_walkers': 100,  # number of walkers (chain)
                'n_steps': 100000,  # number of maximum steps
                'n_burn': 200,  # number of burn-in steps
                'moves': None,  # Moves for the emcee sampler
                'multiprocessing': False,  # multiprocessing
                'verbose': False # verbosity
                }
        The items shown above are the default values. If any parmeter is
        not defined, the default value will be assigned to it.
    bayes_loocv : bool, optional
        Bayesian Leave-one-out Cross Validation. The default is `False`. If
        `True`, the LOOCV procedure is used to estimate the bayesian Model
        Evidence (BME).
    n_bootstrap_itrs : int, optional
        Number of bootstrap iteration. The default is `1`. If bayes_loocv is
        `True`, this is qualt to the total length of the observation data
        set.
    perturbed_data : array of shape (n_bootstrap_itrs, n_obs), optional
        User defined perturbed data. The default is `[]`.
    bootstrap_noise : float, optional
        A noise level to perturb the data set. The default is `0.05`.
    plot_post_pred : bool, optional
        Plot posterior predictive plots. The default is `True`.
    plot_map_pred : bool, optional
        Plot the model outputs vs the metamodel predictions for the maximum
        a posteriori (defined as `max_a_posteriori`) parameter set. The
        default is `False`.
    max_a_posteriori : str, optional
        Maximum a posteriori. `'mean'` and `'mode'` are available. The default
        is `'mean'`.
    corner_title_fmt : str, optional
        Title format for the posterior distribution plot with python
        package `corner`. The default is `'.3e'`.

    ### Methods

    `create_inference(self)`
    :   Starts the inference.
        
        Returns
        -------
        BayesInference : obj
            The Bayes inference object.

    `normpdf(self, outputs, obs_data, total_sigma2s, sigma2=None, std=None)`
    :   Calculates the likelihood of simulation outputs compared with
        observation data.
        
        Parameters
        ----------
        outputs : dict
            A dictionary containing the simulation outputs as array of shape
            (n_samples, n_measurement) for each model output.
        obs_data : dict
            A dictionary/dataframe containing the observation data.
        total_sigma2s : dict
            A dictionary with known values of the covariance diagonal entries,
            a.k.a sigma^2.
        sigma2 : array, optional
            An array of the sigma^2 samples, when the covariance diagonal
            entries are unknown and are being jointly inferred. The default is
            None.
        std : dict, optional
            A dictionary containing the root mean squared error as array of
            shape (n_samples, n_measurement) for each model output. The default
            is None.
        
        Returns
        -------
        logLik : array of shape (n_samples)
            Likelihoods.

`MCMC(BayesOpts)`
:   A class for bayesian inference via a Markov-Chain Monte-Carlo (MCMC)
    Sampler to approximate the posterior distribution of the Bayes theorem:
    $$p(\theta|\mathcal{y}) = \frac{p(\mathcal{y}|\theta) p(\theta)}
                                         {p(\mathcal{y})}.$$
    
    This class make inference with emcee package [1] using an Affine Invariant
    Ensemble sampler (AIES) [2].
    
    [1] Foreman-Mackey, D., Hogg, D.W., Lang, D. and Goodman, J., 2013.emcee:
        the MCMC hammer. Publications of the Astronomical Society of the
        Pacific, 125(925), p.306. https://emcee.readthedocs.io/en/stable/
    
    [2] Goodman, J. and Weare, J., 2010. Ensemble samplers with affine
        invariance. Communications in applied mathematics and computational
        science, 5(1), pp.65-80.
    
    
    Attributes
    ----------
    BayesOpts : obj
        Bayes object.

    ### Methods

    `run_sampler(self, observation, total_sigma2)`
    :

    `log_prior(self, theta)`
    :   Calculates the log prior likelihood \( p(\theta)\) for the given
        parameter set(s) \( \theta \).
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter sets, i.e. proposals of MCMC chains.
        
        Returns
        -------
        logprior: float or array of shape n_samples
            Log prior likelihood. If theta has only one row, a single value is
            returned otherwise an array.

    `log_likelihood(self, theta)`
    :   Computes likelihood \( p(\mathcal{Y}|\theta)\) of the performance
        of the (meta-)model in reproducing the observation data.
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter set, i.e. proposals of the MCMC chains.
        
        Returns
        -------
        log_like : array of shape (n_samples)
            Log likelihood.

    `log_posterior(self, theta)`
    :   Computes the posterior likelihood \(p(\theta| \mathcal{Y})\) for
        the given parameterset.
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter set, i.e. proposals of the MCMC chains.
        
        Returns
        -------
        log_like : array of shape (n_samples)
            Log posterior likelihood.

    `eval_model(self, theta)`
    :   Evaluates the (meta-) model at the given theta.
        
        Parameters
        ----------
        theta : array of shape (n_samples, n_params)
            Parameter set, i.e. proposals of the MCMC chains.
        
        Returns
        -------
        mean_pred : dict
            Mean model prediction.
        std_pred : dict
            Std of model prediction.

    `train_error_model(self, sampler)`
    :   Trains an error model using a Gaussian Process Regression.
        
        Parameters
        ----------
        sampler : obj
            emcee sampler.
        
        Returns
        -------
        error_MetaModel : obj
            A error model.

    `gelman_rubin(self, chain, return_var=False)`
    :   The potential scale reduction factor (PSRF) defined by the variance
        within one chain, W, with the variance between chains B.
        Both variances are combined in a weighted sum to obtain an estimate of
        the variance of a parameter \( \theta \).The square root of the
        ratio of this estimates variance to the within chain variance is called
        the potential scale reduction.
        For a well converged chain it should approach 1. Values greater than
        1.1 typically indicate that the chains have not yet fully converged.
        
        Source: http://joergdietrich.github.io/emcee-convergence.html
        
        https://github.com/jwalton3141/jwalton3141.github.io/blob/master/assets/posts/ESS/rwmh.py
        
        Parameters
        ----------
        chain : array (n_walkers, n_steps, n_params)
            The emcee ensamples.
        
        Returns
        -------
        R_hat : float
            The Gelman-Robin values.

    `marginal_llk_emcee(self, sampler, nburn=None, logp=None, maxiter=1000)`
    :   The Bridge Sampling Estimator of the Marginal Likelihood based on
        https://gist.github.com/junpenglao/4d2669d69ddfe1d788318264cdcf0583
        
        Parameters
        ----------
        sampler : TYPE
            MultiTrace, result of MCMC run.
        nburn : int, optional
            Number of burn-in step. The default is None.
        logp : TYPE, optional
            Model Log-probability function. The default is None.
        maxiter : int, optional
            Maximum number of iterations. The default is 1000.
        
        Returns
        -------
        marg_llk : dict
            Estimated Marginal log-Likelihood.
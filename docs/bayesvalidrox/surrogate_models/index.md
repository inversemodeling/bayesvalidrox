Module bayesvalidrox.surrogate_models
=====================================

Sub-modules
-----------
* bayesvalidrox.surrogate_models.apoly_construction
* bayesvalidrox.surrogate_models.bayes_linear
* bayesvalidrox.surrogate_models.eval_rec_rule
* bayesvalidrox.surrogate_models.exp_designs
* bayesvalidrox.surrogate_models.exploration
* bayesvalidrox.surrogate_models.glexindex
* bayesvalidrox.surrogate_models.inputs
* bayesvalidrox.surrogate_models.reg_fast_ard
* bayesvalidrox.surrogate_models.reg_fast_laplace
* bayesvalidrox.surrogate_models.sequential_design
* bayesvalidrox.surrogate_models.surrogate_models

Classes
-------

`MetaModel(input_obj, meta_model_type='PCE', pce_reg_method='OLS', pce_deg=1, pce_q_norm=1.0, dim_red_method='no', verbose=False)`
:   Meta (surrogate) model
    
    This class trains a surrogate model. It accepts an input object (input_obj)
    containing the specification of the distributions for uncertain parameters
    and a model object with instructions on how to run the computational model.
    
    Attributes
    ----------
    input_obj : obj
        Input object with the information on the model input parameters.
    meta_model_type : str
        Surrogate model types. Three surrogate model types are supported:
        polynomial chaos expansion (`PCE`), arbitrary PCE (`aPCE`) and
        Gaussian process regression (`GPE`). Default is PCE.
    pce_reg_method : str
        PCE regression method to compute the coefficients. The following
        regression methods are available:
    
        1. OLS: Ordinary Least Square method
        2. BRR: Bayesian Ridge Regression
        3. LARS: Least angle regression
        4. ARD: Bayesian ARD Regression
        5. FastARD: Fast Bayesian ARD Regression
        6. VBL: Variational Bayesian Learning
        7. EBL: Emperical Bayesian Learning
        Default is `OLS`.
    pce_deg : int or list of int
        Polynomial degree(s). If a list is given, an adaptive algorithm is used
        to find the best degree with the lowest Leave-One-Out cross-validation
        (LOO) error (or the highest score=1-LOO). Default is `1`.
    pce_q_norm : float
        Hyperbolic (or q-norm) truncation for multi-indices of multivariate
        polynomials. Default is `1.0`.
    dim_red_method : str
        Dimensionality reduction method for the output space. The available
        method is based on principal component analysis (PCA). The Default is
        `'no'`. There are two ways to select number of components: use
        percentage of the explainable variance threshold (between 0 and 100)
        (Option A) or direct prescription of components' number (Option B):
    
            >>> MetaModelOpts.dim_red_method = 'PCA'
            >>> MetaModelOpts.var_pca_threshold = 99.999  # Option A
            >>> MetaModelOpts.n_pca_components = 12 # Option B
    
    verbose : bool
        Prints summary of the regression results. Default is `False`.
    
    Note
    -------
    To define the sampling methods and the training set, an experimental design
    instance shall be defined. This can be done by:
    
    >>> MetaModelOpts.add_ExpDesign()
    
    Two experimental design schemes are supported: one-shot (`normal`) and
    adaptive sequential (`sequential`) designs.
    For experimental design refer to `ExpDesigns`.

    ### Class variables

    `auto_vivification`
    :   Implementation of perl's AutoVivification feature.
        
        Source: https://stackoverflow.com/a/651879/18082457

    ### Methods

    `create_metamodel(self, Model)`
    :   Starts the training of the meta-model for the model objects containg
         the given computational model.
        
        Parameters
        ----------
        Model : obj
            Model object.
        
        Returns
        -------
        metamodel : obj
            The meta model object.

    `train_norm_design(self, Model, verbose=False)`
    :   This function loops over the outputs and each time step/point and fits
        the meta model.
        
        Parameters
        ----------
        Model : obj
            Model object.
        verbose : bool, optional
            Flag for a sequential design in silent mode. The default is False.
        
        Returns
        -------
        self: obj
            Meta-model object.

    `create_basis_indices(self, degree, q_norm)`
    :   Creates set of selected multi-indices of multivariate polynomials for
        certain parameter numbers, polynomial degree, hyperbolic (or q-norm)
        truncation scheme.
        
        Parameters
        ----------
        degree : int
            Polynomial degree.
        q_norm : float
            hyperbolic (or q-norm) truncation.
        
        Returns
        -------
        basis_indices : array of shape (n_terms, n_params)
            Multi-indices of multivariate polynomials.

    `add_ExpDesign(self)`
    :   Instanciates experimental design object.
        
        Returns
        -------
        None.

    `generate_ExpDesign(self, Model)`
    :   Prepares the experimental design either by reading from the prescribed
        data or running simulations.
        
        Parameters
        ----------
        Model : obj
            Model object.
        
        Raises
        ------
        Exception
            If model sumulations are not provided properly.
        
        Returns
        -------
        ED_X_tr: array of shape (n_samples, n_params)
            Training samples transformed by an isoprobabilistic transformation.
        ED_Y: dict
            Model simulations (target) for all outputs.

    `univ_basis_vals(self, samples, n_max=None)`
    :   Evaluates univariate regressors along input directions.
        
        Parameters
        ----------
        samples : array of shape (n_samples, n_params)
            Samples.
        n_max : int, optional
            Maximum polynomial degree. The default is `None`.
        
        Returns
        -------
        univ_basis: array of shape (n_samples, n_params, n_max+1)
            All univariate regressors up to n_max.

    `create_psi(self, basis_indices, univ_p_val)`
    :   This function assemble the design matrix Psi from the given basis index
        set INDICES and the univariate polynomial evaluations univ_p_val.
        
        Parameters
        ----------
        basis_indices : array of shape (n_terms, n_params)
            Multi-indices of multivariate polynomials.
        univ_p_val : array of (n_samples, n_params, n_max+1)
            All univariate regressors up to `n_max`.
        
        Raises
        ------
        ValueError
            n_terms in arguments do not match.
        
        Returns
        -------
        psi : array of shape (n_samples, n_terms)
            Multivariate regressors.

    `fit(self, X, y, basis_indices, reg_method=None)`
    :   Fit regression using the regression method provided.
        
        Parameters
        ----------
        X : array of shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples and
            n_features is the number of features.
        y : array of shape (n_samples,)
            Target values.
        basis_indices : array of shape (n_terms, n_params)
            Multi-indices of multivariate polynomials.
        reg_method : str, optional
            DESCRIPTION. The default is None.
        
        Returns
        -------
        return_out_dict : Dict
            Fitted estimator, spareMulti-Index, sparseX and coefficients.

    `adaptive_regression(self, ED_X, ED_Y, varIdx, verbose=False)`
    :   Adaptively fits the PCE model by comparing the scores of different
        degrees and q-norm.
        
        Parameters
        ----------
        ED_X : array of shape (n_samples, n_params)
            Experimental design.
        ED_Y : array of shape (n_samples,)
            Target values, i.e. simulation results for the Experimental design.
        varIdx : int
            Index of the output.
        verbose : bool, optional
            Print out summary. The default is False.
        
        Returns
        -------
        returnVars : Dict
            Fitted estimator, best degree, best q-norm, LOOCVScore and
            coefficients.

    `corr_loocv_error(self, clf, psi, coeffs, y)`
    :   Calculates the corrected LOO error for regression on regressor
        matrix `psi` that generated the coefficients based on [1] and [2].
        
        [1] Blatman, G., 2009. Adaptive sparse polynomial chaos expansions for
            uncertainty propagation and sensitivity analysis (Doctoral
            dissertation, Clermont-Ferrand 2).
        
        [2] Blatman, G. and Sudret, B., 2011. Adaptive sparse polynomial chaos
            expansion based on least angle regression. Journal of computational
            Physics, 230(6), pp.2345-2367.
        
        Parameters
        ----------
        clf : object
            Fitted estimator.
        psi : array of shape (n_samples, n_features)
            The multivariate orthogonal polynomials (regressor).
        coeffs : array-like of shape (n_features,)
            Estimated cofficients.
        y : array of shape (n_samples,)
            Target values.
        
        Returns
        -------
        Q_2 : float
            LOOCV Validation score (1-LOOCV erro).
        residual : array of shape (n_samples,)
            Residual values (y - predicted targets).

    `pca_transformation(self, Output)`
    :   Transforms the targets (outputs) via Principal Component Analysis
        
        Parameters
        ----------
        Output : array of shape (n_samples,)
            Target values.
        
        Returns
        -------
        pca : obj
            Fitted sklearnPCA object.
        OutputMatrix : array of shape (n_samples,)
            Transformed target values.

    `gaussian_process_emulator(self, X, y, nug_term=None, autoSelect=False, varIdx=None)`
    :   Fits a Gaussian Process Emulator to the target given the training
         points.
        
        Parameters
        ----------
        X : array of shape (n_samples, n_params)
            Training points.
        y : array of shape (n_samples,)
            Target values.
        nug_term : float, optional
            Nugget term. The default is None, i.e. variance of y.
        autoSelect : bool, optional
            Loop over some kernels and select the best. The default is False.
        varIdx : int, optional
            The index number. The default is None.
        
        Returns
        -------
        gp : object
            Fitted estimator.

    `eval_metamodel(self, samples=None, nsamples=None, sampling_method='random', return_samples=False)`
    :   Evaluates meta-model at the requested samples. One can also generate
        nsamples.
        
        Parameters
        ----------
        samples : array of shape (n_samples, n_params), optional
            Samples to evaluate meta-model at. The default is None.
        nsamples : int, optional
            Number of samples to generate, if no `samples` is provided. The
            default is None.
        sampling_method : str, optional
            Type of sampling, if no `samples` is provided. The default is
            'random'.
        return_samples : bool, optional
            Retun samples, if no `samples` is provided. The default is False.
        
        Returns
        -------
        mean_pred : dict
            Mean of the predictions.
        std_pred : dict
            Standard deviatioon of the predictions.

    `create_model_error(self, X, y, name='Calib')`
    :   Fits a GPE-based model error.
        
        Parameters
        ----------
        X : array of shape (n_outputs, n_inputs)
            Input array. It can contain any forcing inputs or coordinates of
             extracted data.
        y : array of shape (n_outputs,)
            The model response for the MAP parameter set.
        name : str, optional
            Calibration or validation. The default is `'Calib'`.
        
        Returns
        -------
        self: object
            Self object.

    `eval_model_error(self, X, y_pred)`
    :   Evaluates the error model.
        
        Parameters
        ----------
        X : array
            Inputs.
        y_pred : dict
            Predictions.
        
        Returns
        -------
        mean_pred : dict
            Mean predition of the GPE-based error model.
        std_pred : dict
            standard deviation of the GPE-based error model.
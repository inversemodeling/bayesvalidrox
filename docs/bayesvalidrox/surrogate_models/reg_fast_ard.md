Module bayesvalidrox.surrogate_models.reg_fast_ard
==================================================
Created on Tue Mar 24 19:41:45 2020

@author: farid

Functions
---------

    
`update_precisions(Q, S, q, s, A, active, tol, n_samples, clf_bias)`
:   Selects one feature to be added/recomputed/deleted to model based on 
    effect it will have on value of log marginal likelihood.

Classes
-------

`RegressionFastARD(n_iter=300, start=None, tol=0.001, fit_intercept=True, normalize=False, copy_X=True, compute_score=False, verbose=False)`
:   Regression with Automatic Relevance Determination (Fast Version uses 
    Sparse Bayesian Learning)
    https://github.com/AmazaspShumik/sklearn-bayes/blob/master/skbayes/rvm_ard_models/fast_rvm.py
    
    Parameters
    ----------
    n_iter: int, optional (DEFAULT = 100)
        Maximum number of iterations
    
    start: list, optional (DEFAULT = None)
        Initial selected features.
    
    tol: float, optional (DEFAULT = 1e-3)
        If absolute change in precision parameter for weights is below threshold
        algorithm terminates.
    
    fit_intercept : boolean, optional (DEFAULT = True)
        whether to calculate the intercept for this model. If set
        to false, no intercept will be used in calculations
        (e.g. data is expected to be already centered).
    
    copy_X : boolean, optional (DEFAULT = True)
        If True, X will be copied; else, it may be overwritten.
    
    compute_score : bool, default=False
        If True, compute the log marginal likelihood at each iteration of the
        optimization.
    
    verbose : boolean, optional (DEFAULT = FALSE)
        Verbose mode when fitting the model
    
    Attributes
    ----------
    coef_ : array, shape = (n_features)
        Coefficients of the regression model (mean of posterior distribution)
    
    alpha_ : float
       estimated precision of the noise
    
    active_ : array, dtype = np.bool, shape = (n_features)
       True for non-zero coefficients, False otherwise
    
    lambda_ : array, shape = (n_features)
       estimated precisions of the coefficients
    
    sigma_ : array, shape = (n_features, n_features)
        estimated covariance matrix of the weights, computed only
        for non-zero coefficients
    
    scores_ : array-like of shape (n_iter_+1,)
        If computed_score is True, value of the log marginal likelihood (to be
        maximized) at each iteration of the optimization.
    
    References
    ----------
    [1] Fast marginal likelihood maximisation for sparse Bayesian models
    (Tipping & Faul 2003) (http://www.miketipping.com/papers/met-fastsbl.pdf)
    [2] Analysis of sparse Bayesian learning (Tipping & Faul 2001)
        (http://www.miketipping.com/abstracts.htm#Faul:NIPS01)

    ### Ancestors (in MRO)

    * sklearn.linear_model._base.LinearModel
    * sklearn.base.BaseEstimator
    * sklearn.base.RegressorMixin

    ### Methods

    `fit(self, X, y)`
    :   Fits ARD Regression with Sequential Sparse Bayes Algorithm.
        
        Parameters
        -----------
        X: {array-like, sparse matrix} of size (n_samples, n_features)
           Training data, matrix of explanatory variables
        
        y: array-like of size [n_samples, n_features]
           Target values
        
        Returns
        -------
        self : object
            Returns self.

    `log_marginal_like(self, XXa, XYa, Aa, beta)`
    :   Computes the log of the marginal likelihood.

    `predict(self, X, return_std=False)`
    :   Computes predictive distribution for test set.
        Predictive distribution for each data point is one dimensional
        Gaussian and therefore is characterised by mean and variance based on
        Ref.[1] Section 3.3.2.
        
        Parameters
        -----------
        X: {array-like, sparse} (n_samples_test, n_features)
           Test data, matrix of explanatory variables
        
        Returns
        -------
        : list of length two [y_hat, var_hat]
        
             y_hat: numpy array of size (n_samples_test,)
                    Estimated values of targets on test set (i.e. mean of
                    predictive distribution)
        
                var_hat: numpy array of size (n_samples_test,)
                    Variance of predictive distribution
        References
        ----------
        [1] Bishop, C. M. (2006). Pattern recognition and machine learning.
        springer.
Module bayesvalidrox.surrogate_models.apoly_construction
========================================================

Functions
---------

    
`apoly_construction(Data, degree)`
:   Construction of Data-driven Orthonormal Polynomial Basis
    Author: Dr.-Ing. habil. Sergey Oladyshkin
    Department of Stochastic Simulation and Safety Research for Hydrosystems
    Institute for Modelling Hydraulic and Environmental Systems
    Universitaet Stuttgart, Pfaffenwaldring 5a, 70569 Stuttgart
    E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de
    http://www.iws-ls3.uni-stuttgart.de
    The current script is based on definition of arbitrary polynomial chaos
    expansion (aPC), which is presented in the following manuscript:
    Oladyshkin, S. and W. Nowak. Data-driven uncertainty quantification using
    the arbitrary polynomial chaos expansion. Reliability Engineering & System
    Safety, Elsevier, V. 106, P.  179-190, 2012.
    DOI: 10.1016/j.ress.2012.05.002.
    
    Parameters
    ----------
    Data : array
        Raw data.
    degree : int
        Maximum polynomial degree.
    
    Returns
    -------
    Polynomial : array
        The coefficients of the univariate orthonormal polynomials.
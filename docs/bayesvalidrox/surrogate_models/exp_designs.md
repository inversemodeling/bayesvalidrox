Module bayesvalidrox.surrogate_models.exp_designs
=================================================

Classes
-------

`ExpDesigns(Input, method='normal', meta_Model='pce', sampling_method='random', hdf5_file=None, n_new_samples=1, n_max_samples=None, mod_LOO_threshold=1e-16, tradeoff_scheme=None, n_canddidate=1, explore_method='random', exploit_method='Space-filling', util_func='Space-filling', n_cand_groups=4, n_replication=1, post_snapshot=False, step_snapshot=1, max_a_post=[], adapt_verbose=False)`
:   This class generates samples from the prescribed marginals for the model
    parameters using the `Input` object.
    
    Attributes
    ----------
    Input : obj
        Input object containing the parameter marginals, i.e. name,
        distribution type and distribution parameters or available raw data.
    method : str
        Type of the experimental design. The default is `'normal'`. Other
        option is `'sequential'`.
    meta_Model : str
        Type of the meta_model.
    sampling_method : str
        Name of the sampling method for the experimental design. The following
        sampling method are supported:
    
        * random
        * latin_hypercube
        * sobol
        * halton
        * hammersley
        * korobov
        * chebyshev(FT)
        * grid(FT)
        * nested_grid(FT)
        * user
    hdf5_file : str
        Name of the hdf5 file that contains the experimental design.
    n_new_samples : int
        Number of (initial) training points.
    n_max_samples : int
        Number of maximum training points.
    mod_LOO_threshold : float
        The modified leave-one-out cross validation threshold where the
        sequential design stops.
    tradeoff_scheme : str
        Trade-off scheme to assign weights to the exploration and exploitation
        scores in the sequential design.
    n_canddidate : int
        Number of candidate training sets to calculate the scores for.
    explore_method : str
        Type of the exploration method for the sequential design. The following
        methods are supported:
    
        * Voronoi
        * random
        * latin_hypercube
        * LOOCV
        * dual annealing
    exploit_method : str
        Type of the exploitation method for the sequential design. The
        following methods are supported:
    
        * BayesOptDesign
        * BayesActDesign
        * VarOptDesign
        * alphabetic
        * Space-filling
    util_func : str or list
        The utility function to be specified for the `exploit_method`. For the
        available utility functions see Note section.
    n_cand_groups : int
        Number of candidate groups. Each group of candidate training sets will
        be evaulated separately in parallel.
    n_replication : int
        Number of replications. Only for comparison. The default is 1.
    post_snapshot : int
        Whether to plot the posterior in the sequential design. The default is
        `True`.
    step_snapshot : int
        The number of steps to plot the posterior in the sequential design. The
        default is 1.
    max_a_post : list or array
        Maximum a posteriori of the posterior distribution, if known. The
        default is `[]`.
    adapt_verbose : bool
        Whether to plot the model response vs that of metamodel for the new
        trining point in the sequential design.
    
    Note
    ----------
    The following utiliy functions for the **exploitation** methods are
    supported:
    
    #### BayesOptDesign (when data is available)
    - DKL (Kullback-Leibler Divergence)
    - DPP (D-Posterior-percision)
    - APP (A-Posterior-percision)
    
    #### VarBasedOptDesign -> when data is not available
    - Entropy (Entropy/MMSE/active learning)
    - EIGF (Expected Improvement for Global fit)
    - LOOCV (Leave-one-out Cross Validation)
    
    #### alphabetic
    - D-Opt (D-Optimality)
    - A-Opt (A-Optimality)
    - K-Opt (K-Optimality)

    ### Methods

    `generate_samples(self, n_samples, sampling_method='random', transform=False)`
    :   Generates samples with given sampling method
        
        Parameters
        ----------
        n_samples : int
            Number of requested samples.
        sampling_method : str, optional
            Sampling method. The default is `'random'`.
        transform : bool, optional
            Transformation via an isoprobabilistic transformation method. The
            default is `False`.
        
        Returns
        -------
        samples: array of shape (n_samples, n_params)
            Generated samples from defined model input object.

    `generate_ED(self, n_samples, sampling_method='random', transform=False, max_pce_deg=None)`
    :   Generates experimental designs (training set) with the given method.
        
        Parameters
        ----------
        n_samples : int
            Number of requested training points.
        sampling_method : str, optional
            Sampling method. The default is `'random'`.
        transform : bool, optional
            Isoprobabilistic transformation. The default is `False`.
        max_pce_deg : int, optional
            Maximum PCE polynomial degree. The default is `None`.
        
        Returns
        -------
        samples : array of shape (n_samples, n_params)
            Selected training samples.

    `init_param_space(self, max_deg=None)`
    :   Initializes parameter space.
        
        Parameters
        ----------
        max_deg : int, optional
            Maximum degree. The default is `None`.
        
        Returns
        -------
        raw_data : array of shape (n_params, n_samples)
            Raw data.
        bound_tuples : list of tuples
            A list containing lower and upper bounds of parameters.

    `build_dist(self, rosenblatt)`
    :   Creates the polynomial types to be passed to univ_basis_vals method of
        the MetaModel object.
        
        Parameters
        ----------
        rosenblatt : bool
            Rosenblatt transformation flag.
        
        Returns
        -------
        orig_space_dist : object
            A chaospy JDist object or a gaussian_kde object.
        poly_types : list
            List of polynomial types for the parameters.

    `random_sampler(self, n_samples)`
    :   Samples the given raw data randomly.
        
        Parameters
        ----------
        n_samples : int
            Number of requested samples.
        
        Returns
        -------
        samples: array of shape (n_samples, n_params)
            The sampling locations in the input space.

    `pcm_sampler(self, max_deg)`
    :   Generates collocation points based on the root of the polynomial
        degrees.
        
        Parameters
        ----------
        max_deg : int
            Maximum degree defined by user.
        
        Returns
        -------
        opt_col_points: array of shape (n_samples, n_params)
            Collocation points.

    `transform(self, X, params=None)`
    :   Transform the samples via either a Rosenblatt or an isoprobabilistic
        transformation.
        
        Parameters
        ----------
        X : array of shape (n_samples,n_params)
            Samples to be transformed.
        
        Returns
        -------
        tr_X: array of shape (n_samples,n_params)
            Transformed samples.

    `fit_dist(self, y)`
    :   Fits the known distributions to the data.
        
        Parameters
        ----------
        y : array of shape (n_samples)
            Data to be fitted.
        
        Returns
        -------
        sel_dist: string
            Selected distribution type from `lognorm`, `norm`, `uniform` or
            `expon`.
        params : list
            Parameters corresponding to the selected distibution type.
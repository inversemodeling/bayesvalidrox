Module bayesvalidrox.surrogate_models.eval_rec_rule
===================================================
Based on the implementation in UQLab [1].

References:
1. S. Marelli, and B. Sudret, UQLab: A framework for uncertainty quantification
in Matlab, Proc. 2nd Int. Conf. on Vulnerability, Risk Analysis and Management
(ICVRAM2014), Liverpool, United Kingdom, 2014, 2554-2563.

2. S. Marelli, N. Lüthen, B. Sudret, UQLab user manual – Polynomial chaos
expansions, Report # UQLab-V1.4-104, Chair of Risk, Safety and Uncertainty
Quantification, ETH Zurich, Switzerland, 2021.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Fri Jan 14 2022

Functions
---------

    
`poly_rec_coeffs(n_max, poly_type, params=None)`
:   Computes the recurrence coefficients for classical Wiener-Askey orthogonal
    polynomials.
    
    Parameters
    ----------
    n_max : int
        Maximum polynomial degree.
    poly_type : string
        Polynomial type.
    params : list, optional
        Parameters required for `laguerre` poly type. The default is None.
    
    Returns
    -------
    AB : dict
        The 3 term recursive coefficients and the applicable ranges.

    
`eval_rec_rule(x, max_deg, poly_type)`
:   Evaluates the polynomial that corresponds to the Jacobi matrix defined
    from the AB.
    
    Parameters
    ----------
    x : array (n_samples)
        Points where the polynomials are evaluated.
    max_deg : int
        Maximum degree.
    poly_type : string
        Polynomial type.
    
    Returns
    -------
    values : array of shape (n_samples, max_deg+1)
        Polynomials corresponding to the Jacobi matrix.

    
`eval_rec_rule_arbitrary(x, max_deg, poly_coeffs)`
:   Evaluates the polynomial at sample array x.
    
    Parameters
    ----------
    x : array (n_samples)
        Points where the polynomials are evaluated.
    max_deg : int
        Maximum degree.
    poly_coeffs : dict
        Polynomial coefficients computed based on moments.
    
    Returns
    -------
    values : array of shape (n_samples, max_deg+1)
        Univariate Polynomials evaluated at samples.

    
`eval_univ_basis(x, max_deg, poly_types, apoly_coeffs=None)`
:   Evaluates univariate regressors along input directions.
    
    Parameters
    ----------
    x : array of shape (n_samples, n_params)
        Training samples.
    max_deg : int
        Maximum polynomial degree.
    poly_types : list of strings
        List of polynomial types for all parameters.
    apoly_coeffs : dict , optional
        Polynomial coefficients computed based on moments. The default is None.
    
    Returns
    -------
    univ_vals : array of shape (n_samples, n_params, max_deg+1)
        Univariate polynomials for all degrees and parameters evaluated at x.
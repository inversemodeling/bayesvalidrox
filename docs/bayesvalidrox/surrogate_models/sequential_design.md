Module bayesvalidrox.surrogate_models.sequential_design
=======================================================
Created on Fri Jan 28 09:21:18 2022

@author: farid

Classes
-------

`SeqDesign(MetaModel)`
:   Sequential experimental design
    This class provieds method for trainig the meta-model in an iterative
    manners.
    The main method to execute the task is `train_seq_design`, which
     recieves a model object and returns the trained metamodel.

    ### Methods

    `train_seq_design(self, Model)`
    :   Starts the adaptive sequential design for refining the surrogate model
        by selecting training points in a sequential manner.
        
        Parameters
        ----------
        Model : object
            An object containing all model specifications.
        
        Returns
        -------
        PCEModel : object
            Meta model object.

    `util_VarBasedDesign(self, X_can, index, util_func='Entropy')`
    :   Computes the exploitation scores based on:
        active learning MacKay(ALM) and active learning Cohn (ALC)
        Paper: Sequential Design with Mutual Information for Computer
        Experiments (MICE): Emulation of a Tsunami Model by Beck and Guillas
        (2016)
        
        Parameters
        ----------
        X_can : array of shape (n_samples, n_params)
            Candidate samples.
        index : int
            Model output index.
        UtilMethod : string, optional
            Exploitation utility function. The default is 'Entropy'.
        
        Returns
        -------
        float
            Score.

    `util_BayesianActiveDesign(self, X_can, sigma2Dict, var='DKL')`
    :   Computes scores based on Bayesian active design criterion (var).
        
        It is based on the following paper:
        Oladyshkin, Sergey, Farid Mohammadi, Ilja Kroeker, and Wolfgang Nowak.
        "Bayesian3 active learning for the gaussian process emulator using
        information theory." Entropy 22, no. 8 (2020): 890.
        
        Parameters
        ----------
        X_can : array of shape (n_samples, n_params)
            Candidate samples.
        sigma2Dict : dict
            A dictionary containing the measurement errors (sigma^2).
        var : string, optional
            BAL design criterion. The default is 'DKL'.
        
        Returns
        -------
        float
            Score.

    `util_BayesianDesign(self, X_can, X_MC, sigma2Dict, var='DKL')`
    :   Computes scores based on Bayesian sequential design criterion (var).
        
        Parameters
        ----------
        X_can : array of shape (n_samples, n_params)
            Candidate samples.
        sigma2Dict : dict
            A dictionary containing the measurement errors (sigma^2).
        var : string, optional
            Bayesian design criterion. The default is 'DKL'.
        
        Returns
        -------
        float
            Score.

    `subdomain(self, Bounds, n_new_samples)`
    :   Divides a domain defined by Bounds into sub domains.
        
        Parameters
        ----------
        Bounds : list of tuples
            List of lower and upper bounds.
        n_new_samples : TYPE
            DESCRIPTION.
        
        Returns
        -------
        Subdomains : TYPE
            DESCRIPTION.

    `run_util_func(self, method, candidates, index, sigma2Dict=None, var=None, X_MC=None)`
    :   Runs the utility function based on the given method.
        
        Parameters
        ----------
        method : string
            Exploitation method: `VarOptDesign`, `BayesActDesign` and
            `BayesOptDesign`.
        candidates : array of shape (n_samples, n_params)
            All candidate parameter sets.
        index : int
            ExpDesign index.
        sigma2Dict : dict, optional
            A dictionary containing the measurement errors (sigma^2). The
            default is None.
        var : string, optional
            Utility function. The default is None.
        X_MC : TYPE, optional
            DESCRIPTION. The default is None.
        
        Returns
        -------
        index : TYPE
            DESCRIPTION.
        TYPE
            DESCRIPTION.

    `dual_annealing(self, method, Bounds, sigma2Dict, var, Run_No, verbose=False)`
    :   Exploration algorithim to find the optimum parameter space.
        
        Parameters
        ----------
        method : string
            Exploitation method: `VarOptDesign`, `BayesActDesign` and
            `BayesOptDesign`.
        Bounds : list of tuples
            List of lower and upper boundaries of parameters.
        sigma2Dict : dict
            A dictionary containing the measurement errors (sigma^2).
        Run_No : int
            Run number.
        verbose : bool, optional
            Print out a summary. The default is False.
        
        Returns
        -------
        Run_No : int
            Run number.
        array
            Optimial candidate.

    `tradoff_weights(self, tradeoff_scheme, old_EDX, old_EDY)`
    :   Calculates weights for exploration scores based on the requested
        scheme: `None`, `equal`, `epsilon-decreasing` and `adaptive`.
        
        `None`: No exploration.
        `equal`: Same weights for exploration and exploitation scores.
        `epsilon-decreasing`: Start with more exploration and increase the
            influence of exploitation along the way with a exponential decay
            function
        `adaptive`: An adaptive method based on:
            Liu, Haitao, Jianfei Cai, and Yew-Soon Ong. "An adaptive sampling
            approach for Kriging metamodeling by maximizing expected prediction
            error." Computers & Chemical Engineering 106 (2017): 171-182.
        
        Parameters
        ----------
        tradeoff_scheme : string
            Trade-off scheme for exloration and exploitation scores.
        old_EDX : array (n_samples, n_params)
            Old experimental design (training points).
        old_EDY : dict
            Old model responses (targets).
        
        Returns
        -------
        exploration_weight : float
            Exploration weight.
        exploitation_weight: float
            Exploitation weight.

    `opt_SeqDesign(self, sigma2, n_candidates=5, var='DKL')`
    :   Runs optimal sequential design.
        
        Parameters
        ----------
        sigma2 : dict, optional
            A dictionary containing the measurement errors (sigma^2). The
            default is None.
        n_candidates : int, optional
            Number of candidate samples. The default is 5.
        var : string, optional
            Utility function. The default is None.
        
        Raises
        ------
        NameError
            Wrong utility function.
        
        Returns
        -------
        Xnew : array (n_samples, n_params)
            Selected new training point(s).

    `util_AlphOptDesign(self, candidates, var='D-Opt')`
    :   Enriches the Experimental design with the requested alphabetic
        criterion based on exploring the space with number of sampling points.
        
        Ref: Hadigol, M., & Doostan, A. (2018). Least squares polynomial chaos
        expansion: A review of sampling strategies., Computer Methods in
        Applied Mechanics and Engineering, 332, 382-407.
        
        Arguments
        ---------
        NCandidate : int
            Number of candidate points to be searched
        
        var : string
            Alphabetic optimality criterion
        
        Returns
        -------
        X_new : array of shape (1, n_params)
            The new sampling location in the input space.
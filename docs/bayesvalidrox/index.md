Module bayesvalidrox
====================

Sub-modules
-----------
* bayesvalidrox.bayes_inference
* bayesvalidrox.post_processing
* bayesvalidrox.pylink
* bayesvalidrox.surrogate_models

Classes
-------

`PyLinkForwardModel(link_type='pylink', name=None, py_file=None, shell_command='', input_file=None, input_template=None, aux_file=None, exe_path='', output_file_names=[], output_names=[], output_parser='', multi_process=True, n_cpus=None, meas_file=None, meas_file_valid=None, mc_ref_file=None, obs_dict={}, obs_dict_valid={}, mc_ref_dict={})`
:   A forward model binder
    
    This calss serves as a code wrapper. This wrapper allows the execution of
    a third-party software/solver within the scope of BayesValidRox.
    
    Attributes
    ----------
    link_type : str
        The type of the wrapper. The default is `'pylink'`. This runs the
        third-party software or an executable using a sell command with given
        input files.
        Second option is `'function'` which assumed that model can be run using
        a function written separately in a Python script.
    name : str
        Name of the model.
    py_file : str
        Python file name without `.py` extension to be run for the `'function'`
        wrapper. Note that the name of the python file and that of the function
        must be simillar. This function must recieve the parameters in an array
        of shape `(n_samples, n_params)` and returns a dictionary with the
        x_values and output arrays for given output names.
    shell_command : str
        Shell command to be executed for the `'pylink'` wrapper.
    input_file : str or list
        The input file to be passed to the `'pylink'` wrapper.
    input_template : str or list
        A template input file to be passed to the `'pylink'` wrapper. This file
        must be a copy of `input_file` with `<Xi>` place holder for the input
        parameters defined using `inputs` class, with i being the number of
        parameter. The file name ending should include `.tpl` before the actual
        extension of the input file, for example, `params.tpl.input`.
    aux_file : str or list
        The list of auxiliary files needed for the `'pylink'` wrapper.
    exe_path : str
        Execution path if you wish to run the model for the `'pylink'` wrapper
        in another directory. The default is `None`, which corresponds to the
        currecnt working directory.
    output_file_names : list of str
        List of the name of the model output text files for the `'pylink'`
        wrapper.
    output_names : list of str
        List of the model outputs to be used for the analysis.
    output_parser : str
        Name of the model parser file (without `.py` extension) that recieves
        the `output_file_names` and returns a 2d-array with the first row being
        the x_values, e.g. x coordinates or time and the rest of raws pass the
        simulation output for each model output defined in `output_names`. Note
        that again here the name of the file and that of the function must be
        the same.
    multi_process: bool
        Whether the model runs to be executed in parallel for the `'pylink'`
        wrapper. The default is `True`.
    n_cpus: int
        The number of cpus to be used for the parallel model execution for the
        `'pylink'` wrapper. The default is `None`, which corresponds to all
        available cpus.
    meas_file : str
        The name of the measurement text-based file. This file must contain
        x_values as the first column and one column for each model output. The
        default is `None`. Only needed for the Bayesian Inference.
    meas_file_valid : str
        The name of the measurement text-based file for the validation. The
        default is `None`. Only needed for the validation with Bayesian
        Inference.
    mc_ref_file : str
        The name of the text file for the Monte-Carlo reference (mean and
        standard deviation) values. It must contain `x_values` as the first
        column, `mean` as the second column and `std` as the third. It can be
        used to compare the estimated moments using meta-model in the post-
        processing step. This is only available for one output.
    obs_dict : dict
        A dictionary containing the measurement text-based file. It must
        contain `x_values` as the first item and one item for each model output
        . The default is `{}`. Only needed for the Bayesian Inference.
    obs_dict_valid : dict
        A dictionary containing the validation measurement text-based file. It
        must contain `x_values` as the first item and one item for each model
        output. The default is `{}`.
    mc_ref_dict : dict
        A dictionary containing the Monte-Carlo reference (mean and standard
        deviation) values. It must contain `x_values` as the first item and
        `mean` as the second item and `std` as the third. The default is `{}`.
        This is only available for one output.

    ### Class variables

    `Output`
    :

    ### Methods

    `within_range(self, out, minout, maxout)`
    :

    `read_observation(self, case='calib')`
    :   Reads/prepare the observation/measurement data for
        calibration.
        
        Returns
        -------
        DataFrame
            A dataframe with the calibration data.

    `read_mc_reference(self)`
    :   Is used, if a Monte-Carlo reference is available for
        further in-depth post-processing after meta-model training.
        
        Returns
        -------
        None

    `read_output(self)`
    :   Reads the the parser output file and returns it as an
         executable function. It is required when the models returns the
         simulation outputs in csv files.
        
        Returns
        -------
        Output : func
            Output parser function.

    `update_input_params(self, new_input_file, param_set)`
    :   Finds this pattern with <X1> in the new_input_file and replace it with
         the new value from the array param_sets.
        
        Parameters
        ----------
        new_input_file : list
            List of the input files with the adapted names.
        param_set : array of shape (n_params)
            Parameter set.
        
        Returns
        -------
        None.

    `run_command(self, command, output_file_names)`
    :   Runs the execution command given by the user to run the given model.
        It checks if the output files have been generated. If yes, the jobe is
        done and it extracts and returns the requested output(s). Otherwise,
        it executes the command again.
        
        Parameters
        ----------
        command : str
            The shell command to be executed.
        output_file_names : list
            Name of the output file names.
        
        Returns
        -------
        simulation_outputs : array of shape (n_obs, n_outputs)
            Simulation outputs.

    `run_forwardmodel(self, xx)`
    :   This function creates subdirectory for the current run and copies the
        necessary files to this directory and renames them. Next, it executes
        the given command.
        
        Parameters
        ----------
        xx : tuple
            A tuple including parameter set, simulation number and key string.
        
        Returns
        -------
        output : array of shape (n_outputs+1, n_obs)
            An array passed by the output paraser containing the x_values as
            the first row and the simulations results stored in the the rest of
            the array.

    `run_model_parallel(self, c_points, prevRun_No=0, key_str='', mp=True)`
    :   Runs model simulations. If mp is true (default), then the simulations
         are started in parallel.
        
        Parameters
        ----------
        c_points : array of shape (n_samples, n_params)
            Collocation points (training set).
        prevRun_No : int, optional
            Previous run number, in case the sequential design is selected.
            The default is `0`.
        key_str : str, optional
            A descriptive string for validation runs. The default is `''`.
        mp : bool, optional
            Multiprocessing. The default is `True`.
        
        Returns
        -------
        all_outputs : dict
            A dictionary with x values (time step or point id) and all outputs.
            Each key contains an array of the shape `(n_samples, n_obs)`.
        new_c_points : array
            Updated collocation points (training set). If a simulation does not
            executed successfully, the parameter set is removed.

    `zip_subdirs(self, dir_name, key)`
    :   Zips all the files containing the key(word).
        
        Parameters
        ----------
        dir_name : str
            Directory name.
        key : str
            Keyword to search for.
        
        Returns
        -------
        None.

`Input()`
:   A class to define the uncertain input parameters.
    
    Attributes
    ----------
    Marginals : obj
        Marginal objects. See `inputs.Marginal`.
    Rosenblatt : bool
        If Rossenblatt transformation is required for the dependent input
        parameters.
    
    Examples
    -------
    Marginals can be defined as following:
    
    >>> Inputs.add_marginals()
    >>> Inputs.Marginals[0].name = 'X_1'
    >>> Inputs.Marginals[0].dist_type = 'uniform'
    >>> Inputs.Marginals[0].parameters = [-5, 5]
    
    If there is no common data is avaliable, the input data can be given
    as following:
    
    >>> Inputs.add_marginals()
    >>> Inputs.Marginals[0].name = 'X_1'
    >>> Inputs.Marginals[0].input_data = input_data

    ### Class variables

    `poly_coeffs_flag`
    :

    ### Methods

    `add_marginals(self)`
    :   Adds a new Marginal object to the input object.
        
        Returns
        -------
        None.

`Discrepancy(InputDisc='', disc_type='Gaussian', parameters=None)`
:   Discrepancy class for Bayesian inference method.
    We define the reference or reality to be equal to what we can model and a
    descripancy term \( \epsilon \). We consider the followin format:
    
    $$\textbf{y}_{\text{reality}} = \mathcal{M}(\theta) + \epsilon,$$
    
    where \( \epsilon \in R^{N_{out}} \) represents the the effects of
    measurement error and model inaccuracy. For simplicity, it can be defined
    as an additive Gaussian disrepancy with zeromean and given covariance
    matrix \( \Sigma \):
    
    $$\epsilon \sim \mathcal{N}(\epsilon|0, \Sigma). $$
    
    In the context of model inversion or calibration, an observation point
    \( \textbf{y}_i \in \mathcal{y} \) is a realization of a Gaussian
    distribution with mean value of \(\mathcal{M}(\theta) \) and covariance
    matrix of \( \Sigma \).
    
    $$ p(\textbf{y}|\theta) = \mathcal{N}(\textbf{y}|\mathcal{M}
                                             (\theta))$$
    
    The following options are available:
    
    * Option A: With known redidual covariance matrix \(\Sigma\) for
    independent measurements.
    
    * Option B: With unknown redidual covariance matrix \(\Sigma\),
    paramethrized as \(\Sigma(\theta_{\epsilon})=\sigma^2 \textbf{I}_
    {N_{out}}\) with unknown residual variances \(\sigma^2\).
    This term will be jointly infer with the uncertain input parameters. For
    the inversion, you need to define a prior marginal via `Input` class. Note
    that \(\sigma^2\) is only a single scalar multiplier for the diagonal
    entries of the covariance matrix \(\Sigma\).
    
    Attributes
    ----------
    InputDisc : obj
        Input object. When the \(\sigma^2\) is expected to be inferred
        jointly with the parameters (`Option B`).If multiple output groups are
        defined by `Model.Output.names`, each model output needs to have.
        a prior marginal using the `Input` class. The default is `''`.
    disc_type : str
        Type of the noise definition. `'Gaussian'` is only supported so far.
    parameters : dict or pandas.DataFrame
        Known residual variance \(\sigma^2\), i.e. diagonal entry of the
        covariance matrix of the multivariate normal likelihood in case of
        `Option A`.

    ### Methods

    `get_sample(self, n_samples)`
    :   Generate samples for the \(\sigma^2\), i.e. the diagonal entries of
        the variance-covariance matrix in the multivariate normal distribution.
        
        Parameters
        ----------
        n_samples : int
            Number of samples (parameter sets).
        
        Returns
        -------
        sigma2_prior: array of shape (n_samples, n_params)
            \(\sigma^2\) samples.

`MetaModel(input_obj, meta_model_type='PCE', pce_reg_method='OLS', pce_deg=1, pce_q_norm=1.0, dim_red_method='no', verbose=False)`
:   Meta (surrogate) model
    
    This class trains a surrogate model. It accepts an input object (input_obj)
    containing the specification of the distributions for uncertain parameters
    and a model object with instructions on how to run the computational model.
    
    Attributes
    ----------
    input_obj : obj
        Input object with the information on the model input parameters.
    meta_model_type : str
        Surrogate model types. Three surrogate model types are supported:
        polynomial chaos expansion (`PCE`), arbitrary PCE (`aPCE`) and
        Gaussian process regression (`GPE`). Default is PCE.
    pce_reg_method : str
        PCE regression method to compute the coefficients. The following
        regression methods are available:
    
        1. OLS: Ordinary Least Square method
        2. BRR: Bayesian Ridge Regression
        3. LARS: Least angle regression
        4. ARD: Bayesian ARD Regression
        5. FastARD: Fast Bayesian ARD Regression
        6. VBL: Variational Bayesian Learning
        7. EBL: Emperical Bayesian Learning
        Default is `OLS`.
    pce_deg : int or list of int
        Polynomial degree(s). If a list is given, an adaptive algorithm is used
        to find the best degree with the lowest Leave-One-Out cross-validation
        (LOO) error (or the highest score=1-LOO). Default is `1`.
    pce_q_norm : float
        Hyperbolic (or q-norm) truncation for multi-indices of multivariate
        polynomials. Default is `1.0`.
    dim_red_method : str
        Dimensionality reduction method for the output space. The available
        method is based on principal component analysis (PCA). The Default is
        `'no'`. There are two ways to select number of components: use
        percentage of the explainable variance threshold (between 0 and 100)
        (Option A) or direct prescription of components' number (Option B):
    
            >>> MetaModelOpts.dim_red_method = 'PCA'
            >>> MetaModelOpts.var_pca_threshold = 99.999  # Option A
            >>> MetaModelOpts.n_pca_components = 12 # Option B
    
    verbose : bool
        Prints summary of the regression results. Default is `False`.
    
    Note
    -------
    To define the sampling methods and the training set, an experimental design
    instance shall be defined. This can be done by:
    
    >>> MetaModelOpts.add_ExpDesign()
    
    Two experimental design schemes are supported: one-shot (`normal`) and
    adaptive sequential (`sequential`) designs.
    For experimental design refer to `ExpDesigns`.

    ### Class variables

    `auto_vivification`
    :   Implementation of perl's AutoVivification feature.
        
        Source: https://stackoverflow.com/a/651879/18082457

    ### Methods

    `create_metamodel(self, Model)`
    :   Starts the training of the meta-model for the model objects containg
         the given computational model.
        
        Parameters
        ----------
        Model : obj
            Model object.
        
        Returns
        -------
        metamodel : obj
            The meta model object.

    `train_norm_design(self, Model, verbose=False)`
    :   This function loops over the outputs and each time step/point and fits
        the meta model.
        
        Parameters
        ----------
        Model : obj
            Model object.
        verbose : bool, optional
            Flag for a sequential design in silent mode. The default is False.
        
        Returns
        -------
        self: obj
            Meta-model object.

    `create_basis_indices(self, degree, q_norm)`
    :   Creates set of selected multi-indices of multivariate polynomials for
        certain parameter numbers, polynomial degree, hyperbolic (or q-norm)
        truncation scheme.
        
        Parameters
        ----------
        degree : int
            Polynomial degree.
        q_norm : float
            hyperbolic (or q-norm) truncation.
        
        Returns
        -------
        basis_indices : array of shape (n_terms, n_params)
            Multi-indices of multivariate polynomials.

    `add_ExpDesign(self)`
    :   Instanciates experimental design object.
        
        Returns
        -------
        None.

    `generate_ExpDesign(self, Model)`
    :   Prepares the experimental design either by reading from the prescribed
        data or running simulations.
        
        Parameters
        ----------
        Model : obj
            Model object.
        
        Raises
        ------
        Exception
            If model sumulations are not provided properly.
        
        Returns
        -------
        ED_X_tr: array of shape (n_samples, n_params)
            Training samples transformed by an isoprobabilistic transformation.
        ED_Y: dict
            Model simulations (target) for all outputs.

    `univ_basis_vals(self, samples, n_max=None)`
    :   Evaluates univariate regressors along input directions.
        
        Parameters
        ----------
        samples : array of shape (n_samples, n_params)
            Samples.
        n_max : int, optional
            Maximum polynomial degree. The default is `None`.
        
        Returns
        -------
        univ_basis: array of shape (n_samples, n_params, n_max+1)
            All univariate regressors up to n_max.

    `create_psi(self, basis_indices, univ_p_val)`
    :   This function assemble the design matrix Psi from the given basis index
        set INDICES and the univariate polynomial evaluations univ_p_val.
        
        Parameters
        ----------
        basis_indices : array of shape (n_terms, n_params)
            Multi-indices of multivariate polynomials.
        univ_p_val : array of (n_samples, n_params, n_max+1)
            All univariate regressors up to `n_max`.
        
        Raises
        ------
        ValueError
            n_terms in arguments do not match.
        
        Returns
        -------
        psi : array of shape (n_samples, n_terms)
            Multivariate regressors.

    `fit(self, X, y, basis_indices, reg_method=None)`
    :   Fit regression using the regression method provided.
        
        Parameters
        ----------
        X : array of shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples and
            n_features is the number of features.
        y : array of shape (n_samples,)
            Target values.
        basis_indices : array of shape (n_terms, n_params)
            Multi-indices of multivariate polynomials.
        reg_method : str, optional
            DESCRIPTION. The default is None.
        
        Returns
        -------
        return_out_dict : Dict
            Fitted estimator, spareMulti-Index, sparseX and coefficients.

    `adaptive_regression(self, ED_X, ED_Y, varIdx, verbose=False)`
    :   Adaptively fits the PCE model by comparing the scores of different
        degrees and q-norm.
        
        Parameters
        ----------
        ED_X : array of shape (n_samples, n_params)
            Experimental design.
        ED_Y : array of shape (n_samples,)
            Target values, i.e. simulation results for the Experimental design.
        varIdx : int
            Index of the output.
        verbose : bool, optional
            Print out summary. The default is False.
        
        Returns
        -------
        returnVars : Dict
            Fitted estimator, best degree, best q-norm, LOOCVScore and
            coefficients.

    `corr_loocv_error(self, clf, psi, coeffs, y)`
    :   Calculates the corrected LOO error for regression on regressor
        matrix `psi` that generated the coefficients based on [1] and [2].
        
        [1] Blatman, G., 2009. Adaptive sparse polynomial chaos expansions for
            uncertainty propagation and sensitivity analysis (Doctoral
            dissertation, Clermont-Ferrand 2).
        
        [2] Blatman, G. and Sudret, B., 2011. Adaptive sparse polynomial chaos
            expansion based on least angle regression. Journal of computational
            Physics, 230(6), pp.2345-2367.
        
        Parameters
        ----------
        clf : object
            Fitted estimator.
        psi : array of shape (n_samples, n_features)
            The multivariate orthogonal polynomials (regressor).
        coeffs : array-like of shape (n_features,)
            Estimated cofficients.
        y : array of shape (n_samples,)
            Target values.
        
        Returns
        -------
        Q_2 : float
            LOOCV Validation score (1-LOOCV erro).
        residual : array of shape (n_samples,)
            Residual values (y - predicted targets).

    `pca_transformation(self, Output)`
    :   Transforms the targets (outputs) via Principal Component Analysis
        
        Parameters
        ----------
        Output : array of shape (n_samples,)
            Target values.
        
        Returns
        -------
        pca : obj
            Fitted sklearnPCA object.
        OutputMatrix : array of shape (n_samples,)
            Transformed target values.

    `gaussian_process_emulator(self, X, y, nug_term=None, autoSelect=False, varIdx=None)`
    :   Fits a Gaussian Process Emulator to the target given the training
         points.
        
        Parameters
        ----------
        X : array of shape (n_samples, n_params)
            Training points.
        y : array of shape (n_samples,)
            Target values.
        nug_term : float, optional
            Nugget term. The default is None, i.e. variance of y.
        autoSelect : bool, optional
            Loop over some kernels and select the best. The default is False.
        varIdx : int, optional
            The index number. The default is None.
        
        Returns
        -------
        gp : object
            Fitted estimator.

    `eval_metamodel(self, samples=None, nsamples=None, sampling_method='random', return_samples=False)`
    :   Evaluates meta-model at the requested samples. One can also generate
        nsamples.
        
        Parameters
        ----------
        samples : array of shape (n_samples, n_params), optional
            Samples to evaluate meta-model at. The default is None.
        nsamples : int, optional
            Number of samples to generate, if no `samples` is provided. The
            default is None.
        sampling_method : str, optional
            Type of sampling, if no `samples` is provided. The default is
            'random'.
        return_samples : bool, optional
            Retun samples, if no `samples` is provided. The default is False.
        
        Returns
        -------
        mean_pred : dict
            Mean of the predictions.
        std_pred : dict
            Standard deviatioon of the predictions.

    `create_model_error(self, X, y, name='Calib')`
    :   Fits a GPE-based model error.
        
        Parameters
        ----------
        X : array of shape (n_outputs, n_inputs)
            Input array. It can contain any forcing inputs or coordinates of
             extracted data.
        y : array of shape (n_outputs,)
            The model response for the MAP parameter set.
        name : str, optional
            Calibration or validation. The default is `'Calib'`.
        
        Returns
        -------
        self: object
            Self object.

    `eval_model_error(self, X, y_pred)`
    :   Evaluates the error model.
        
        Parameters
        ----------
        X : array
            Inputs.
        y_pred : dict
            Predictions.
        
        Returns
        -------
        mean_pred : dict
            Mean predition of the GPE-based error model.
        std_pred : dict
            standard deviation of the GPE-based error model.

`PostProcessing(MetaModel, name='calib')`
:   This class provides many helper functions to post-process the trained
    meta-model.
    
    Attributes
    ----------
    MetaModel : obj
        MetaModel object to do postprocessing on.
    name : str
        Type of the anaylsis. The default is `'calib'`. If a validation is
        expected to be performed change this to `'valid'`.

    ### Methods

    `plot_moments(self, xlabel='Time [s]', plot_type=None, save_fig=True)`
    :   Plots the moments in a pdf format in the directory
        `Outputs_PostProcessing`.
        
        Parameters
        ----------
        xlabel : str, optional
            String to be displayed as x-label. The default is `'Time [s]'`.
        plot_type : str, optional
            Options: bar or line. The default is `None`.
        save_fig : bool, optional
            Save figure or not. The default is `True`.
        
        Returns
        -------
        pce_means: dict
            Mean of the model outputs.
        pce_means: dict
            Standard deviation of the model outputs.

    `valid_metamodel(self, n_samples=1, samples=None, x_axis='Time [s]')`
    :   Evaluates and plots the meta model and the PCEModel outputs for the
        given number of samples or the given samples.
        
        Parameters
        ----------
        n_samples : int, optional
            Number of samples to be evaluated. The default is 1.
        samples : array of shape (n_samples, n_params), optional
            Samples to be evaluated. The default is None.
        x_axis : str, optional
            Label of x axis. The default is `'Time [s]'`.
        
        Returns
        -------
        None.

    `check_accuracy(self, n_samples=None, samples=None, outputs=None)`
    :   Checks accuracy of the metamodel by computing the root mean square
        error and validation error for all outputs.
        
        Parameters
        ----------
        n_samples : int, optional
            Number of samples. The default is None.
        samples : array of shape (n_samples, n_params), optional
            Parameter sets to be checked. The default is None.
        outputs : dict, optional
            Output dictionary with model outputs for all given output types in
            `Model.Output.names`. The default is None.
        
        Raises
        ------
        Exception
            When neither n_samples nor samples are provided.
        
        Returns
        -------
        rmse: dict
            Root mean squared error for each output.
        valid_error : dict
            Validation error for each output.

    `plot_seq_design_diagnostics(self, ref_BME_KLD=None, save_fig=True)`
    :   Plots the Bayesian Model Evidence (BME) and Kullback-Leibler divergence
        (KLD) for the sequential design.
        
        Parameters
        ----------
        ref_BME_KLD : array, optional
            Reference BME and KLD . The default is `None`.
        save_fig : bool, optional
            Whether to save the figures. The default is `True`.
        
        Returns
        -------
        None.

    `sobol_indices(self, xlabel='Time [s]', plot_type=None, save_fig=True)`
    :   Provides Sobol indices as a sensitivity measure to infer the importance
        of the input parameters. See Eq. 27 in [1] for more details. For the
        case with Principal component analysis refer to [2].
        
        [1] Global sensitivity analysis: A flexible and efficient framework
        with an example from stochastic hydrogeology S. Oladyshkin, F.P.
        de Barros, W. Nowak  https://doi.org/10.1016/j.advwatres.2011.11.001
        
        [2] Nagel, J.B., Rieckermann, J. and Sudret, B., 2020. Principal
        component analysis and sparse polynomial chaos expansions for global
        sensitivity analysis and model calibration: Application to urban
        drainage simulation. Reliability Engineering & System Safety, 195,
        p.106737.
        
        Parameters
        ----------
        xlabel : str, optional
            Label of the x-axis. The default is `'Time [s]'`.
        plot_type : str, optional
            Plot type. The default is `None`. This corresponds to line plot.
            Bar chart can be selected by `bar`.
        save_fig : bool, optional
            Whether to save the figures. The default is `True`.
        
        Returns
        -------
        sobol_cell: dict
            Sobol indices.
        total_sobol: dict
            Total Sobol indices.

    `check_reg_quality(self, n_samples=1000, samples=None, save_fig=True)`
    :   Checks the quality of the metamodel for single output models based on:
        https://towardsdatascience.com/how-do-you-check-the-quality-of-your-regression-model-in-python-fa61759ff685
        
        
        Parameters
        ----------
        n_samples : int, optional
            Number of parameter sets to use for the check. The default is 1000.
        samples : array of shape (n_samples, n_params), optional
            Parameter sets to use for the check. The default is None.
        save_fig : bool, optional
            Whether to save the figures. The default is True.
        
        Returns
        -------
        None.

    `eval_pce_model_3d(self, save_fig=True)`
    :

`BayesInference(MetaModel, discrepancy=None, emulator=True, name='Calib', bootstrap=False, req_outputs=None, selected_indices=None, samples=None, n_samples=500000, measured_data=None, inference_method='rejection', mcmc_params=None, bayes_loocv=False, n_bootstrap_itrs=1, perturbed_data=[], bootstrap_noise=0.05, plot_post_pred=True, plot_map_pred=False, max_a_posteriori='mean', corner_title_fmt='.3e')`
:   A class to perform Bayesian Analysis.
    
    
    Attributes
    ----------
    MetaModel : obj
        Meta model object.
    discrepancy : obj
        The discrepancy object for the sigma2s, i.e. the diagonal entries
        of the variance matrix for a multivariate normal likelihood.
    name : str, optional
        The type of analysis, either calibration (`Calib`) or validation
        (`Valid`). The default is `'Calib'`.
    emulator : bool, optional
        Analysis with emulator (MetaModel). The default is `True`.
    bootstrap : bool, optional
        Bootstrap the analysis. The default is `False`.
    req_outputs : list, optional
        The list of requested output to be used for the analysis.
        The default is `None`. If None, all the defined outputs for the model
        object is used.
    selected_indices : dict, optional
        A dictionary with the selected indices of each model output. The
        default is `None`. If `None`, all measurement points are used in the
        analysis.
    samples : array of shape (n_samples, n_params), optional
        The samples to be used in the analysis. The default is `None`. If
        None the samples are drawn from the probablistic input parameter
        object of the MetaModel object.
    n_samples : int, optional
        Number of samples to be used in the analysis. The default is `500000`.
        If samples is not `None`, this argument will be assigned based on the
        number of samples given.
    measured_data : dict, optional
        A dictionary containing the observation data. The default is `None`.
        if `None`, the observation defined in the Model object of the
        MetaModel is used.
    inference_method : str, optional
        A method for approximating the posterior distribution in the Bayesian
        inference step. The default is `'rejection'`, which stands for
        rejection sampling. A Markov Chain Monte Carlo sampler can be simply
        selected by passing `'MCMC'`.
    mcmc_params : dict, optional
        A dictionary with args required for the Bayesian inference with
        `MCMC`. The default is `None`.
    
        Pass the mcmc_params like the following:
    
            >>> mcmc_params:{
                'init_samples': None,  # initial samples
                'n_walkers': 100,  # number of walkers (chain)
                'n_steps': 100000,  # number of maximum steps
                'n_burn': 200,  # number of burn-in steps
                'moves': None,  # Moves for the emcee sampler
                'multiprocessing': False,  # multiprocessing
                'verbose': False # verbosity
                }
        The items shown above are the default values. If any parmeter is
        not defined, the default value will be assigned to it.
    bayes_loocv : bool, optional
        Bayesian Leave-one-out Cross Validation. The default is `False`. If
        `True`, the LOOCV procedure is used to estimate the bayesian Model
        Evidence (BME).
    n_bootstrap_itrs : int, optional
        Number of bootstrap iteration. The default is `1`. If bayes_loocv is
        `True`, this is qualt to the total length of the observation data
        set.
    perturbed_data : array of shape (n_bootstrap_itrs, n_obs), optional
        User defined perturbed data. The default is `[]`.
    bootstrap_noise : float, optional
        A noise level to perturb the data set. The default is `0.05`.
    plot_post_pred : bool, optional
        Plot posterior predictive plots. The default is `True`.
    plot_map_pred : bool, optional
        Plot the model outputs vs the metamodel predictions for the maximum
        a posteriori (defined as `max_a_posteriori`) parameter set. The
        default is `False`.
    max_a_posteriori : str, optional
        Maximum a posteriori. `'mean'` and `'mode'` are available. The default
        is `'mean'`.
    corner_title_fmt : str, optional
        Title format for the posterior distribution plot with python
        package `corner`. The default is `'.3e'`.

    ### Methods

    `create_inference(self)`
    :   Starts the inference.
        
        Returns
        -------
        BayesInference : obj
            The Bayes inference object.

    `normpdf(self, outputs, obs_data, total_sigma2s, sigma2=None, std=None)`
    :   Calculates the likelihood of simulation outputs compared with
        observation data.
        
        Parameters
        ----------
        outputs : dict
            A dictionary containing the simulation outputs as array of shape
            (n_samples, n_measurement) for each model output.
        obs_data : dict
            A dictionary/dataframe containing the observation data.
        total_sigma2s : dict
            A dictionary with known values of the covariance diagonal entries,
            a.k.a sigma^2.
        sigma2 : array, optional
            An array of the sigma^2 samples, when the covariance diagonal
            entries are unknown and are being jointly inferred. The default is
            None.
        std : dict, optional
            A dictionary containing the root mean squared error as array of
            shape (n_samples, n_measurement) for each model output. The default
            is None.
        
        Returns
        -------
        logLik : array of shape (n_samples)
            Likelihoods.
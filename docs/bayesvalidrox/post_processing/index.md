Module bayesvalidrox.post_processing
====================================

Sub-modules
-----------
* bayesvalidrox.post_processing.adaptPlot
* bayesvalidrox.post_processing.post_processing

Classes
-------

`PostProcessing(MetaModel, name='calib')`
:   This class provides many helper functions to post-process the trained
    meta-model.
    
    Attributes
    ----------
    MetaModel : obj
        MetaModel object to do postprocessing on.
    name : str
        Type of the anaylsis. The default is `'calib'`. If a validation is
        expected to be performed change this to `'valid'`.

    ### Methods

    `plot_moments(self, xlabel='Time [s]', plot_type=None, save_fig=True)`
    :   Plots the moments in a pdf format in the directory
        `Outputs_PostProcessing`.
        
        Parameters
        ----------
        xlabel : str, optional
            String to be displayed as x-label. The default is `'Time [s]'`.
        plot_type : str, optional
            Options: bar or line. The default is `None`.
        save_fig : bool, optional
            Save figure or not. The default is `True`.
        
        Returns
        -------
        pce_means: dict
            Mean of the model outputs.
        pce_means: dict
            Standard deviation of the model outputs.

    `valid_metamodel(self, n_samples=1, samples=None, x_axis='Time [s]')`
    :   Evaluates and plots the meta model and the PCEModel outputs for the
        given number of samples or the given samples.
        
        Parameters
        ----------
        n_samples : int, optional
            Number of samples to be evaluated. The default is 1.
        samples : array of shape (n_samples, n_params), optional
            Samples to be evaluated. The default is None.
        x_axis : str, optional
            Label of x axis. The default is `'Time [s]'`.
        
        Returns
        -------
        None.

    `check_accuracy(self, n_samples=None, samples=None, outputs=None)`
    :   Checks accuracy of the metamodel by computing the root mean square
        error and validation error for all outputs.
        
        Parameters
        ----------
        n_samples : int, optional
            Number of samples. The default is None.
        samples : array of shape (n_samples, n_params), optional
            Parameter sets to be checked. The default is None.
        outputs : dict, optional
            Output dictionary with model outputs for all given output types in
            `Model.Output.names`. The default is None.
        
        Raises
        ------
        Exception
            When neither n_samples nor samples are provided.
        
        Returns
        -------
        rmse: dict
            Root mean squared error for each output.
        valid_error : dict
            Validation error for each output.

    `plot_seq_design_diagnostics(self, ref_BME_KLD=None, save_fig=True)`
    :   Plots the Bayesian Model Evidence (BME) and Kullback-Leibler divergence
        (KLD) for the sequential design.
        
        Parameters
        ----------
        ref_BME_KLD : array, optional
            Reference BME and KLD . The default is `None`.
        save_fig : bool, optional
            Whether to save the figures. The default is `True`.
        
        Returns
        -------
        None.

    `sobol_indices(self, xlabel='Time [s]', plot_type=None, save_fig=True)`
    :   Provides Sobol indices as a sensitivity measure to infer the importance
        of the input parameters. See Eq. 27 in [1] for more details. For the
        case with Principal component analysis refer to [2].
        
        [1] Global sensitivity analysis: A flexible and efficient framework
        with an example from stochastic hydrogeology S. Oladyshkin, F.P.
        de Barros, W. Nowak  https://doi.org/10.1016/j.advwatres.2011.11.001
        
        [2] Nagel, J.B., Rieckermann, J. and Sudret, B., 2020. Principal
        component analysis and sparse polynomial chaos expansions for global
        sensitivity analysis and model calibration: Application to urban
        drainage simulation. Reliability Engineering & System Safety, 195,
        p.106737.
        
        Parameters
        ----------
        xlabel : str, optional
            Label of the x-axis. The default is `'Time [s]'`.
        plot_type : str, optional
            Plot type. The default is `None`. This corresponds to line plot.
            Bar chart can be selected by `bar`.
        save_fig : bool, optional
            Whether to save the figures. The default is `True`.
        
        Returns
        -------
        sobol_cell: dict
            Sobol indices.
        total_sobol: dict
            Total Sobol indices.

    `check_reg_quality(self, n_samples=1000, samples=None, save_fig=True)`
    :   Checks the quality of the metamodel for single output models based on:
        https://towardsdatascience.com/how-do-you-check-the-quality-of-your-regression-model-in-python-fa61759ff685
        
        
        Parameters
        ----------
        n_samples : int, optional
            Number of parameter sets to use for the check. The default is 1000.
        samples : array of shape (n_samples, n_params), optional
            Parameter sets to use for the check. The default is None.
        save_fig : bool, optional
            Whether to save the figures. The default is True.
        
        Returns
        -------
        None.

    `eval_pce_model_3d(self, save_fig=True)`
    :
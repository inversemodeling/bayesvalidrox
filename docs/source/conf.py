# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../'))
sys.path.insert(0, os.path.abspath('../../src/'))
#sys.path.insert(0, os.path.abspath('../../src/bayesvalidrox'))
#sys.path.insert(0, os.path.abspath('../../src/bayesvalidrox/surrogate_models'))
#sys.path.insert(0, os.path.abspath('../../src/bayesvalidrox/pylink'))
#sys.path.insert(0, os.path.abspath('../../src/bayesvalidrox/post_processing'))
#sys.path.insert(0, os.path.abspath('../../src/bayesvalidrox/bayes_inference'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'bayesvalidrox'
copyright = '2023, Farid Mohammadi, Rebecca Kohlhaas'
author = 'Farid Mohammadi, Rebecca Kohlhaas'
release = '2.0.0'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'm2r2',
    'sphinx.ext.mathjax',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Path for mathjax
mathjax_path = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Add the logo
html_logo = 'BVRLogoV03_shorttext.png'

# Other html options
# Colors for BVRox:
#'#9bb4e3', # very light blue
#'#7d93dd', # light blue
#'#5663a1', # mid blue
#'#263455', # dark blue
html_theme_options = {
    'sidebar_hide_name': True,
    'navigation_with_keys': True,
    'light_css_variables': {
        'color-brand-primary': '#5663a1',
        'color-brand-content': '#5663a1',
    },
    'dark_css_variables': {
        'color-brand-primary': '#9bb4e3',
        'color-brand-content': '#9bb4e3',
    },
}

# Html-related css - path is relative to html_static_path
html_css_files = [
    'custom.css',
]
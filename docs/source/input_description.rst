Priors, input space and experimental design
*******************************************
The surrogate models, as used in BayesValidRox, consider model formulations where at least one of the input parameters is associated with uncertainty.
This uncertainty can be described as probability distributions over possible values for the parameter.

.. container:: twocol

   .. container:: leftside
   
      Four classes contained in bayesvalidrox are associated with describing uncertain inputs: :any:`bayesvalidrox.surrogate_models.inputs.Marginal`, :any:`bayesvalidrox.surrogate_models.inputs.Input`, :any:`bayesvalidrox.surrogate_models.input_space.InputSpace` and :any:`bayesvalidrox.surrogate_models.exp_designs.ExpDesigns`. 
   
      Uncertain parameters are specified via their marginal distributions in :any:`bayesvalidrox.surrogate_models.inputs.Marginal` objects as either distribution types with associated parameters, or via a set of realizations.
      Supported distribution types include ``unif``, ``norm``, ``gamma``, ``beta``, ``lognorm``, ``expon`` and ``weibull``.
   
      All marginals contained in an :any:`bayesvalidrox.surrogate_models.inputs.Input` object should be defined in the same manner, mixing definitions via distribution type and sampels is not supported.

      If ``rosenblatt`` is set as ``True``, then a Rosenblatt transform will be applied for training the surrogate.

   .. container:: rightside
   
      .. image:: ./diagrams/input_classes.png
         :width: 300
         :alt: UML diagram for input-related classes in bayesvalidrox
   
.. note::
   When using a polynomial-type surrogate setting ``rosenblatt`` to ``True`` results in all hermite polynomials.
   If all uncertain parameters are specified as samples, the corresponding polynomials are automatically calculated following an aPCE.
   
The :any:`bayesvalidrox.surrogate_models.input_space.InputSpace` checks the validity of the given marginals and builds the input space.
This includes sampling from the distributions and applying the Rosenblatt transform or an isoprobabilistic transformation.

The class :any:`bayesvalidrox.surrogate_models.input_space.InputSpace` is used mainly in the surrogate model.
The class :any:`bayesvalidrox.surrogate_models.exp_designs.ExpDesigns` additionally contains methods and attributes related to sampling from the input space for static and iterative training of the surrogate model.
Supported sampling methods include ``random``, ``latin-hypercube``, ``sobol``, ``halton``, ``hammersley``, ``chebyshev(FT)``, ``grid(FT)`` and ``user`` for user-defined sampling.

The options for iterative metamodel training are detailed in :any:`al_description`.

Example
=======
In practice, only the classes :any:`bayesvalidrox.surrogate_models.inputs.Input` and :any:`bayesvalidrox.surrogate_models.exp_designs.ExpDesigns` are directly used.

>>> from bayesvalidrox import Input, ExpDesign

Marginals of uncertain parameters can be directly added and defined on an ``Input`` object.

>>> inputs = Input()

If they are defined via distribution types, the ``name``, ``dist_type`` and ``parameters`` for the distribution should be set.

>>> inputs.add_marginals(name='$X$', dist_type ='unif', parameters=[-5,5])
        
If they are given from data, only ``name`` and ``input_data`` are relevant.

>>> inputParams = np.random.uniform(-5,-5,100)
>>> inputs.add_marginals(name='$X$', input_data=inputParams)

An experimental design can be constructed based on these inputs.

>>> exp_design = ExpDesigns(inputs)

Samples of the marginals can be created by specifying a sampling method and generating the wanted number of samples.

>>> exp_design.sampling_method = 'latin_hypercube'
>>> samples = exp_design.generate_samples(100)

The generated samples can be visualized against their marginal distributions.

>>> exp_design.plot_samples(samples)

The results will be saved in the folder ``Outputs_Priors``.

.. image:: ./examples/user_guide/Outputs_Priors/prior_$X$.png
   :width: 400
   :alt: Generated samples against their marginal distribution

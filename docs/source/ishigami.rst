Example: ishigami
*****************
This example deals with the surrogate modeling of a Ishigami function.
You will see how to check the quality of your regression model and perform 
sensitivity analysis via Sobol Indices.

	ISHIGAMI FUNCTION

    Authors: Sonja Surjanovic, Simon Fraser University
             Derek Bingham, Simon Fraser University
			 
    Questions/Comments: Please email Derek Bingham at dbingham@stat.sfu.ca.

    Copyright 2013. Derek Bingham, Simon Fraser University.

    THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    derivative works, such modified software should be clearly marked.
    Additionally, this program is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; version 2.0 of the License.
    Accordingly, this program is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    For function details and reference information, see:
        https://www.sfu.ca/~ssurjano/ishigami.html

No reference data is given for this example, the surrogate is trained with BCS as the regression method and no active learning.

Model and Data
==============

.. list-table:: Pylink model
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function
   * - Number of input parameters
     - 3
   * - Number of output parameters
     - 1: flow rate [m$^3$/yr]
   * - Time- or space- dependency
     - ??
   * - MC reference
     - No
	 
.. list-table:: Priors
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - X_1
     - uniform
   * - X_2
     - uniform
   * - X_3
     - uniform
	
Surrogate
=========

.. list-table:: MetaModel settings
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - surrogate-type
     - aPCE
   * - associated model
     - 'Ishigami'
   * - degree choices
     - max degree 14, q-norm truncation 1.0
   * - regression
     - BCS (Bayesian Compressive Sensing)
	 
	 
.. list-table:: Training choices
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Static sampling method
     - latin-hypercube
   * - Number of static samples
     - 200
   * - Number of total samples
     - 200
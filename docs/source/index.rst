.. bayesvalidrox documentation master file, created by
   sphinx-quickstart on Wed Dec 13 11:14:59 2023.
   
Surrogate-assisted Bayesian validation of computational models
==============================================================
**BayesValidRox** is an open-source python package that provides methods for surrogate modeling, Bayesian inference and model comparison.

.. image:: ./diagrams/balance.png
   :width: 600
   :alt: Weighting model results against data with associated uncertainty, costs and sparsity
   
An introductory tutorial to the overall workflow with **bayesvalidrox** is provided in :any:`tutorial` and descriptions of the available examples can be found in :any:`examples`.
The functionality and options for the different classes is described more in-depth in :any:`packagedescription` and a list of all the classes and functions is provided in :any:`api`.

Links
-----
* GitHub: https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/tree/master?ref_type=heads
* PyPI: https://pypi.org/project/bayesvalidrox/
* Documentation:


Installation
------------
This package runs under Python 3.9 for versions <1.0.0 and 3.9+ from version 1.0.0 on, use pip to install:

.. code-block:: bash

   pip install bayesvalidrox


Quickstart
----------
Here we show a minimal example to get started on working with BayesValidRox.
The :any:`packagedescription` goes into more detail on the available options and proposed workflow.

The central functionalities of BayesValidRox all depend on building an object of class ``Engine`` that includes an interface to a model and a definition of an input space and sampling option in the form of an ``ExpDesigns`` object.
It can contain and build a surrogate model of class ``MetaModel``, but also functions without one.

We import the needed classes in our main file ``main.py``.

>>> from bayesvalidrox import PyLinkForwardModel, InputSpace, ExpDesigns, Engine, PCE

Here we use a simple linear model. 
This is defined in another python file in the same folder, here we call it ``model.py``.
This file contains a python function that expects samples of two parameter and returns a linear combination of them.
For a detailed description of the expected output format see :any:`model_description`.

>>> def model(samples):
>>> return {'Z':samples[:,0]+2*samples[:,1], 'x_values':[0]}

With this we can create the interface to the model in ``main.py``.

>>> model = PyLinkForwardModel(name='linear model', link_type='Function', 
>>>         py_file='model', output_names=['Z'])

We specify marginal distributions on the inputs in an object of class ``InputSpace`` and use this to build the experimental design.

>>> inputs = InputSpace()
>>> inputs.add_marginals(name='input0', dist_type='unif', parameters=[0,1])
>>> inputs.add_marginals(name='input1', dist_type='unif', parameters=[0,1])
>>> 
>>> exp_design = ExpDesigns(inputs, sampling_method='random')

If we do not want to build a surrogate model, we can define the engine from these objects.

>>> engine = Engine(None, model, exp_design)

If we want to build a surrogate model, we create and object of a class that inherits from ``MetaModel`` and set its properties.
Here we build an arbitrary Polynomial Chaos Expansion using the class ``PCE`` and train it on 10 samples given by the experimental design and the model.

>>> meta_model = PCE(inputs, meta_model_type ='aPCE', pce_reg_method ='FastARD',
>>>         pce_deg = 3, pce_q_norm = 0.85)
>>> exp_design.n_init_samples = 10
>>> 
>>> engine = Engine(meta_model, model, exp_design)
>>> engine.train_normal()

The engine with the trained metamodel can now be used for postprocessing, Bayesian inference, of Bayesian model comparison.

License
-------
BayesValidRox is licensed under the MIT license_.

.. _license: https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/blob/master/LICENCE.md

Contribution
------------
We would be happy for you to contribute to **BayesValidRox**.
This can include e.g. reporting issues, proposing new features, working on features, or support with the documentation.
If you want to contibute, check out our contribution_ guidelines.
You can contact us on the gitlab_ page.

.. _gitlab: https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox

.. _contribution: https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/blob/docs/sphinx_new/CONTRIBUTING.md?ref_type=heads

Further contents
----------------

.. toctree::
   :maxdepth: 1
   
   packagedescription
   tutorial
   examples
   api

   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

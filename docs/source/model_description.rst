Models
******

.. container:: twocol

   .. container:: leftside
   
      BayesValidRox gives options to create interfaces for a variety of models with the class :any:`bayesvalidrox.pylink.pylink.PyLinkForwardModel`.
      Its main function is to run the model on given samples and to read in and contain MC references and observations.
	  
      Models can be defined via python functions, shell commands or as general executables.
      This allows for the use of BayesValidRox with a broad range of models and easy extension to models that are defined with e.g. UM-Bridge.

   .. container:: rightside
   
      .. image:: ./diagrams/model.png
         :width: 150
         :alt: UML diagram for the bayesvalidrox class :any:`bayesvalidrox.pylink.pylink.PyLinkForwardModel`.
   
Example
=======
For this example we use a very simple model that maps the uncertain parameter :math:`X` from the example in :any:`input_description` to two outputs, the original value and its square.
We define this model as a function ``model`` in a new file ``model.py``.
This function takes a single realization of the uncertain parameter as a 2-dimensional ``np.array`` and returns a dictionary of model results.
Here we use the key ``A`` for the sample values and ``B`` for their squares.
Under the key ``x_values`` a list should be given that is of the same length as each output of the model for a single input.
The values in this list can denote e.g. timesteps and are used in postprocessing as labels of the x-axis.
If we want to set the ``x_values`` outside of the model, it can also be given as an additional parameter

>>> def model(samples, x_values):
>>>     sample = samples[0]*x_values
>>>     square = np.power(samples[0]*x_values, 2)
>>>     outputs = {'A': sample, 'B': square, 'x_values': [0]}
>>>     return outputs

In the main python file we can create an object of :any:`bayesvalidrox.pylink.pylink.PyLinkForwardModel` that links to this model.

>>> from bayesvalidrox import PyLinkForwardModel

We create an object of this class and set the ``link_type`` as 'Function' to indicate that this is a function in a python file.
``py_file`` should be set to the filename of the model and ``name`` to the name of the function.
Lastly we list the keys of the outputs that we are interested in.

>>> model = PyLinkForwardModel()
>>> model.link_type = 'Function'
>>> model.py_file = 'model'
>>> model.name = 'model'
>>> model.output.names = ['A']

Any parameters to the model function, that are not the samples, can be set via the ``func_args`` argument.
In this case we define ``x_values`` as a ``np.array`` and include it.

>>> x_values = np.arange(0,1,0.1)
>>> model.func_args = {'x_values':x_values}

With this we have completed an interface to our model.
We can now evaluate this model on the samples created in the input example.

>>> output, samples = model.run_model_parallel(samples)

Other types of model links can be found in :any:`examples`, e.g. :any:`beam` uses a shell command and parser.

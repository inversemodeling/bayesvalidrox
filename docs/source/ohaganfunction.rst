Example: OHagan-function
************************
This example deals with the surrogate modeling of O'Hagan function with 15
parameters.
You will see how to check the quality of your regression model and perform sensitivity analysis via Sobol Indices
	

    Oakley & O'Hagan (2004) Function

    This function's a-coefficients are chosen so that 5 of the input variables
    contribute significantly to the output variance, 5 have a much smaller
    effect, and the remaining 5 have almost no effect on the output variance.

    O'Hagan, 2004, Probabilistic sensitivity analysis of complex models: a
    Bayesian approach J. R. Statist. Soc. B (2004) 66, Part 3, pp. 751-769.

This example trains a surrogate with AL.
FastARD is set as the regression type and the space-filling sequential exploitaiton scheme is chosen as no data is given.


Model and Data
==============

.. list-table:: Pylink model
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function
   * - Number of input parameters
     - 15
   * - Number of output parameters
     - 1
   * - Time- or space- dependency
     - ??
   * - MC reference
     - No
	 
.. list-table:: Priors
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - 0-14
     - gaussian
	
Surrogate
=========

.. list-table:: MetaModel settings
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - surrogate-type
     - aPCE
   * - associated model
     - 'OHagan'
   * - degree choices
     - max degree 7, q-norm truncation 0.65
   * - regression
     - FastARD
	 
	 
.. list-table:: Training choices
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Static sampling method
     - latin-hypercube
   * - Number of static samples
     - 100
   * - Number of total samples
     - 500
   * - Number of samples per AL iteration
     - 1
   * - AL tradeoff scheme
     - None
   * - AL exploration method
     - latin-hypercube, n_candidates=10000, n_cand_groups=4
   * - AL exploitation method
     - space-filling 
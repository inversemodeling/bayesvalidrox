USER GUIDE
**********

Installation
------------
BayesValidRox provides functionalities for describing uncertain parameters, building surrogate models based on model outputs and evaluating them with Bayesian validation methods.

This package runs under Python 3.9 for versions <1.0.0 and 3.9+ from version 1.0.0 on.
It can be installed with pip, best practice is to do so inside a virtual environment.

.. code-block:: bash

   python3 -m venv bayes_env
   cd bayes_env
   source bin/activate

Here replace ``bayes_env`` with your preferred name.
Then install the latest release of BayesValidRox inside the venv.

.. code-block:: bash

   pip install bayesvalidrox

The current master can be installed by cloning the repository.

.. code-block:: bash

   git clone https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox.git
   cd bayesvalidrox
   pip install .

Overview
========
This package is split into multiple aspects corresponding to its folder structure.


.. image:: ./diagrams/folder_structure.png
   :width: 600
   :alt: Folder structure of **bayesvalidrox**

The folder ``surrogate_models`` contains all the functions and classes that are necessary in order to create and train the surrogate model.
This includes 

* defining the input marginals
* setting properties of the sampling in an experimental design
* choosing the surrogate model and its properties
* training the surrogate model on model evaluations in a straightforward manner or iteratively with active learning

The computational model is linked via a ``pylink`` interface.

Multiple post-processing options are available, including the calculation of Sobol' indices, checking the accuracy of the surrogate model and visualizations of the moments of the surrogate.

Bayesian inference can be performed with rejection sampling or MCMC, while taking into account the estimated uncertainty of the data that the (surrogate) model is compared to.
If multiple (surrogate) models are given, they can be compared against each other with pairwise Bayes Factors, model weights or a justifiability analysis.

..
   We split this into the aspects :any:`input_description` and :any:`surrogate_description` to provide insight into the options available in bayesvalidrox.
..
   :any:`post_description` can be applied to trained surrogate models, or using the underlying models themselves.

The next pages lead through the topics given in BayesValidRox and describe the available classes and give brief examples for their use.

.. toctree::
   :maxdepth: 1
   
   input_description
   model_description
   surrogate_description
   al_description
   post_description
   bayes_description
   bmc_description

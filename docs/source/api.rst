API
***
Here you can find the api for the package **bayesvalidrox**.

.. autosummary::
   :toctree: _autosummary 
   :template: custom-module-template.rst
   :recursive:
   
   bayesvalidrox
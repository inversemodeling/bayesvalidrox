Example: Analytical function
****************************
This example shows a surrogate-assisted Bayesian calibration of a time dependent analytical function.
A detailed explanation of this example is provided in the :any:`tutorial`.

Model and Data
==============

.. list-table:: Pylink model
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function
   * - Number of input parameters
     - Set to either 2 or 10
   * - Number of output parameters
     - 1
   * - Time- or space- dependency
     - Time-dependent output, 10 steps in time
   * - MC reference
     - Yes
	 
.. list-table:: Priors
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - 0-10
     - Uniform in (-5,5)
	 
.. list-table:: Discrepancy
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Distribution type
     - Gaussian
   * - Characteristic value
     - variance: data^2
	 
Surrogate
=========

.. list-table:: MetaModel settings
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - surrogate-type
     - aPCE
   * - associated model
     - 'analytical function'
   * - degree choices
     - max degree 12, q-norm truncation 0.85 or 0.5 depending on number of inputs
   * - regression
     - FastARD with bootstrapping
	 
	 
.. list-table:: Training choices
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Static sampling method
     - latin-hypercube
   * - Number of static samples
     - 3*#inputparams
   * - Number of total samples
     - 150
   * - Number of samples per AL iteration
     - 1
   * - AL tradeoff scheme
     - None
   * - AL exploration method
     - random, n_candidates=1000, n_cand_groups=4
   * - AL exploitation method
     - Bayesian Active Design (BAL) with DKL
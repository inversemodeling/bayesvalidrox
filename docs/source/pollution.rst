Example: pollution
******************
This test shows a surrogate-assisted Bayesian calibration of a time dependent
pollution function. Here, the noise will be jointly inferred with the input parameters.

    ENVIRONMENTAL MODEL FUNCTION

    Authors: Sonja Surjanovic, Simon Fraser University
             Derek Bingham, Simon Fraser University
			 
    Questions/Comments: Please email Derek Bingham at dbingham@stat.sfu.ca.

    Copyright 2013. Derek Bingham, Simon Fraser University.

    THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    derivative works, such modified software should be clearly marked.
    Additionally, this program is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; version 2.0 of the License.
    Accordingly, this program is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    For function details and reference information, see:
        http://www.sfu.ca/~ssurjano/
		
This example trains a surrogate and performs Bayesian Inference based on the given data.
Active Learning can be activated and will then be performed with Variance Optimal Design based on Entropy and an epsilon-decreasing tradeoff scheme.

.. note::
   This example contains two ``test_*`` files that can be run.

Model and Data
==============

.. list-table:: Pylink model
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function
   * - Number of input parameters
     - 4
   * - Number of output parameters
     - 1
   * - Time- or space- dependency
     - Yes, ??
   * - MC reference
     - No
	 
.. list-table:: Priors
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - M
     - uniform
   * - D
     - uniform
   * - L
     - uniform
   * - tau
     - uniform
	
Surrogate
=========

.. list-table:: MetaModel settings
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - surrogate-type
     - aPCE
   * - associated model
     - 'pollution'
   * - degree choices
     - max degree 8, q-norm truncation 1.0
   * - regression
     - BCS (Bayesian Compressive Sensing) with  ``'fast'`` bootstrap
	 
	 
.. list-table:: Training choices
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Static sampling method
     - latin-hypercube
   * - Number of static samples
     - 150
   * - Number of total samples
     - 150
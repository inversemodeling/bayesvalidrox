API
***

SURROGATE MODELS
----------------

.. autosummary::
   :toctree: generated
   
   bayesvalidrox.surrogate_models.adaptPlot
   bayesvalidrox.surrogate_models.apoly_construction
   bayesvalidrox.surrogate_models.bayes_linear
   bayesvalidrox.surrogate_models.eval_rec_rule
   bayesvalidrox.surrogate_models.exp_designs
   bayesvalidrox.surrogate_models.exploration
   bayesvalidrox.surrogate_models.glexindex
   bayesvalidrox.surrogate_models.inputs
   bayesvalidrox.surrogate_models.meta_model_engine
   bayesvalidrox.surrogate_models.orthogonal_matching_pursuit
   bayesvalidrox.surrogate_models.reg_fast_ard
   bayesvalidrox.surrogate_models.reg_fast_laplace
   bayesvalidrox.surrogate_models.surrogate_models
   
   
PYLINK
------

.. autosummary::
   :toctree: generated
   
   bayesvalidrox.pylink.pylink
   
   
POST PROCESSING
---------------

.. autosummary::
   :toctree: generated
   
   bayesvalidrox.post_processing.post_processing
   
   
BAYES INFERENCE
---------------

.. autosummary::
   :toctree: generated
   
   bayesvalidrox.bayes_inference.bayes_inference
   bayesvalidrox.bayes_inference.bayes_model_comparison
   bayesvalidrox.bayes_inference.discrepancy
   bayesvalidrox.bayes_inference.mcmc
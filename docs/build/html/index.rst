.. bayesvalidrox documentation master file, created by
   sphinx-quickstart on Wed Dec 13 11:14:59 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Surrogate-assisted Bayesian validation of computational models
==============================================================
**bayesvalidrox** is an open-source python package that provides methods for surrogate modeling, Bayesian inference and model comparison.


An introductory tutorial to the overall workflow with **bayesvalidrox** is provided in :any:`tutorial` and descriptions of the available examples can be found in :any:`examples`.
The functionality and options for the different classes is described more in-depth in :any:`packagedescription` and a list of all the classes and functions is provided in :any:`api`.


Contents
--------

.. toctree::
   :maxdepth: 1
   
   tutorial
   examples
   packagedescription
   api
   
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Example: beam
*************
This example shows how a surrogate for a beam deflection model can be created and
illustrates how a model with an executable and an
input file can be linked with the bayesvalidrox package.

The surrogate is trained without active learning and no inference is performed, though reference data is available.

.. warning::
   This example does not run through without issues at the moment.

Model and Data
==============

.. list-table:: Pylink model
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Runs model via given shell command and parser
   * - Number of input parameters
     - 4
   * - Number of output parameters
     - 1: deflection [m]
   * - Time- or space- dependency
     - ??
   * - MC reference
     - Yes
	 
.. list-table:: Priors
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - Beam width
     - lognormal
   * - Beam height
     - lognormal
   * - Youngs modulus
     - lognormal
   * - Uniform load
     - lognormal
	 
.. list-table:: Discrepancy
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Distribution type
     - Gaussian
   * - Characteristic value
     - variance: data^2
	 
Surrogate
=========

.. list-table:: MetaModel settings
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - surrogate-type
     - PCE
   * - associated model
     - 'Beam9points'
   * - degree choices
     - max degree 6, q-norm truncation 0.75
   * - regression
     - FastARD
	 
	 
.. list-table:: Training choices
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Static sampling method
     - latin-hypercube
   * - Number of static samples
     - 100
   * - Number of total samples
     - 100
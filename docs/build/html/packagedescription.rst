PACKAGE DESCRIPTION
*******************
The package **bayesvalidrox** is split into multiple topics corresponding to the folder structure of the package.
The folder `surrogate_models` contains all the functions and classes to set up the experimental design with priors and create and train the surrogate model.
The computational model is linked via *pylink*.
Postprocessing, Bayesian inference and Bayesian model comparison each also get their own section.


TODO: add image to show folder/file interconnection

Priors, experimental design and surrogates
==========================================
The package **bayesvalidrox** 

Example: model comparison
*************************
This example shows the multi-model comparison.
You will see how to perform a multi-model comparison
	
Provided are three models, a linear models with 2 input parameters, a nonlinear model with 2 input parameters 
and a nonlinear model with 4 input parameters.
The data to base the comparison on is given in an extra file.

.. note:: 
   A detailed explanation of this example will be provided in future as part of the tutorial.

Model 1: L2_model
=================

.. list-table:: Pylink model1
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function (linear)
   * - Number of input parameters
     - 2
   * - Number of output parameters
     - 1
   * - Time- or space- dependency
     - space-dependency
   * - MC reference
     - No
	 
.. list-table:: Priors1
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - 0-2
     - given as correlated samples
	
Model 1: NL2_model
==================

.. list-table:: Pylink model1
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function (exponential)
   * - Number of input parameters
     - 2
   * - Number of output parameters
     - 1
   * - Time- or space- dependency
     - space-dependency
   * - MC reference
     - No
	 
.. list-table:: Priors1
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - 0-2
     - given as correlated samples
	
Model 1: NL4_model
==================

.. list-table:: Pylink model1
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function (cosine)
   * - Number of input parameters
     - 4
   * - Number of output parameters
     - 1
   * - Time- or space- dependency
     - space-dependency
   * - MC reference
     - No
	 
.. list-table:: Priors1
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - 0-4
     - given as correlated samples
	
Surrogates 1-3
==============
All surrogates share the same setup and only differ in the given model.

.. list-table:: MetaModel settings
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - surrogate-type
     - aPCE
   * - associated model
     - see lists above
   * - degree choices
     - 1-12, q-norm truncation 1.0
   * - regression
     - OMP (Orthogonal matching pursuit)
	 
	 
.. list-table:: Training choices
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Static sampling method
     - latin-hypercube
   * - Number of static samples
     - 100
   * - Number of total samples
     - 100
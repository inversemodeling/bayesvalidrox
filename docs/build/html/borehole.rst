Example: borehole
*****************
This test deals with the surrogate modeling of a Borehole function.
You will see how to check the quality of your regression model and perform sensitivity analysis via Sobol Indices
	
	
    BOREHOLE FUNCTION

    Authors: Sonja Surjanovic, Simon Fraser University
             Derek Bingham, Simon Fraser University

    Questions/Comments: Please email Derek Bingham at dbingham@stat.sfu.ca.

    Copyright 2013. Derek Bingham, Simon Fraser University.

    THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    derivative works, such modified software should be clearly marked.
    Additionally, this program is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; version 2.0 of the License.
    Accordingly, this program is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    For function details and reference information, see:
        https://www.sfu.ca/~ssurjano/ishigami.html

no reference data given
Surrogate with AL - OMP for regression and Space-filling sequential exploitaiton scheme (no data)

.. warning::
   Still some error with saving the sobol indices

Model and Data
==============

.. list-table:: Pylink model
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Model type
     - Function
   * - Number of input parameters
     - 8
   * - Number of output parameters
     - 1: flow rate [m$^3$/yr]
   * - Time- or space- dependency
     - ??
   * - MC reference
     - No
	 
.. list-table:: Priors
   :widths: 30 30
   :header-rows: 1
   
   * - Parameter
     - Distribution
   * - r_w
     - uniform
   * - L
     - uniform
   * - K_w
     - uniform
   * - T_u
     - uniform
   * - T_l
     - uniform
   * - H_u
     - uniform
   * - H_l
     - uniform
   * - r
     - lognormal
	
Surrogate
=========

.. list-table:: MetaModel settings
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - surrogate-type
     - aPCE
   * - associated model
     - 'borehole'
   * - degree choices
     - max degree 5, q-norm truncation 1.0
   * - regression
     - OMP (Orthogonal matching pursuit)
	 
	 
.. list-table:: Training choices
   :widths: 30 30
   :header-rows: 1
   
   * - Property
     - Setting
   * - Static sampling method
     - latin-hypercube
   * - Number of static samples
     - 50
   * - Number of total samples
     - 300
   * - Number of samples per AL iteration
     - 1
   * - AL tradeoff scheme
     - None
   * - AL exploration method
     - latin-hypercube, n_candidates=5000, n_cand_groups=4
   * - AL exploitation method
     - space-filling 
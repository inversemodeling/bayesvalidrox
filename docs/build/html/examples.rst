EXAMPLES
********
Within the git repository of **bayesvalidrox** a set of examples can be found.
Here we provide short introductions into the used models and what each example can teach about the functionality of the package.

The following examples are provided:

.. toctree::
   :maxdepth: 1

   Analytical function <analyticalfunction>
   Beam <beam>
   Borehole <borehole>
   Ishigami <ishigami>
   Model comparison <modelcomparison>
   OHagan-function <ohaganfunction>
   Pollution <pollution>
﻿bayesvalidrox.surrogate\_models.reg\_fast\_ard
==============================================

.. automodule:: bayesvalidrox.surrogate_models.reg_fast_ard

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      update_precisions
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      RegressionFastARD
   
   

   
   
   




﻿bayesvalidrox.surrogate\_models.bayes\_linear
=============================================

.. automodule:: bayesvalidrox.surrogate_models.bayes_linear

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      gamma_mean
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      BayesianLinearRegression
      EBLinearRegression
      VBLinearRegression
   
   

   
   
   




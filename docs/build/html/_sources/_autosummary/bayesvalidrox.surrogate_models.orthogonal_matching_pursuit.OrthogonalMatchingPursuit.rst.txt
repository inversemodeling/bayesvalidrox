bayesvalidrox.surrogate\_models.orthogonal\_matching\_pursuit.OrthogonalMatchingPursuit
=======================================================================================

.. currentmodule:: bayesvalidrox.surrogate_models.orthogonal_matching_pursuit

.. autoclass:: OrthogonalMatchingPursuit
   :members:                                   
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~OrthogonalMatchingPursuit.__init__
      ~OrthogonalMatchingPursuit.blockwise_inverse
      ~OrthogonalMatchingPursuit.fit
      ~OrthogonalMatchingPursuit.get_metadata_routing
      ~OrthogonalMatchingPursuit.get_params
      ~OrthogonalMatchingPursuit.loo_error
      ~OrthogonalMatchingPursuit.predict
      ~OrthogonalMatchingPursuit.score
      ~OrthogonalMatchingPursuit.set_params
      ~OrthogonalMatchingPursuit.set_score_request
   
   

   
   
   
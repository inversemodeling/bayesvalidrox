﻿bayesvalidrox.post\_processing.post\_processing.PostProcessing
==============================================================

.. currentmodule:: bayesvalidrox.post_processing.post_processing

.. autoclass:: PostProcessing
   :members:                                   
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~PostProcessing.__init__
      ~PostProcessing.check_accuracy
      ~PostProcessing.check_reg_quality
      ~PostProcessing.plot_metamodel_3d
      ~PostProcessing.plot_moments
      ~PostProcessing.plot_seq_design_diagnostics
      ~PostProcessing.plot_sobol
      ~PostProcessing.sobol_indices
      ~PostProcessing.valid_metamodel
   
   

   
   
   
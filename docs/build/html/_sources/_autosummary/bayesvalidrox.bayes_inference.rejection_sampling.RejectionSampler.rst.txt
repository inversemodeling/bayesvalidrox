bayesvalidrox.bayes\_inference.rejection\_sampling.RejectionSampler
===================================================================

.. currentmodule:: bayesvalidrox.bayes_inference.rejection_sampling

.. autoclass:: RejectionSampler
   :members:                                   
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~RejectionSampler.__init__
      ~RejectionSampler.calculate_loglik_logbme
      ~RejectionSampler.calculate_valid_metrics
      ~RejectionSampler.normpdf
      ~RejectionSampler.run_sampler
   
   

   
   
   
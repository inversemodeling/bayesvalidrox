bayesvalidrox.surrogate\_models.orthogonal\_matching\_pursuit
=============================================================

.. automodule:: bayesvalidrox.surrogate_models.orthogonal_matching_pursuit

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:     
   
      corr
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
      :toctree:     
      :template: custom-class-template.rst  
   
      OrthogonalMatchingPursuit
   
   

   
   
   




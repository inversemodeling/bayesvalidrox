bayesvalidrox.surrogate\_models.gaussian\_process\_sklearn.MySklGPE
===================================================================

.. currentmodule:: bayesvalidrox.surrogate_models.gaussian_process_sklearn

.. autoclass:: MySklGPE
   :members:                                   
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~MySklGPE.__init__
      ~MySklGPE.fit
      ~MySklGPE.get_metadata_routing
      ~MySklGPE.get_params
      ~MySklGPE.log_marginal_likelihood
      ~MySklGPE.predict
      ~MySklGPE.sample_y
      ~MySklGPE.score
      ~MySklGPE.set_params
      ~MySklGPE.set_predict_request
      ~MySklGPE.set_score_request
   
   

   
   
   
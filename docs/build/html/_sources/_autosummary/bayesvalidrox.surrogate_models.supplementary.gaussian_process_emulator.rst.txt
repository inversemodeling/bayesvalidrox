bayesvalidrox.surrogate\_models.supplementary.gaussian\_process\_emulator
=========================================================================

.. currentmodule:: bayesvalidrox.surrogate_models.supplementary

.. autofunction:: gaussian_process_emulator
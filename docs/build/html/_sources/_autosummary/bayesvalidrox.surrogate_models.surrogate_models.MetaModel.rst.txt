bayesvalidrox.surrogate\_models.surrogate\_models.MetaModel
===========================================================

.. currentmodule:: bayesvalidrox.surrogate_models.surrogate_models

.. autoclass:: MetaModel
   :members:                                   
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~MetaModel.__init__
      ~MetaModel.add_InputSpace
      ~MetaModel.build_metamodel
      ~MetaModel.calculate_moments
      ~MetaModel.check_is_gaussian
      ~MetaModel.copy_meta_model_opts
      ~MetaModel.eval_metamodel
      ~MetaModel.fit
      ~MetaModel.pca_transformation
   
   

   
   
   
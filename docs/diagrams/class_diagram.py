# -*- coding: utf-8 -*-
"""
Interaction of classes in `bayesvalidrox`

@author: Rebecca Kohlhaas
"""
import graphviz
from pyUML import Graph, UMLClass

def print_and_save(graph, name):
    #print(graph.to_string())
    graph.write_raw(f"{name}.dot")
    graph.write(f"{name}.png", "dot", "png")

def generate_input_uml():
    # Create UML for the input classes
    graph = Graph('pyUML')
    
    # Add input class
    inputs = UMLClass('Input',
                      attributes={
                              'marginals':'list',
                              'rosenblatt':'bool'
                          },
                      methods=['add_marginals()']
                      )
    graph.add_class(inputs)
    
    # Add marginal class
    marginals = UMLClass('Marginal',
                         attributes={
                                 'name':'string',
                                 'dist_type': 'string',
                                 'parameters': 'list',
                                 'input_data':'array',
                                 'moments': 'list'
                             }
                         )
    graph.add_class(marginals)
    graph.add_composition(marginals, inputs, multiplicity_parent=1, multiplicity_child='1..*')
    
    inputspace = UMLClass('InputSpace',
                          attributes = {
                              'input_obj':'Input',
                              'meta_model_type':'string'
                              },
                          methods = [
                              'check_valid_inputs()', 'init_param_space()', 
                              'build_polytypes()', 'transform()'
                              ]
                          )
    graph.add_class(inputspace)
    graph.add_composition(inputs, inputspace, multiplicity_parent=1, multiplicity_child=1)
    
    expdesign = UMLClass('ExpDesigns',
                         attributes = {
                             'sampling_method': 'string',
                             'hdf5_file': 'string',
                             'n_new_samples': 'int',
                             'n_max_samples': 'int',
                             'mod_loo_threshold': 'double',
                             'tradeoff_scheme':'string',
                             'n_canddidate': 'int',
                             'explore_method':'string',
                             'exploit_method': 'string',
                             'util_func':'string',
                             'n_cand_groups': 'int',
                             'n_replication': 'int',
                             'max_func_itr': 'int',
                             'out_dir': 'string'
                             },
                         methods = [
                             'generate_samples()', 'generate_ed()', 
                             'read_from_file()', 'random_sampler()',
                             'pcm_sampler()', 'plot_samples()'
                             ])
    graph.add_class(expdesign)
    graph.add_implementation(expdesign, inputspace)
    
    print_and_save(graph, 'input_classes')
    
def generate_input_uml_reduced():
    # Create UML for the input classes
    graph = Graph('pyUML')
    
    # Add input class
    inputs = UMLClass('Input',
                      attributes={
                              'marginals':'list',
                              'rosenblatt':'bool'
                          },
                      methods=['add_marginals()']
                      )
    graph.add_class(inputs)
    
    # Add marginal class
    marginals = UMLClass('Marginal',
                         attributes={
                                 'name':'string',
                                 'dist_type': 'string',
                                 'parameters': 'list',
                                 'input_data':'array',
                                 'moments': 'list'
                             }
                         )
    graph.add_class(marginals)
    graph.add_composition(marginals, inputs, multiplicity_parent=1, multiplicity_child='1..*')
    
    inputspace = UMLClass('InputSpace',
                          attributes = {
                              'input_obj':'Input',
                              'meta_model_type':'string'
                              },
                          methods = [
                              'check_valid_inputs()', 'init_param_space()', 
                              'transform()'
                              ]
                          )
    graph.add_class(inputspace)
    graph.add_composition(inputs, inputspace, multiplicity_parent=1, multiplicity_child=1)
    
    expdesign = UMLClass('ExpDesigns',
                         attributes = {
                             'sampling_method': 'string',
                             'hdf5_file': 'string',
                             'out_dir': 'string'
                             },
                         methods = [
                             'generate_samples()', 'generate_ed()', 
                             'read_from_file()', 'random_sampler()',
                             'pcm_sampler()', 'plot_samples()'
                             ])
    graph.add_class(expdesign)
    graph.add_implementation(expdesign, inputspace)
    
    print_and_save(graph, 'input_classes_reduced')
    
def generate_training_uml_reduced():
    """
    Generates the uml for static training and related classes
    """
    graph = Graph('pyUML')
    
    # Add input class
    inputs = UMLClass('Input')
    graph.add_class(inputs)
    inputspace = UMLClass('InputSpace')
    graph.add_class(inputspace)
    
    expdesign = UMLClass('ExpDesigns',
                         attributes = {
                             'sampling_method': 'string',
                             'hdf5_file': 'string',
                             'n_init_samples': 'int',
                             'out_dir': 'string'
                             },
                         methods = [
                             'generate_samples()', 'generate_ed()', 
                             'read_from_file()', 'random_sampler()',
                             'pcm_sampler()', 'plot_samples()'
                             ])
    graph.add_class(expdesign)
    graph.add_composition(inputs, expdesign, multiplicity_parent = 1, multiplicity_child = 1)
    
    model = UMLClass('PyLinkForwardModel')
    graph.add_class(model)
    
    metamod = UMLClass('MetaModel',
                       attributes = {
                           'input_obj':'Input',
                           'meta_model_type': 'string',
                           'pce_reg_method':'string',
                           'bootstrap_method':'string',
                           'n_bootstrap_itrs':'int',
                           'pce_deg':'int',
                           'pce_q_norm':'double',
                           'dim_red_method':'string',
                           'verbose':'bool'
                           },
                       methods = [
                           'build_metamodel()', 'fit()'
                           'add_input_space()','pca_transformation()', 'eval_metamodel()',
                           'copy_meta_model_opts()','_compute_moments()'
                           ]
                       )
    graph.add_class(metamod)
    graph.add_composition(inputs, metamod, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(inputspace, metamod, multiplicity_parent = 1, multiplicity_child = 1)
    
    engine = UMLClass('Engine',
                      attributes = {
                          'meta_model':'MetaModel',
                          'model':'PyLinkForwardModel',
                          'exp_Design':'ExpDesigns',
                          'parallel': 'bool', 
                          'trained':'bool',
                          },
                      methods = [
                          'start_engine()', 'train_normal()',
                          'eval_metamodel()','_valid_error()',
                          '_error_mean_std()'
                          ]
                      )
    graph.add_class(engine)
    graph.add_composition(model, engine, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(expdesign, engine, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(metamod, engine, multiplicity_parent = 1, multiplicity_child = 1)
    
    print_and_save(graph, 'metamod_training_reduced')
    
def generate_model_uml():
    
    graph = Graph('pyUML')
    model = UMLClass('PyLinkForwardModel',
                     attributes = {
                         'link_type':'string', 
                         'name':'string', 
                         'py_file':'string',
                        'func_args':'dict', 
                        'shell_command':'string', 
                        'input_file':'string',
                        'input_template':'string', 
                        'aux_file':'string', 
                        'exe_path':'string',
                        'output_file_names':'list', 
                        'output_names':'list', 
                        'output_parser':'string',
                        'multi_process':'bool', 
                        'n_cpus':'int', 
                        'meas_file':'string',
                        'meas_file_valid':'string', 
                        'mc_ref_file':'string', 
                        'obs_dict':'dict',
                        'obs_dict_valid':'dict', 
                        'mc_ref_dict':'dict'
                         },
                     methods = [
                         'read_observation()', 'read_output()','update_input_params()',
                         'run_command()', 'run_forwardmodel()', 'run_model_parallel()',
                         'um_bridge_model()', '_store_simulations()', 'zip_subdirs()'
                         ]
                     )
    graph.add_class(model)
    print_and_save(graph, 'model')
    
def generate_al_uml_reduced():
    """
    Generates the uml for active learning
    """
    graph = Graph('pyUML')
    
    expdesign = UMLClass('ExpDesigns',
                         attributes = {
                             'sampling_method': 'string',
                             'hdf5_file': 'string',
                             'n_new_samples': 'int',
                             'n_max_samples': 'int',
                             'mod_loo_threshold': 'double',
                             'tradeoff_scheme':'string',
                             'n_canddidate': 'int',
                             'explore_method':'string',
                             'exploit_method': 'string',
                             'util_func':'string',
                             'n_cand_groups': 'int',
                             'n_replication': 'int',
                             'max_func_itr': 'int',
                             'out_dir': 'string'
                             },
                         methods = [
                             'generate_samples()', 'generate_ed()', 
                             'read_from_file()', 'random_sampler()',
                             'pcm_sampler()', 'plot_samples()'
                             ])
    graph.add_class(expdesign)
    
    model = UMLClass('PyLinkForwardModel')
    graph.add_class(model)
    
    metamod = UMLClass('MetaModel')
    graph.add_class(metamod)
    
    engine = UMLClass('Engine',
                      attributes = {
                          'meta_model':'MetaModel',
                          'model':'PyLinkForwardModel',
                          'exp_design':'ExpDesigns',
                          'parallel': 'bool', 
                          'trained':'bool',
                          'discrepancy': 'Discrepancy',
                          },
                      methods = [
                          'start_engine()', 'train_normal()', 'train_sequential()', 
                          'eval_metamodel()', '_bme_calculator()',
                          'plot_posterior()', 'plot_adapt()', '_valid_error()', '_error_mean_std()'
                          ]
                      )
    graph.add_class(engine)
    graph.add_composition(model, engine, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(expdesign, engine, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(metamod, engine, multiplicity_parent = 1, multiplicity_child = 1)
    
    seqdes = UMLClass('SeqDesign',
                      attributes = {
                          'meta_model':'MetaModel',
                          'model':'PyLinkForwardModel',
                          'exp_design':'ExpDesigns',
                          'engine': 'Engine',
                          'discrepancy': 'Discrepancy'
                          },
                      methods = ['choose_next_sample()', 'tradeoff_weights()', 'run_util_func()', 
                          'util_var_based_design()', 
                          'util_bayesian_active_design()', 'util_bayesian_design()', 
                          'dual_annealing()', 
                          'util_alph_opt_design()', '_select_indices()'
                          ]
                      )
    graph.add_class(seqdes)
    graph.add_composition(seqdes, engine, multiplicity_child=1, multiplicity_parent=1)
    
    explor = UMLClass('Exploration',
                      attributes = {
                          'exp_design':'ExpDesigns',
                          'n_candidate':'int',
                          'mc_criterion':'string',
                          },
                      methods=[
                          'get_exploration_samples()','get_vornoi_samples()',
                          'get_mc_samples()', 'approximate_voronoi()', '_build_dist_matrix_point()'
                          ]
                      )
    graph.add_class(explor)
    #graph.add_composition(expdesign, explor, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(explor, seqdes, multiplicity_parent = 1, multiplicity_child = 1)
    
    disc = UMLClass('Discrepancy',
                    attributes={
                        'disc_type': 'str',
                        'parameters': 'dict'
                    },
                    methods = [
                        'build_discrepancy()'
                    ])
    graph.add_class(disc)
    graph.add_composition(disc, engine, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(disc, seqdes, multiplicity_parent = 1, multiplicity_child = 1)
    
    print_and_save(graph, 'active_learning_reduced')
    
def generate_postprocessing_uml():
    graph = Graph('pyUML')
    
    expdesign = UMLClass('ExpDesigns')
    graph.add_class(expdesign)
    
    model = UMLClass('PyLinkForwardModel')
    graph.add_class(model)
    
    metamod = UMLClass('MetaModel')
    graph.add_class(metamod)
    
    engine = UMLClass('Engine')
    graph.add_class(engine)
    graph.add_composition(model, engine, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(expdesign, engine, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(metamod, engine, multiplicity_parent = 1, multiplicity_child = 1)
    
    post = UMLClass('PostProcessing',
                    attributes = {
                        'engine':'Engine',
                        'name':'string',
                        'out_dir':'string' # TODO: this is somehow not in the code anymore??
                        },
                    methods = [
                        'plot_moments()', 'valid_metamodel()', 'check_accuracy()',
                        'plot_seq_design_diagnostics()', 'sobol_indices()', 
                        'check_reg_quality()', 'eval_pce_model_3d()', 'calculate_pce_moments()',
                        '_get_sample()', '_eval_model()', '_plot_validation()', '_plot_validation_multi()'
                        ]
                    )
    graph.add_class(post)
    graph.add_composition(engine, post, multiplicity_parent = 1, multiplicity_child = 1)
    
    print_and_save(graph, 'postprocessing')
    
def generate_sampler_uml():
    graph = Graph('pyUML')

    engine = UMLClass('Engine')
    graph.add_class(engine)
    
    disc = UMLClass('Discrepancy')
    graph.add_class(disc)

    postsamp = UMLClass('PostSampler',
                        attributes={
                            'engine': 'Engine',
                            'discrepancy': 'Discrepancy',
                            'observation': 'dict',
                            'out_names': 'list',
                            'selected_indices':'list',
                            'use_emulator': 'bool',
                            'out_dir': 'str'
                        },
                        methods = [
                            'run_sampler()',
                            'normpdf()',
                            '_corr_factor_bme()',
                            'calculate_loglik_logbme()'
                        ])
    graph.add_class(postsamp)
    graph.add_composition(engine, postsamp, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(disc, postsamp, multiplicity_parent = 1, multiplicity_child = 1)

    rejsam = UMLClass('RejectionSampler',
                      attributes={
                            'prior_samples':'array'
                      },
                      methods=[
                            'calculate_valid_metrics()'                           
                      ])
    graph.add_class(rejsam)
    graph.add_implementation(rejsam, postsamp)

    mcmc = UMLClass('MCMC',
                      attributes={
                          'mcmc_params':'dict'
                      },
                      methods=[  
                          'log_prior()',
                          'log_likelihood()',
                          'log_posterior()',
                          'eval_model()',
                          'is_valid_move()'
                      ])
    graph.add_class(mcmc)
    graph.add_implementation(mcmc, postsamp)

    print_and_save(graph, 'sampler')

def generate_bayes_uml():
    graph = Graph('pyUML')

    sampler = UMLClass('PostSampler')
    graph.add_class(sampler)
    
    engine = UMLClass('Engine')
    graph.add_class(engine)
    
    disc = UMLClass('Discrepancy')
    graph.add_class(disc)
    
    bayesinf = UMLClass('BayesInference',
                        attributes = {
                            'engine':'Engine',
                            'discrepancy':'Discrepancy',
                            'use_emulator':'bool',
                            'name':'str',
                            'out_names':'list',
                            'selected_indices':'list',
                            'prior_samples':'array',
                            'n_prior_samples':'int',
                            'measured_data':'dict',
                            'inference_method':'str',
                            'mcmc_params':'dict',
                            'bootstrap_method':'str',
                            'n_bootstrap_itrs':'int',
                            'perturbed_data':'list',
                            'bootstrap_noise':'double',
                            'valid_metrics':'list',
                            'plot':'bool',
                            'max_a_posteriori':'string',
                            'out_dir':'string'
                            },
                        methods = [
                            'setup()','run_inference()','run_validation()',
                            'get_surr_error()', 'perturb_data()', 'calculate_loglik_logbme()',
                            'calculate_valid_metrics()', 'posterior_predictive()', 'write_as_hdf5()',
                            'plot_max_a_posteriori()','plot_post_params()', 'plot_logbme()',
                            'plot_post_predictive()'
                            ]
                        )
    graph.add_class(bayesinf)
    graph.add_composition(engine, bayesinf, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(disc, bayesinf, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(sampler, bayesinf, multiplicity_parent = 1, multiplicity_child = 1)
    
    print_and_save(graph, 'bayesian_validation')
                        
def generate_bmc_uml():
    graph = Graph('pyUML')
    
    engine = UMLClass('Engine')
    graph.add_class(engine)
    
    disc = UMLClass('Discrepancy')
    graph.add_class(disc)
    
    bayesinf = UMLClass('BayesInference')
    graph.add_class(bayesinf)
    graph.add_composition(disc, bayesinf, multiplicity_parent = 1, multiplicity_child = 1)
    
    # BMC
    bmc = UMLClass('BayesianModelComparison',
                   attributes = {'model_dict':'dict', 
                                 'bayes_opts':'dict', 
                                 'perturbed_data':'array',
                                 'n_bootstrap':'int', 
                                 'data_noise_level':'float',
                                 'use_emulator':'bool', 
                                 'out_dir':'string'
                                 },
                   methods = ['setup()', 'model_comparison_all()','calc_bayes_factors',
                              'calc_model_weights()', 'calc_justifiability_analysis()',
                              'generate_ja_dataset()', 'plot_confusion_matrix()',
                              'plot_model_weights()', 'plot_bayes_factor()']
                   )
    graph.add_class(bmc)
    graph.add_composition(bayesinf, bmc, multiplicity_parent=1, multiplicity_child='1..*')
    graph.add_composition(engine, bmc, multiplicity_parent=1, multiplicity_child='1..*')
    
    print_and_save(graph, 'bayesian_model_comparison')
                        
def generate_metamod_uml():
    
    graph = Graph('pyUML')
    
    # Add input class
    inputs = UMLClass('Input')
    graph.add_class(inputs)
    inputspace = UMLClass('InputSpace',
                          methods = ['build_polytypes()'])
    graph.add_class(inputspace)
    
    metamod = UMLClass('MetaModel',
                       attributes = {
                           'input_obj':'Input',
                           'meta_model_type': 'string',
                           'is_gaussian' : 'bool',
                           'bootstrap_method':'string',
                           'n_bootstrap_itrs':'int',
                           'dim_red_method':'string',
                           'verbose':'bool'
                           },
                       methods = [
                           'check_is_gaussian()', 'build_metamodel()', 'fit()',
                           'pca_transformation()', 'eval_metamodel()',
                           'calculate_moments()',
                           'copy_meta_model_opts()','add_InputSpace()'
                           ]
                       )
    graph.add_class(metamod)
    graph.add_composition(inputs, metamod, multiplicity_parent = 1, multiplicity_child = 1)
    graph.add_composition(inputspace, metamod, multiplicity_parent = 1, multiplicity_child = 1)
    
    
    pce = UMLClass('PCE',
                       attributes = {
                           'pce_reg_method':'string',
                           'pce_deg':'int',
                           'pce_q_norm':'double'
                           },
                       methods = [
                           'update_pce_coeffs()', 'univ_basis_vals()', 'regression()',
                           'adaptive_regression()', '_select_degree()', 
                           'generate_polynomials()'
                           ]
                       )
    graph.add_class(pce)
    graph.add_implementation(pce, metamod)
    
    # Add the regression Classes for PCE
    Breg = UMLClass('BayesianLinearRegression',
                      attributes = {
                          'n_iter':'int',
                          'tol': 'double',
                          'fit_intercept':'bool'
                          },
                      methods = [
                          '_check_convergence()', '_center_data()', 'predict_dist()'
                          ]
                      )
    graph.add_class(Breg)
    vBreg = UMLClass('VBLinearRegression',
                      methods = ['fit()', 'predict()']
                      )
    graph.add_class(vBreg)
    eBreg = UMLClass('EBLinearRegression',
                      methods = ['fit()', 'predict()']
                      )
    graph.add_class(eBreg)
    omp = UMLClass('OrthogonalMatchingPursuit',
                      attributes = {'fit_intercept':'bool'},
                      methods = ['fit()', 'predict()']
                      )
    graph.add_class(omp)
    regArd = UMLClass('RegressionFastARD',
                      attributes = {
                          'n_iter':'int',
                          'tol':'double',
                          'fit_intercept':'bool',
                          'normalize':'bool',
                          'compute_score':'bool'
                          },
                      methods = ['fit()', 'predict()']
                      )
    graph.add_class(regArd)
    regLap = UMLClass('RegressionFastLaplace',
                      attributes = {
                          'n_iter':'int',
                          'tol':'double',
                          'fit_intercept':'bool',
                          'bias_term':'bool'
                          },
                      methods = ['fit()', 'predict()']
                      )
    graph.add_class(regLap)
    graph.add_implementation(vBreg, Breg)
    graph.add_implementation(eBreg, Breg)
    graph.add_composition(vBreg, pce, multiplicity_parent = 1, multiplicity_child = '1..*')
    graph.add_composition(eBreg, pce, multiplicity_parent = 1, multiplicity_child = '1..*')
    graph.add_composition(omp, pce, multiplicity_parent = 1, multiplicity_child = '1..*')
    graph.add_composition(regArd, pce, multiplicity_parent = 1, multiplicity_child = '1..*')
    graph.add_composition(regLap, pce, multiplicity_parent = 1, multiplicity_child = '1..*')
    
    
    gpe = UMLClass('GPESkl',
                       attributes = {
                           'gpe_reg_method':'string',
                           'auto_select':'bool',
                           'kernel_type':'string',
                           'isotropy' : 'bool',
                           'noisy' : 'bool',
                           'nugget' : 'float'
                           },
                       methods = [
                           'build_kernels()', 'adaptive_regression()', 
                           'transform_x()', 'scale_x()'
                           ]
                       )
    graph.add_class(gpe)
    graph.add_implementation(gpe, metamod)
    
    print_and_save(graph, 'metamodel_classes')

if __name__ == '__main__':
    
    if 1:
        generate_input_uml()
        generate_input_uml_reduced()
        generate_model_uml()
        generate_training_uml_reduced()
        generate_al_uml_reduced()
        generate_postprocessing_uml()
        generate_sampler_uml()
        generate_bayes_uml()
        generate_bmc_uml()
    
    generate_metamod_uml()
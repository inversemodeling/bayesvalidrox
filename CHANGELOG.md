# CHANGELOG

## [2.0.0] (2025-02-05)
### Requirements
* Set `matplotlib` version back to 3.7.3 to ensure stability in combination with seaborn

### Added
Features
* `caculate_moments` method in `Meta_Model` class for a general calculation of the moments 
* `PCEGPR` class for combining polynomial chaos and gaussian processes 

Tests
* Tests for `GPE` class 
* Tests for `calculate_moments` method in `Meta_Model` and `GPESkl` classes 
* Tests for `PCEGPR` class 
* Tests for `PostProcessing`class

Examples
* Example `analytical_function_pcegpr.py` to show application of the combined class `PCEGPR`

### Changed 
* Split surrogate classes into template class `MetaModel` and child classes `PCE` and `GPESklearn`
* Changed Exception format in `GPE` class to more specific Exceptions 
* Extend `Inputs.add_marginals` to allow direct setting of marginal properties
* Move Sobol' calculation to `PCE.calculate_sobol`
* Clean `PostProcessing` class and update for general `MetaModel` objects
* Split `BayesInference`class into `BayesInference`, template class `PostSampler` and child class `RejectionSampler`
* Class `MCMC` child class of `PostSampler`
* Split `BayesInference.create_inference()` into `BayesInference.run_inference()`and `BayesInference.run_validation()`
* Plot in `PCE.adaptive_regression` is saved automatically
* Moved `adaptPlot()` to be `Engine.plot_adapt()`

Renaming
* Renamed attributes of classes `BayesInference`, `BMC`, `MCMC`, `SeqDesign` and `Engine` following from structure reworking
* Renamed attribues and functions to snake-style naming: 
    * `*.Discrepancy` to `*.discrepancy`
    * `surrogate_models.py` to `meta_model.py` 
    * `*.MetaModel` to `*.meta_model`
    * `*.ExpDesign` to `*.exp_design`
    * `*.Model` to `*.model`
    * `*LCerror` to `*lc_error`
    * `*.InputSpace` to `*.input_space`
    * `*.Marginals` to `*.marginals`
    * `*.Rosenblatt` to `*.rosenblatt`
    * `*.add_InputSpace` to `*.add_input_space`
    * `*.InputObj` to `*.input_object`
    * `*.JDist` to `*.j_dist`
    * `._autoSelect` to `*._auto_select`
    * `ExpDesigns.generate_ED` to `ExpDesigns.generate_ed`
    * `ExpDesigns.X` to `ExpDesigns.x`
    * `ExpDesigns.Y` to `ExpDesigns.y`
    * `PyLinkForwardModel.Output` to `PyLinkForwardModel.output`
* Class `auto_vivification` to `AutoVivification`

Bug fixes
* `ExpDesign` always uses user-defined samples when given
* `ExpDesign` n_init_samples can be user-defined
* Fixed pce-loocv calculation for few training samples
* Two-step coefficient estimation for `LARS` PCE training options

### Removed
* Input `n_samples` for `ExpDesign.generate_ED()`
* Reduced `Discrepancy` options to only `total_sigma2`

## [1.1.0]
### Requirements
* numpy >= 1.23.5

### Added
Features
* class `SeqDesign` for sequential training
* `Engine` can be built without a surrogate
* `BayesInference` and `BayesModelComparison` can be performed on an `Engine` object without a surrogate

Examples
* Example `user_guide` to go along with the user guide on the website
* Example `principal_component_analysis` to show application of pca on metamodel outputs
* Example 'only_model' for use of inference and model comparison without a metamodel

### Changed
* Moved functions for sequential training from `Engine` to `SeqDesign`
* Moved `hellinger_distance`, `logpdf`, `subdomain` into `surrogate_models/seq_design`
* Early stop in `BayesInf` for improved performance of `BayesModelComp`
* Allow singular matrices in exploitation with `BayesActDesign` 
* `ExpDesign.generate_ED` no longer needs `transform`

Bug fixes
* Import of `ExpDesign` allowed
* Images in `PostProcessing` only saved, not opened
* Fixed option `MetaModel.dim_red_method = 'pca'`


### Removed
* Disabled exploration with `voronoi`
* `BayesModelComp.just_n_meas`

## [1.0.0]
### Requirements
* numpy now at 1.23.3
* ....

### Added
Features
* PyLinkForwardModel has new link_type 'umbridge' for UM-Bridge type models
* class `InputSpace` as parent class to `ExpDesigns`
* `MetaModel` has option to train with OLS+constraints

Examples
* Example `umbridge_tsunamitutorial` to show two options of using UM-Bridge type models
* Example `convergence_tests` for the constraints

### Changed
General
* Independent functions moved out of classes
* Constructors of `PostProcessing`, `BayesInference`, `BayesModelComp` are to be given the engine instead of the metamodel
* Constructor of `Exploration` to be given `ExpDesigns` instead of `MetaModel`

Engine
* `MetaModelEngine` renamed to `Engine`
* Split `run()` into `train_normal()` and `train_sequential()`
* `opt_SeqDesign()` renamed to `choose_next_sample()`

MetaModel
* `MetaModel` fully independent of model
* `MetaModel` uses `InputSpace` instead of `ExpDesigns`
* `create_metamodel()` split into `build_metamodel()` and `fit()`
* `fit()` renamed to `regression()`
* `eval_metamodel()` only runs on given samples, full functionality retained in `Engine.eval_metamodel()`
* Polynomial related functions moved from `MetaModelEngine` to `MetaModel`

PyLinkForwardModel
* Read-in of MC-references now performed by `PyLinkForwardModel.read_observation()`

ExpDesigns
* Parameters related to sequential training were moved from `MetaModel` to `ExpDesigns` (`Seq*`, `valid_model_runs`)
 
### Removed
* Class `SequentialDesign`
* `ExpDesigns.method` 
* `MetaModel.create_basis_indices()`



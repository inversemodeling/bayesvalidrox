#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a nonlinear cosine model.

The code for this numerical experiments is available at
https://github.com/MichaelSinsbeck/paper_sequential-design-model-selection.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS),
University of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Fri Oct 8 2021

"""
import numpy as np


def NL4_model(xx):
    """
    Nonlinear model y = a*cos(b*x+c)+d

    Models adapted from Anneli Guthke's paper:
        ch€oniger, A., T. W€ohling, L. Samaniego,and W. Nowak (2014), Model
        selection on solid ground: Rigorous comparison ofnine ways to evaluate
        Bayesian modelevidence,Water Resour. Res.,50,9484–9513,
        doi:10.1002/2014WR016062

    Parameters
    ----------
    xx : array
        Parameters a and b.

    Returns
    -------
    2D-array
        The first row contains the measurement locations.
        The second row contains the model outputs.

    """
    n_output = 15
    meas_loc = np.linspace(0.25, 4.75, n_output)

    # NL4_model
    NL4_model = xx[:, 0] * np.cos(xx[:, 1] * meas_loc + xx[:, 2]) + xx[:, 3]

    # Output
    output = {
        'x_values': meas_loc,
        'Z': NL4_model
        }

    return output

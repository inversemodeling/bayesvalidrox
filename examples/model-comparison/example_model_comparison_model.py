#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows a surrogate-assisted Bayesian calibration of a time dependent
    analytical function.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Fri Aug 9 2019

"""
import numpy as np
import pandas as pd
import scipy.io as io
import sys
import joblib
# import bayesvalidrox
# Add BayesValidRox path
sys.path.append("../../src/")

from bayesvalidrox import PyLinkForwardModel, Input, ExpDesigns, BayesInference, Discrepancy, Engine, BayesModelComparison

from bayesvalidrox.bayes_inference.bayes_model_comparison import BayesModelComparison
import matplotlib
matplotlib.use('agg')


if __name__ == "__main__":

    # =====================================================
    # ========  Model Comparison without Emulator ==========
    # =====================================================
    sigma = 0.6
    data = {
        'x [m]': np.linspace(0.25, 4.75, 15),
        'Z': io.loadmat('data/synth_data.mat')['d'].reshape(1, -1)[0]
        }

    # =====================================================
    # ============   COMPUTATIONAL MODELS  ================
    # =====================================================
    # Define the models options
    # ---------- Linear model  -------------
    model_l2 = PyLinkForwardModel()

    model_l2.link_type = 'Function'
    model_l2.py_file = 'L2_model'
    model_l2.name = 'linear'
    model_l2.output.names = ['Z']
    model_l2.observations_valid = data

    # -- Nonlinear exponential model -------
    model_nl2 = PyLinkForwardModel()

    model_nl2.link_type = 'Function'
    model_nl2.py_file = 'NL2_model'
    model_nl2.name = 'exponential'
    model_nl2.output.names = ['Z']
    model_nl2.observations_valid = data

    # ------ Nonlinear cosine model ---------
    # Data generating process
    model_nl4 = PyLinkForwardModel()

    model_nl4.link_type = 'Function'
    model_nl4.py_file = 'NL4_model'
    model_nl4.name = 'cosine'
    model_nl4.output.names = ['Z']
    model_nl4.observations_valid = data

    # =====================================================
    # =========   PROBABILISTIC INPUT MODEL  ==============
    # =====================================================
    # Define model inputs
    n_sample = 10000
    # ---------- Linear model  -------------
    inputs_l2 = Input()
    prior_mean_l2 = np.array([1, 0])
    prior_cov_l2 = np.array(
        [[0.04, -0.007],
         [-0.007, 0.04]]
        )
    params_l2 = np.random.multivariate_normal(
        prior_mean_l2, prior_cov_l2, size=n_sample
        )

    for i in range(params_l2.shape[1]):
        inputs_l2.add_marginals()
        inputs_l2.marginals[i].name = f'$X_{i+1}$'
        inputs_l2.marginals[i].input_data = params_l2[:, i]

    # ------ Nonlinear exponential model ---------
    inputs_nl2 = Input()
    prior_mean_nl2 = np.array([0.4, -0.3])
    prior_cov_nl2 = np.array(
        [[0.003, -0.0001],
         [-0.0001, 0.03]]
        )
    params_nl2 = np.random.multivariate_normal(
        prior_mean_nl2, prior_cov_l2, size=n_sample
        )

    for i in range(params_nl2.shape[1]):
        inputs_nl2.add_marginals()
        inputs_nl2.marginals[i].name = f'$X_{i+1}$'
        inputs_nl2.marginals[i].input_data = params_nl2[:, i]

    # ------ Nonlinear cosine model ---------
    inputs_nl4 = Input()
    prior_mean_nl4 = np.array([2.6, 0.5, -2.8, 2.3])
    prior_cov_nl4 = np.array(
        [[0.46, -0.07, 0.24, -0.14],
         [-0.07, 0.04, -0.05, 0.02],
         [0.24, -0.05, 0.30, -0.16],
         [-0.14, 0.02, -0.16, 0.30]]
        )
    params_nl4 = np.random.multivariate_normal(
        prior_mean_nl4, prior_cov_nl4, size=n_sample
        )

    for i in range(params_nl4.shape[1]):
        inputs_nl4.add_marginals()
        inputs_nl4.marginals[i].name = f'$X_{i+1}$'
        inputs_nl4.marginals[i].input_data = params_nl4[:, i]

    # ------------------------------------------------
    # ------ Experimental Design Configuration -------
    # ------------------------------------------------
    #L2_MetaModelOpts.add_ExpDesign()
    exp_design_l2 = ExpDesigns(inputs_l2)
    exp_design_l2.n_init_samples = 100
    exp_design_l2.sampling_method = 'latin_hypercube'

    # ------ Nonlinear cosine model ---------
    exp_design_nl2 = ExpDesigns(inputs_nl2)
    exp_design_nl2.n_init_samples = 100
    exp_design_nl2.sampling_method = 'latin_hypercube'

    # ------ Nonlinear cosine model ---------
    exp_design_nl4 = ExpDesigns(inputs_nl4)
    exp_design_nl4.n_init_samples = 100
    exp_design_nl4.sampling_method = 'latin_hypercube'

    # >>>>>> Train the Surrogates <<<<<<<<<<<
    engine_l2 = Engine(None, model_l2, exp_design_l2)
    engine_l2.train_normal()
    engine_nl2 = Engine(None, model_nl2, exp_design_nl2)
    engine_nl2.train_normal()
    engine_nl4 = Engine(None, model_nl4, exp_design_nl4)
    engine_nl4.train_normal()
    
    # =====================================================
    # =========  BAYESIAN MULTIMODEL COMPARISON ===========
    # =====================================================
    # ----- Define the discrepancy model -------
    sigma = np.ones(15) * np.array(sigma).flatten()
    discrepancy = Discrepancy('')
    discrepancy.type = 'Gaussian'
    discrepancy.parameters = pd.DataFrame(sigma**2, columns=['Z'])

    # ----- Define the options model -------
    meta_models = {
        "linear": engine_l2,
        "exponential": engine_nl2,
        "cosine": engine_nl4
        }

    # BME Bootstrap options
    opts_bayes = {
        "bootstrap_method": 'normal',
        "n_samples": 10,
        "discrepancy": discrepancy,
        "plot": False
        }

    # Run model comparison
    bmc = BayesModelComparison(meta_models, opts_bayes, use_emulator=False,n_bootstrap=10)
    output_dict = bmc.model_comparison_all()

    # Save the results
    with open('model_comparison_output_dict.pkl', 'wb') as output:
        joblib.dump(bmc, output, 2)

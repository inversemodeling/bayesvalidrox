#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows the multi-model comparison.

You will see how to:
    Perform a multi-model comparison

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS),
University of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

"""

from copy import deepcopy
import numpy as np
import scipy.io as io
import pandas as pd
import joblib
import sys
#sys.path.append("../../src/bayesvalidrox/")
sys.path.append("../../src/")

from bayesvalidrox import PyLinkForwardModel, Input, ExpDesigns, PCE, PostProcessing, Discrepancy, BayesModelComparison, Engine


if __name__ == "__main__":
    # Read data
    sigma = 0.6
    data = {
        'x [m]': np.linspace(0.25, 4.75, 15),
        'Z': io.loadmat('data/synth_data.mat')['d'].reshape(1, -1)[0]
        }
    RUN = False
    # =====================================================
    # ============   COMPUTATIONAL MODELS  ================
    # =====================================================
    # Define the models options
    # ---------- Linear model  -------------
    model_l2 = PyLinkForwardModel()

    model_l2.link_type = 'Function'
    model_l2.py_file = 'L2_model'
    model_l2.name = 'linear'
    model_l2.output.names = ['Z']
    model_l2.observations_valid = data

    # -- Nonlinear exponential model -------
    model_nl2 = PyLinkForwardModel()

    model_nl2.link_type = 'Function'
    model_nl2.py_file = 'NL2_model'
    model_nl2.name = 'exponential'
    model_nl2.output.names = ['Z']
    model_nl2.observations_valid = data

    # ------ Nonlinear cosine model ---------
    # Data generating process
    model_nl4 = PyLinkForwardModel()

    model_nl4.link_type = 'Function'
    model_nl4.py_file = 'NL4_model'
    model_nl4.name = 'cosine'
    model_nl4.output.names = ['Z']
    model_nl4.observations_valid = data

    if RUN:
        # =====================================================
        # =========   PROBABILISTIC INPUT MODEL  ==============
        # =====================================================
        # Define model inputs
        n_sample = 10000
        # ---------- Linear model  -------------
        inputs_l2 = Input()
        prior_mean_l2 = np.array([1, 0])
        prior_cov_l2 = np.array(
            [[0.04, -0.007],
            [-0.007, 0.04]]
            )
        params_l2 = np.random.multivariate_normal(
            prior_mean_l2, prior_cov_l2, size=n_sample
            )

        for i in range(params_l2.shape[1]):
            inputs_l2.add_marginals()
            inputs_l2.marginals[i].name = f'$X_{i+1}$'
            inputs_l2.marginals[i].input_data = params_l2[:, i]

        # ------ Nonlinear exponential model ---------
        inputs_nl2 = Input()
        prior_mean_nl2 = np.array([0.4, -0.3])
        prior_cov_nl2 = np.array(
            [[0.003, -0.0001],
            [-0.0001, 0.03]]
            )
        params_nl2 = np.random.multivariate_normal(
            prior_mean_nl2, prior_cov_l2, size=n_sample
            )

        for i in range(params_nl2.shape[1]):
            inputs_nl2.add_marginals()
            inputs_nl2.marginals[i].name = f'$X_{i+1}$'
            inputs_nl2.marginals[i].input_data = params_nl2[:, i]

        # ------ Nonlinear cosine model ---------
        inputs_nl4 = Input()
        prior_mean_nl4 = np.array([2.6, 0.5, -2.8, 2.3])
        prior_cov_nl4 = np.array(
            [[0.46, -0.07, 0.24, -0.14],
            [-0.07, 0.04, -0.05, 0.02],
            [0.24, -0.05, 0.30, -0.16],
            [-0.14, 0.02, -0.16, 0.30]]
            )
        params_nl4 = np.random.multivariate_normal(
            prior_mean_nl4, prior_cov_nl4, size=n_sample
            )

        for i in range(params_nl4.shape[1]):
            inputs_nl4.add_marginals()
            inputs_nl4.marginals[i].name = f'$X_{i+1}$'
            inputs_nl4.marginals[i].input_data = params_nl4[:, i]

        # =====================================================
        # ======  POLYNOMIAL CHAOS EXPANSION METAMODELS  ======
        # =====================================================
        # Define meta-model options
        # ---------- Linear model  -------------
        meta_model_l2 = PCE(inputs_l2)

        # Select if you want to preserve the spatial/temporal depencencies
        # meta_model_l2.dim_red_method = 'PCA'
        # MetaModelOpts.var_pca_threshold = 99.999
        # MetaModelOpts.n_pca_components = 10 #3, 10

        # Select your metamodel method
        meta_model_l2.meta_model_type = 'aPCE'

        # ------------------------------------------------
        # ------------- PCE Specification ----------------
        # ------------------------------------------------
        # Select the sparse least-square minimization method for
        # the PCE coefficients calculation:
        # 1)OLS: Ordinary Least Square  2)BRR: Bayesian Ridge Regression
        # 3)LARS: Least angle regression  4)ARD: Bayesian ARD Regression
        # 5)FastARD: Fast Bayesian ARD Regression
        # 6)VBL: Variational Bayesian Learning
        # 7)EBL: Emperical Bayesian Learning
        meta_model_l2.pce_reg_method = 'FastARD'

        # Specify the max degree to be compared by the adaptive algorithm:
        # The degree with the lowest Leave-One-Out cross-validation (LOO)
        # error (or the highest score=1-LOO)estimator is chosen as the final
        # metamodel. pce_deg accepts degree as a scalar or a range.
        meta_model_l2.pce_deg = np.arange(1, 12)

        # q-quasi-norm 0<q<1 (default=1)
        meta_model_l2.pce_q_norm = 1.0

        # ------------------------------------------------
        # ------ Experimental Design Configuration -------
        # ------------------------------------------------
        #meta_model_l2.add_ExpDesign()
        exp_design_l2 = ExpDesigns(inputs_l2)
        exp_design_l2.n_init_samples = 100
        exp_design_l2.sampling_method = 'latin_hypercube'

        # ------ Nonlinear cosine model ---------
        exp_design_nl2 = ExpDesigns(inputs_nl2)
        exp_design_nl2.n_init_samples = 100
        exp_design_nl2.sampling_method = 'latin_hypercube'
        meta_model_nl2 = deepcopy(meta_model_l2)
        meta_model_nl2.input_obj = inputs_nl2

        # ------ Nonlinear cosine model ---------
        meta_model_nl4 = deepcopy(meta_model_l2)
        meta_model_nl4.input_obj = inputs_nl4
        exp_design_nl4 = ExpDesigns(inputs_nl4)
        exp_design_nl4.n_init_samples = 100
        exp_design_nl4.sampling_method = 'latin_hypercube'

        # >>>>>> Train the Surrogates <<<<<<<<<<<
        engine_l2 = Engine(meta_model_l2, model_l2, exp_design_l2)
        engine_l2.train_normal()
        engine_nl2 = Engine(meta_model_nl2, model_nl2, exp_design_nl2)
        engine_nl2.train_normal()
        engine_nl4 = Engine(meta_model_nl4, model_nl4, exp_design_nl4)
        engine_nl4.train_normal()

        # =====================================================
        # =========  POST PROCESSING OF METAMODELS  ===========
        # =====================================================
        # ---------- Linear model  -------------
        
        post_l2 = PostProcessing(engine_l2, name=model_l2.name)

        # Plot to check validation visually.
        post_l2.valid_metamodel(n_samples=3)

        # Plot moments
        post_l2.plot_moments()

        # Compute and print RMSE error
        post_l2.check_accuracy(n_samples=3000)

        # Plot the sobol indices
        total_sobol = post_l2.sobol_indices()

        # ---------- Linear model  -------------
        post_nl2 = PostProcessing(engine_nl2, name=model_nl2.name)

        # Plot to check validation visually.
        post_nl2.valid_metamodel(n_samples=3)

        # Plot moments
        post_nl2.plot_moments()

        # Compute and print RMSE error
        post_nl2.check_accuracy(n_samples=3000)

        # Plot the sobol indices
        total_sobol = post_nl2.sobol_indices()

        # ------ Nonlinear cosine model ---------
        post_nl4 = PostProcessing(engine_nl4, name=model_nl4.name)

        # Plot to check validation visually.
        post_nl4.valid_metamodel(n_samples=3)

        # Plot moments
        post_nl4.plot_moments()

        # Compute and print RMSE error
        post_nl4.check_accuracy(n_samples=3000)

        # Plot the sobol indices
        total_sobol = post_nl4.sobol_indices()
        
        with open("engine_l2.pkl", "wb") as output:
            joblib.dump(engine_l2, output, 2)
        with open("engine_nl2.pkl", "wb") as output:
            joblib.dump(engine_nl2, output, 2)
        with open("engine_nl4.pkl", "wb") as output:
            joblib.dump(engine_nl4, output, 2)

    with open("engine_l2.pkl", "rb") as input_:
        engine_l2 = joblib.load(input_)
    with open("engine_nl2.pkl", "rb") as input_:
        engine_nl2 = joblib.load(input_)
    with open("engine_nl4.pkl", "rb") as input_:
        engine_nl4 = joblib.load(input_)

    # =====================================================
    # =========  BAYESIAN MULTIMODEL COMPARISON ===========
    # =====================================================
    # ----- Define the discrepancy model -------
    sigma = np.ones(15) * np.array(sigma).flatten()
    discrepancy = Discrepancy(disc_type='Gaussian', 
                                  parameters = pd.DataFrame(sigma**2, columns=['Z']))

    # ----- Define the options model -------
    meta_models = {
        "linear": engine_l2,
        "exponential": engine_nl2,
        "cosine": engine_nl4
        }

    # BME Bootstrap options
    bayes_opts = {
        "bootstrap_method": 'normal',
        "discrepancy": discrepancy,
        "plot": False
        }

    # Run model comparison
    bmc = BayesModelComparison(
        model_dict=meta_models,
        bayes_opts=bayes_opts,
        use_emulator=True,
        n_bootstrap=100
        )
    output_dict = bmc.model_comparison_all()

    # Save the results
    with open('bmc.pkl', 'wb') as output:
        joblib.dump(bmc, output, 2)

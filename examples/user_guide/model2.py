# -*- coding: utf-8 -*-
"""
A simple model example for the BVR user guide.

@author: Rebecca Kohlhaas
"""
import numpy as np

def model2(samples, x_values):
    poly = samples[0]*np.power(x_values, 3)
    outputs = {'A': poly, 'x_values': x_values}
    return outputs
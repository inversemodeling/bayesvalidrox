# -*- coding: utf-8 -*-
"""
Code that goes along with the 'user guide' in the BVR docs.

@author: Rebecca Kohlhaas
"""

import copy
import numpy as np
import pandas as pd
import sys
import joblib

import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

# Add BayesValidRox path
sys.path.append("../../src/")
from bayesvalidrox import Input, ExpDesigns, PyLinkForwardModel, PCE, Engine, PostProcessing, Discrepancy, BayesInference, BayesModelComparison

if __name__ == '__main__':
    #### Priors, input space and experimental design
    version = 1
    inputs = Input()
    
    # Version 1
    if version == 1:
        inputs.add_marginals(name='$X$', dist_type ='unif', parameters=[-5,5])
        
    # Version 2
    if version == 2:
        inputParams = np.random.uniform(-5,5,100)
        inputs.add_marginals(name='$X$', input_data=inputParams)

    # Create ExpDesign and generate samples
    exp_design = ExpDesigns(inputs)
    exp_design.sampling_method = 'latin_hypercube'
    samples = exp_design.generate_samples(10)
    exp_design.plot_samples(samples)
    
    #### Models
    x_values = np.arange(0,1,0.1)
    model = PyLinkForwardModel()
    model.link_type = 'Function'
    model.py_file = 'model'
    model.name = 'model'
    model.output.names = ['A']#, 'B']
    model.func_args = {'x_values': x_values}
    model.store = False
    
    output, samples = model.run_model_parallel(samples, mp = True)
    
    #from model import model
    #out1 = model(samples)
    
    #### Training surrogate models
    meta_model = PCE(inputs, meta_model_type = 'aPCE', pce_reg_method='FastARD', pce_deg = 3, pce_q_norm = 1)
    exp_design.n_init_samples = 40
    exp_design.sampling_method = 'random'
    
    engine = Engine(meta_model, model, exp_design)
    engine.start_engine()
    engine.train_normal()
    
    mean, stdev = engine.eval_metamodel(nsamples = 10)
    mean, stdev = engine.meta_model.eval_metamodel(samples)
    
    #### Active learning
    if 0:
        exp_design.n_new_samples = 1
        exp_design.n_max_samples = 14
        exp_design.mod_loo_threshold = 1e-16
        
        exp_design.tradeoff_scheme = None
        
        exp_design.explore_method = 'random'
        exp_design.n_canddidate = 1000
        exp_design.n_cand_groups = 4
        
        exp_design.exploit_method = 'VarOptDesign'
        exp_design.util_func = 'EIGF'
        
        engine = Engine(copy.deepcopy(meta_model), model, copy.deepcopy(exp_design))
        engine.train_sequential()
    
    #### Postprocessing
    post = PostProcessing(engine)
    post.valid_metamodel(n_samples=1)
    post.check_accuracy(n_samples=10)
    post.plot_moments()
    post.sobol_indices()
    #post.plot_seq_design_diagnostics()
    
    # Sanity check - test on training data
    mean, stdev = engine.eval_metamodel(engine.exp_design.x)
    print(mean['A']-engine.exp_design.y['A'])
    
    #### BayesInference
    
    true_sample = [[2]]
    observation = model.run_model_parallel(true_sample)[0]
    model.observations = {}
    for key in observation:
        if key == 'x_values':
            model.observations[key]=observation[key]
        else:
            model.observations[key]=observation[key][0]
            
    obsData = pd.DataFrame(model.observations, columns=model.output.names)
    discrepancy = Discrepancy('')
    discrepancy.type = 'Gaussian'
    discrepancy.parameters = obsData**2 + 0.01
    
    bayes = BayesInference(engine)
    bayes.use_emulator = True
    bayes.discrepancy = discrepancy
    bayes.plot = True
    
    bayes.inference_method = 'MCMC'
    import emcee
    bayes.mcmc_params = {
        'n_steps': 100, #e4,
        'n_walkers': 30,
        'moves': emcee.moves.KDEMove(),
        'multiprocessing': False,
        'verbose': False
        }
    #bayes.run_inference(save=True)

    #### Model validation
    bayes.inference_method ='rejection'

    bayes.bootstrap_method = 'normal'
    bayes.n_bootstrap_itrs = 500
    bayes.bootstrap_noise = 0.2
    
    model.observations_valid = model.observations
    bayes.valid_metrics = ['kld', 'inf_entropy']

    #log_bme = bayes.run_validation()

    #### Model Comparison
    model1 = PyLinkForwardModel()
    model1.link_type = 'Function'
    model1.py_file = 'model'
    model1.name = 'model'
    model1.output.names = ['B']
    model1.func_args = {'x_values': x_values}
    model1.observations_valid = model.observations
    
    meta_model1 = PCE(inputs, meta_model_type = 'aPCE', pce_reg_method='FastARD', pce_deg = 3, pce_q_norm = 1)
    exp_design1 = ExpDesigns(inputs, sampling_method='random')
    exp_design1.n_init_samples = 30
    
    engine1 = Engine(meta_model1, model1, exp_design1)
    engine1.train_normal(save=False)
    
    model2 = PyLinkForwardModel()
    model2.link_type = 'Function'
    model2.py_file = 'model2'
    model2.name = 'model2'
    model2.output.names = ['A']
    model2.func_args = {'x_values': x_values}
    model2.observations_valid = model.observations
    
    meta_model2 = PCE(inputs, meta_model_type = 'aPCE', pce_reg_method='FastARD', pce_deg = 3, pce_q_norm = 1)
    exp_design2 = ExpDesigns(inputs, sampling_method='random')
    exp_design2.n_init_samples = 30

    engine2 = Engine(meta_model2, model2, exp_design2)
    engine2.train_normal(save=False)
    
    meta_models = {
        "linear": engine,
        #"square": engine1,
        "degthree": engine2
        }

    opts_bootstrap = {
        "bootstrap_method": 'normal',
        "n_samples": 10, #0,
        "discrepancy": discrepancy,
        "use_emulator": True,
        "plot": False,
        "mcmc_params": {
            'n_steps': 100, #e4,
            'n_walkers': 10,
            'moves': emcee.moves.KDEMove(),
            'multiprocessing': False,
            'verbose': False
            }
        }

    bmc = BayesModelComparison(model_dict=meta_models, bayes_opts=opts_bootstrap)
    if 0:
        output_dict = bmc.model_comparison_all()
# -*- coding: utf-8 -*-
"""
A simple model example for the BVR user guide.

@author: Rebecca Kohlhaas
"""
import numpy as np

def model(samples, x_values):
    samples = samples[0]*x_values
    square = np.power(samples[0]*x_values, 2)
    outputs = {'A': samples, 'B': square, 'x_values': x_values}
    return outputs
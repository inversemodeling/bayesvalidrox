#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows a surrogate for a beam deflection model can be created.
This example also illustrate how a model with an executable and a
input file can be linked with the bayesvalidrox package.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

"""
# Add BayesValidRox path
import sys
sys.path.append("../../src/bayesvalidrox/")

from bayesvalidrox import PyLinkForwardModel, Input, ExpDesigns, PCE, PostProcessing, BayesInference, Discrepancy, Engine


if __name__ == "__main__":

    # =====================================================
    # =============   COMPUTATIONAL MODEL  ================
    # =====================================================
    model = PyLinkForwardModel()

    model.link_type = 'PyLink'
    model.name = 'Beam9points'
    model.input_file = "SSBeam_Deflection.inp"
    model.input_template = "SSBeam_Deflection.tpl.inp"

    model.shell_command = "myBeam9points SSBeam_Deflection.inp"
    model.output.parser = 'read_Beam_Deflection'
    model.output.names = ['Deflection [m]']
    model.output.file_names = ["SSBeam_Deflection.out"]

    # For Bayesian inversion
    model.meas_file = 'data/MeasuredData.csv'
    model.meas_file_valid = 'data/MeasuredData_Valid.csv'

    # For Checking with the MonteCarlo refrence
    model.mc_ref_file = 'data/MCrefs_MeanStd.csv'

    # =====================================================
    # =========   PROBABILISTIC INPUT MODEL  ==============
    # =====================================================
    # Define the uncertain parameters with their mean and
    # standard deviation
    inputs = Input()

    inputs.add_marginals()
    inputs.marginals[0].name = 'Beam width'
    inputs.marginals[0].dist_type = 'lognormal'
    inputs.marginals[0].parameters = [0.15, 0.0075]

    inputs.add_marginals()
    inputs.marginals[1].name = 'Beam height'
    inputs.marginals[1].dist_type = 'lognormal'
    inputs.marginals[1].parameters = [0.3, 0.015]

    inputs.add_marginals()
    inputs.marginals[2].name = 'Youngs modulus'
    inputs.marginals[2].dist_type = 'lognormal'
    inputs.marginals[2].parameters = [30000e+6, 4500e+6]

    inputs.add_marginals()
    inputs.marginals[3].name = 'Uniform load'
    inputs.marginals[3].dist_type = 'lognormal'
    inputs.marginals[3].parameters = [1e4, 2e3]

    # =====================================================
    # ==========  DEFINITION OF THE METAMODEL  ============
    # =====================================================
    meta_model = PCE(inputs)

    # Select if you want to preserve the spatial/temporal depencencies
    # MetaModelOpts.dim_red_method = 'PCA'
    # MetaModelOpts.var_pca_threshold = 99.999
    # MetaModelOpts.n_pca_components = 12

    # Select your metamodel method
    # 1) PCE (Polynomial Chaos Expansion) 2) aPCE (arbitrary PCE)
    # 3) GPE (Gaussian Process Emulator)
    meta_model.meta_model_type = 'PCE'

    # ------------------------------------------------
    # ------------- PCE Specification ----------------
    # ------------------------------------------------
    # Select the sparse least-square minimization method for
    # the PCE coefficients calculation:
    # 1)OLS: Ordinary Least Square  2)BRR: Bayesian Ridge Regression
    # 3)LARS: Least angle regression  4)ARD: Bayesian ARD Regression
    # 5)FastARD: Fast Bayesian ARD Regression
    # 6)BCS: Bayesian Compressive Sensing
    # 7)OMP: Orthogonal Matching Pursuit
    # 8)VBL: Variational Bayesian Learning
    # 9)EBL: Emperical Bayesian Learning
    meta_model.pce_reg_method = 'FastARD'

    # Specify the max degree to be compared by the adaptive algorithm:
    # The degree with the lowest Leave-One-Out cross-validation (LOO)
    # error (or the highest score=1-LOO)estimator is chosen as the final
    # metamodel. pce_deg accepts degree as a scalar or a range.
    meta_model.pce_deg = 6

    # q-quasi-norm 0<q<1 (default=1)
    meta_model.pce_q_norm = 0.75

    # ------------------------------------------------
    # ------ Experimental Design Configuration -------
    # ------------------------------------------------
   # MetaModelOpts.add_ExpDesign()
    exp_design = ExpDesigns(inputs)

    # One-shot (normal) or Sequential Adaptive (sequential) Design
    exp_design.n_init_samples = 100

    # Sampling methods
    # 1) random 2) latin_hypercube 3) sobol 4) halton 5) hammersley
    # 6) chebyshev(FT) 7) grid(FT) 8)user
    exp_design.sampling_method = 'latin_hypercube'

    # Provide the experimental design object with a hdf5 file
    # MetaModelOpts.ExpDesign.hdf5_file = 'ExpDesign_Beam9points.hdf5'

    # ------ Train the surrogate model ------
    engine = Engine(meta_model, model, exp_design)
    #engine.start_engine()
    engine.train_normal()

    # =====================================================
    # =========  POST PROCESSING OF METAMODELS  ===========
    # =====================================================
    post = PostProcessing(engine)

    # Compute the moments and compare with the Monte-Carlo reference
    post.plot_moments(xlabel='$\\mathrm{L_{rel}}$ (-)')

    # Plot the sobol indices
    if meta_model.meta_model_type != 'GPE':
        total_sobol = post.sobol_indices()

    # Plot to check validation visually.
    post.valid_metamodel(n_samples=3)

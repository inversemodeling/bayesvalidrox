#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 16:48:59 2019

@author: farid
"""
import pandas as pd
import numpy as np


def read_Beam_Deflection(FileNames):

    df_Mean = pd.read_csv(FileNames[0], header=None, delimiter=' ')
    x_values = np.linspace(0, 1, 11)
    deflection = df_Mean[df_Mean.columns[0]]

    deflection_zero = np.zeros((1))
    deflection_mid = np.append(deflection_zero, deflection)
    deflection_final = np.append(deflection_mid, deflection_zero)

    # Prepare output dict using standard bayesvalidrox format
    output = {'x_values': x_values, 'Deflection [m]': deflection_final}

    return output

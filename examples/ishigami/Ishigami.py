#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 08:56:21 2018

@author: farid
"""
import numpy as np


def Ishigami(xx, *args):
    """
    ISHIGAMI FUNCTION

    Authors: Sonja Surjanovic, Simon Fraser University
             Derek Bingham, Simon Fraser University
    Questions/Comments: Please email Derek Bingham at dbingham@stat.sfu.ca.

    Copyright 2013. Derek Bingham, Simon Fraser University.

    THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    derivative works, such modified software should be clearly marked.
    Additionally, this program is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; version 2.0 of the License.
    Accordingly, this program is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    For function details and reference information, see:
        https://www.sfu.ca/~ssurjano/ishigami.html

    Parameters
    ----------
    xx : array of shape (n_samples, n_params)
        Input parameter sets.
    *args : coefficients
        a = coefficient (optional), with default value 7
        b = coefficient (optional), with default value 0.1

    Returns
    -------
    output: dict
        Output with x_values.

    """

    x1, x2, x3 = xx[:, 0], xx[:, 1], xx[:, 2]

    if (len(args) == 0):
        a = 7
        b = 0.1
    elif (len(args) == 1):
        b = 0.1

    term1 = np.sin(x1)
    term2 = a * (np.sin(x2))**2
    term3 = b * x3**4 * np.sin(x1)

    y = term1 + term2 + term3

    # Prepare output dict using standard bayesvalidrox format
    output = {'x_values': np.zeros(1), 'Z': y}

    return output

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows a surrogate-assisted Bayesian calibration of a time dependent
    analytical function.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Fri Aug 9 2019

"""

import sys
import numpy as np
import pandas as pd
import joblib
import emcee

# Add BayesValidRox path
sys.path.append("../../src/")

from bayesvalidrox import (
    PyLinkForwardModel,
    Input,
    ExpDesigns,
    GPESkl,
    PostProcessing,
    BayesInference,
    Discrepancy,
    Engine,
)

if __name__ == "__main__":
    # Number of parameters (supports 2, 10)
    NDIM = 2

    # =====================================================
    # =============   COMPUTATIONAL MODEL  ================
    # =====================================================
    model = PyLinkForwardModel()

    model.link_type = "Function"
    model.py_file = "analytical_function"
    model.name = "AnalyticFunc"

    model.output.names = ["Z"]

    # For Bayesian inversion synthetic data with X=[0,0]
    model.observations = {}
    model.observations["Time [s]"] = np.arange(0, 10, 1.0) / 9
    model.observations["Z"] = np.repeat([2.0], 10)

    # For Checking with the MonteCarlo refrence
    model.mc_reference = {}
    model.mc_reference["Time [s]"] = np.arange(0, 10, 1.0) / 9
    model.mc_reference["mean"] = np.load(f"data/mean_{NDIM}.npy")
    model.mc_reference["std"] = np.load(f"data/std_{NDIM}.npy")

    # =====================================================
    # =========   PROBABILISTIC INPUT MODEL  ==============
    # =====================================================
    # Define the uncertain parameters with their mean and
    # standard deviation
    inputs = Input()

    # Assuming dependent input variables
    # inputs.rosenblatt = True

    for i in range(NDIM):
        inputs.add_marginals(
            name="$\\theta_{" + str(i + 1) + "}$",
            dist_type="uniform",
            parameters=[-5, 5],
        )

    # =====================================================
    # ==========  DEFINITION OF THE METAMODEL  ============
    # =====================================================
    meta_model = GPESkl(inputs)

    # Select if you want to preserve the spatial/temporal depencencies
    # meta_model.dim_red_method = 'PCA'
    # meta_model.var_pca_threshold = 99.999
    # meta_model.n_pca_components = 10

    # ------------------------------------------------
    # ------------- GPE Specification ----------------
    # ------------------------------------------------
    # Select the solver for solving for the GP Hyperparameters using the ML approach
    # ToDo: Remove this as a user-defined parameter, since only one is available?
    # 1)LBFGS: only option for Scikit Learn
    meta_model._gpe_reg_method = "LBFGS"

    # Kernel options ----------------------------
    # Loop over different Kernels:
    # 1) True to loop over the different kernel types and select the best one
    meta_model._auto_select = False

    # Select Kernel type:
    # 1) RBF: Gaussian/squared exponential kernel
    # 2) Matern Kernel
    # 3) RQ: Rational Quadratic kernel
    meta_model._kernel_type = "RBF"

    meta_model.normalize_x_method = "norm"  # Input data transformation
    meta_model._kernel_isotropy = True  # Kernel isotropy, False for anisotropy
    meta_model._nugget = 1e-6  # Set regularization parameter (constant)
    meta_model._kernel_noise = (
        False  # Optimize regularization parameter. True to consider WhiteKernel
    )
    meta_model.n_restarts = 20

    # Bootstraping
    # 1) normal 2) fast
    meta_model.n_bootstrap_itrs = 1

    # ------------------------------------------------
    # ------ Experimental Design Configuration -------
    # ------------------------------------------------
    exp_design = ExpDesigns(inputs)

    # Number of initial (static) training samples
    exp_design.n_init_samples = 140

    # Sampling methods
    # 1) random 2) latin_hypercube 3) sobol 4) halton 5) hammersley
    # 6) chebyshev(FT) 7) grid(FT) 8)user
    exp_design.sampling_method = "latin_hypercube"

    # Provide the experimental design object with a hdf5 file
    # exp_design.hdf5_file = 'exp_design_AnalyticFunc.hdf5'

    # Set the sampling parameters
    exp_design.n_new_samples = 1
    exp_design.n_max_samples = 141  # 150          # sum of init + sequential
    exp_design.mod_loo_threshold = 1e-16

    # Tradeoff scheme
    # 1) None 2) 'equal' 3)'epsilon-decreasing' 4) 'adaptive'
    exp_design.tradeoff_scheme = None
    # exp_design.n_replication = 5

    # -------- Exploration ------
    # 1)'Voronoi' 2)'random' 3)'latin_hypercube' 4)'LOOCV' 5)'dual annealing'
    exp_design.explore_method = "random"

    # Use when 'dual annealing' chosen
    exp_design.max_func_itr = 1000

    # Use when 'Voronoi' or 'random' or 'latin_hypercube' chosen
    exp_design.n_canddidate = 1000
    exp_design.n_cand_groups = 4

    # -------- Exploitation ------
    # 1)'BayesOptDesign' 2)'BayesActDesign' 3)'VarOptDesign' 4)'alphabetic'
    # 5)'Space-filling'
    exp_design.exploit_method = "Space-filling"
    exp_design.exploit_method = "BayesActDesign"
    exp_design.util_func = "DKL"

    # BayesOptDesign/BayesActDesign -> when data is available
    # 1) MI (Mutual information) 2) ALC (Active learning McKay)
    # 2)DKL (Kullback-Leibler Divergence) 3)DPP (D-Posterior-percision)
    # 4)APP (A-Posterior-percision)  # ['DKL', 'BME', 'infEntropy']
    # exp_design.util_func = 'DKL'

    # BayesActDesign -> when data is available
    # 1) BME (Bayesian model evidence) 2) infEntropy (Information entropy)
    # 2)DKL (Kullback-Leibler Divergence)
    # exp_design.util_func = 'DKL'

    # VarBasedOptDesign -> when data is not available
    # 1)ALM 2)EIGF, 3)LOOCV
    # or a combination as a list
    # exp_design.util_func = 'EIGF'

    # alphabetic
    # 1)D-Opt (D-Optimality) 2)A-Opt (A-Optimality)
    # 3)K-Opt (K-Optimality) or a combination as a list
    # exp_design.util_func = 'D-Opt'

    # Defining the measurement error, if it's known a priori
    obs_uncert = pd.DataFrame(model.observations, columns=model.output.names) ** 2
    discrepancy = Discrepancy(parameters=obs_uncert, disc_type="Gaussian")

    # Plot the posterior snapshots for SeqDesign
    # exp_design.max_a_post = [0] * NDIM

    # For calculation of validation error for SeqDesign
    prior = np.load(f"data/Prior_{NDIM}.npy")
    prior_outputs = np.load(f"data/origModelOutput_{NDIM}.npy")
    likelihood = np.load(f"data/validLikelihoods_{NDIM}.npy")
    exp_design.valid_samples = prior[:500]
    exp_design.valid_model_runs = {"Z": prior_outputs[:500]}

    # Run using the engine
    engine = Engine(meta_model, model, exp_design, discrepancy=discrepancy)
    engine.train_sequential()
    # engine.train_normal()

    # =====================================================
    # =========  POST PROCESSING OF METAMODELS  ===========
    # =====================================================
    post = PostProcessing(engine)

    # Plot to check validation visually.
    post.valid_metamodel(n_samples=1)

    # Compute and print RMSE error
    post.check_accuracy(n_samples=300)

    # Compute the moments and compare with the Monte-Carlo reference
    post.plot_moments()

    # Plot the evolution of the KLD,BME, and Modified LOOCV error
    if engine.exp_design.method == "sequential":
        refBME_KLD = np.load("data/refBME_KLD_" + str(NDIM) + ".npy")
        post.plot_seq_design_diagnostics(refBME_KLD)

    with open("engine.pkl", "wb") as output:
        joblib.dump(engine, output, 2)

    with open("engine.pkl", "rb") as input_:
        engine = joblib.load(input_)

    # =====================================================
    # ========  Bayesian inference with Emulator ==========
    # =====================================================
    bayes = BayesInference(engine)

    # Basic settings
    bayes.use_emulator = False
    bayes.n_prior_samples = 1000

    # BME Bootstrap settings
    bayes.bootstrap_method = "normal"
    bayes.n_bootstrap_itrs = 500
    bayes.bootstrap_noise = 100

    # Reference data choice and perturbation
    bayes.selected_indices = [0, 3, 5, 7, 9]

    # Add the discrepancy
    bayes.discrepancy = discrepancy

    # Generate additional validation observations
    observations_valid = model.run_model_parallel(
        engine.exp_design.generate_samples(1), key_str="va"
    )[0]
    for key in observations_valid:
        observations_valid[key] = observations_valid[key][0]
    bayes.engine.model.observations_valid = observations_valid
    bayes.engine.model.n_obs_valid = 1

    # Perform validation
    log_bme = bayes.run_validation()

    # Select the inference method - either 'rejection' or 'MCMC'
    bayes.inference_method = "rejection"
    # bayes.inference_method = "MCMC"

    # Set the MCMC parameters passed to self.mcmc_params
    bayes.mcmc_params = {
        "n_steps": 1e3,
        "n_walkers": 30,
        "moves": emcee.moves.KDEMove(),
        "multiprocessing": False,
        "verbose": False,
    }

    # Perform inference
    posterior = bayes.run_inference()

    # Save BayesInference object
    with open(f"Bayes_{model.name}.pkl", "wb") as output:
        joblib.dump(bayes, output, 2)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 19 14:31:50 2022

@author: farid
"""
import matplotlib.pyplot as plt
import os
import pandas as pd
import seaborn as sns
import numpy as np

# Load the mplstyle
plt.style.use(
    os.path.join(
        os.path.split(__file__)[0],
        "../../../../src/bayesvalidrox/",
        "bayesvalidrox.mplstyle",
    )
)

font_size = 80

posterior_df = pd.read_csv("posterior_orig.csv")
par_names = list(posterior_df.keys())
n_params = len(par_names)
posterior = posterior_df.values
bound_tuples = [(-5, 5), (-5, 5)]

folder = "BAL_DKL"
file = "SeqPosterior_45"
posterior = np.load(f"{folder}/{file}.npy")

figPosterior, ax = plt.subplots(figsize=(15, 15))

sns.kdeplot(
    x=posterior[:, 0],
    y=posterior[:, 1],
    fill=True,
    ax=ax,
    cmap=plt.cm.jet,
    clip=bound_tuples,
)
# Axis labels
plt.xlabel(par_names[0], fontsize=font_size)
plt.ylabel(par_names[1], fontsize=font_size)

# Set axis limit
plt.xlim(bound_tuples[0])
plt.ylim(bound_tuples[1])

# Increase font size
plt.xticks(fontsize=font_size)
plt.yticks(fontsize=font_size)

# Switch off the grids
plt.grid(False)
plt.show()
# figPosterior.savefig("orig_posterior.pdf", bbox_inches='tight')
figPosterior.savefig(f"seq_posterior_45_{folder}.pdf", bbox_inches="tight")
plt.close()

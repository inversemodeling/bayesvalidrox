#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows a surrogate-assisted Bayesian calibration of a time dependent
analytical function with and wihout applying principal component analysis.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Fri Aug 9 2019

"""
import numpy as np
import pandas as pd
import scipy.io as io
import sys
import joblib
# import bayesvalidrox
# Add BayesValidRox path
sys.path.append("../../src/")

from bayesvalidrox import PyLinkForwardModel, Input, PCE, PostProcessing, BayesInference, Discrepancy, Engine, ExpDesigns

import matplotlib
matplotlib.use('agg')


if __name__ == "__main__":

    # Number of parameters
    ndim = 10  # 2, 10

    # =====================================================
    # =============   COMPUTATIONAL MODEL  ================
    # =====================================================
    model = PyLinkForwardModel()

    model.link_type = 'Function'
    model.py_file = 'analytical_function'
    model.name = 'AnalyticFunc'

    model.output.names = ['Z']

    # For Bayesian inversion synthetic data with X=[0,0]
    model.observations = {}
    model.observations['Time [s]'] = np.arange(0, 10, 1.) / 9
    model.observations['Z'] = np.repeat([2.], 10)

    # For Checking with the MonteCarlo refrence
    model.mc_reference = {}
    model.mc_reference['Time [s]'] = np.arange(0, 10, 1.) / 9
    model.mc_reference['mean'] = np.load(f"data/mean_{ndim}.npy")
    model.mc_reference['std'] = np.load(f"data/std_{ndim}.npy")

    # =====================================================
    # =========   PROBABILISTIC INPUT MODEL  ==============
    # =====================================================
    # Define the uncertain parameters with their mean and
    # standard deviation
    inputs = Input()

    for i in range(ndim):
        inputs.add_marginals()
        inputs.marginals[i].name = "$\\theta_{"+str(i+1)+"}$"
        inputs.marginals[i].dist_type = 'uniform'
        inputs.marginals[i].parameters = [-5, 5]

    # ------------------------------------------------
    # ------ Experimental Design Configuration -------
    # ------------------------------------------------
    ExpDesign = ExpDesigns(inputs)
    ExpDesign.n_init_samples = 3*ndim

    # Sampling methods
    # 1) random 2) latin_hypercube 3) sobol 4) halton 5) hammersley
    # 6) chebyshev(FT) 7) grid(FT) 8)user
    ExpDesign.sampling_method = 'latin_hypercube'
    
    # ------------------------------------------------
    # ------------- PCE Specification ----------------
    # ------------------------------------------------
    meta_model = PCE(inputs)

    # Select if you want to preserve the spatial/temporal depencencies
    meta_model.dim_red_method = 'PCA'
    meta_model.var_pca_threshold = 99.999
    meta_model.n_pca_components = 10
    #meta_model.n_bootstrap_itrs = 2

    # Select your metamodel method
    # 1) PCE (Polynomial Chaos Expansion) 2) aPCE (arbitrary PCE)
    # 3) GPE (Gaussian Process Emulator)
    meta_model.meta_model_type = 'aPCE'

    # Select the sparse least-square minimization method for
    # the PCE coefficients calculation:
    # 1)OLS: Ordinary Least Square  2)BRR: Bayesian Ridge Regression
    # 3)LARS: Least angle regression  4)ARD: Bayesian ARD Regression
    # 5)FastARD: Fast Bayesian ARD Regression
    # 6)BCS: Bayesian Compressive Sensing
    # 7)OMP: Orthogonal Matching Pursuit
    # 8)VBL: Variational Bayesian Learning
    # 9)EBL: Emperical Bayesian Learning
    meta_model.pce_reg_method = 'FastARD'

    # Bootstraping
    # 1) normal 2) fast
    #meta_model.bootstrap_method = 'fast'
    #meta_model.n_bootstrap_itrs = 1

    # Specify the max degree to be compared by the adaptive algorithm:
    # The degree with the lowest Leave-One-Out cross-validation (LOO)
    # error (or the highest score=1-LOO)estimator is chosen as the final
    # metamodel. pce_deg accepts degree as a scalar or a range.
    meta_model.pce_deg = 12

    # q-quasi-norm 0<q<1 (default=1)
    meta_model.pce_q_norm = 0.85 if ndim < 5 else 0.5

    # Create the engine
    engine = Engine(meta_model, model, ExpDesign)
    engine.train_normal(verbose = True)
    print('Surrogate has been trained')
    
    # =====================================================
    # ========  PostProcessing on the Model only ==========
    # =====================================================
    
    post = PostProcessing(engine)

    # Plot to check validation visually.
    post.valid_metamodel(n_samples=1)

    # Compute and print RMSE error
    post.check_accuracy(n_samples=300)

    # Compute the moments and compare with the Monte-Carlo reference
    post.plot_moments()

    # Plot the sobol indices
    if meta_model.meta_model_type != 'GPE':
        total_sobol = post.sobol_indices()
    
    # =====================================================
    # ========  Bayesian inference without Emulator ==========
    # =====================================================
    bayes = BayesInference(engine)
    bayes.use_emulator = False
    bayes.plot = True

    # Select the inference method
    import emcee
    bayes.inference_method = "MCMC"
    # Set the MCMC parameters passed to self.mcmc_params
    bayes.mcmc_params = {
        'n_steps': 1e3,
        'n_walkers': 30,
        'moves': emcee.moves.KDEMove(),
        'multiprocessing': False,
        'verbose': False
        }

    # ----- Define the discrepancy model -------
    obsData = pd.DataFrame(model.observations, columns=model.output.names)
    discrepancy = Discrepancy('')
    discrepancy.type = 'Gaussian'
    discrepancy.parameters = obsData**2
    bayes.discrepancy = discrepancy

    # Start the calibration/inference
    posterior = bayes.run_inference()

    # Save class objects
    with open(f'Bayes_{model.name}.pkl', 'wb') as output:
        joblib.dump(bayes, output, 2)


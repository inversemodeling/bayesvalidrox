#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows a surrogate-assisted Bayesian calibration of a time dependent
    analytical function.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Fri Aug 9 2019

"""

import numpy as np
import pandas as pd
import sys
import joblib
import matplotlib
#matplotlib.use('agg')

# import bayesvalidrox
# Add BayesValidRox path
sys.path.append("../../src/")

from bayesvalidrox import Input, PyLinkForwardModel, ExpDesigns, PCE, PostProcessing, BayesInference, Discrepancy, Engine

if __name__ == "__main__":

    # Number of parameters
    ndim = 2#10  # 2, 10
    # Model setup
    model = PyLinkForwardModel()
    model.link_type = 'Function'
    model.py_file = 'analytical_function'
    model.name = 'AnalyticFunc'
    model.output.names = ['Z']

    # For Bayesian inversion synthetic data with X=[0,0]
    model.observations = {}
    model.observations['Time [s]'] = np.arange(0, 10, 1.) / 9
    model.observations['Z'] = np.repeat([2.], 10)

    # For Checking with the MonteCarlo refrence
    model.mc_reference = {}
    model.mc_reference['Time [s]'] = np.arange(0, 10, 1.) / 9
    model.mc_reference['mean'] = np.load(f"data/mean_{ndim}.npy")
    model.mc_reference['std'] = np.load(f"data/std_{ndim}.npy")

    # Define the uncertain parameters with their mean and
    # standard deviation
    inputs = Input()
    for i in range(ndim):
        inputs.add_marginals()
        inputs.marginals[i].name = "$\\theta_{"+str(i+1)+"}$"
        inputs.marginals[i].dist_type = 'uniform'
        inputs.marginals[i].parameters = [-5, 5]

    # MetaModel setup
    meta_model = PCE(inputs)
    meta_model.meta_model_type = 'aPCE'
    meta_model.pce_reg_method = 'FastARD'
    meta_model.bootstrap_method = 'fast'
    meta_model.n_bootstrap_itrs = 1
    meta_model.pce_deg = 12
    meta_model.pce_q_norm = 0.85 if ndim < 5 else 0.5

    # ExpDesign setup
    exp_design = ExpDesigns(inputs)
    exp_design.n_init_samples = 140

    # Set the sampling parameters
    exp_design.n_new_samples = 1
    exp_design.mod_loo_threshold = 1e-16
    exp_design.max_func_itr = 1000
    exp_design.n_canddidate = 1000
    exp_design.n_cand_groups = 4

    # Do the standard training of the surrogate and init of the engine
    engine = Engine(meta_model, model, exp_design)
    engine.train_normal()
    
    # Defining the measurement error, if it's known a priori
    obs = pd.DataFrame(model.observations, columns=model.output.names)
    discrepancy = Discrepancy('')
    discrepancy.type = 'Gaussian'
    discrepancy.parameters = obs**2
    engine.discrepancy = discrepancy

#%% Test combinations of the sequential strategies
    """
    This part of the code tests the available combinations of the 
    sequential strategies.
    The following combinations have remaining issues to be solved:
        - exploration = 'Voronoi' creates mismatching numbers of candidates and weights
        - exploration = 'loocv' needs MetaModel.create_ModelError, which does not exist
        - exploration = 'dual annealing' restricted in what it allows as exploitation methods
        - tradeoff = 'adaptive' perhaps not possible in the first AL iteration?
        
    The following combinations are running through:
        Tradeoff
        - None
        - equal
        - epsilon-decreasing
        Exploration
        - random
        - global_mc
        Exploitation
        - BayesActDesign
        - VarOptDesign
        - alphabetic
        - space-filling
        
    Performance notes:
        - BayesOptDesign slow in the OptBayesianDesign iterations
        - exploitation = 'BayesOptDesign' has circular dependency with the engine class
        
    Note: user-defined options were left out in this test for both 
    sampling-methods and exploitation schemes.
    """
    # Create a single new sample from each AL strategy
    tradeoff_schemes = [None, 'equal', 'epsilon-decreasing', 'adaptive']
    #sampling_method = ['random', 'latin-hypercube', 'sobol', 'halton', 
    #                   'hammersley', 'chebyshev', 'grid']
    #exploration_schemes = ['Voronoi', 'global_mc', 'random', 'latin-hypercube', 'LOOCV', 
    #                       'dual annealing']
    exploration_schemes = ['random', 'dual annealing']
    #exploitation_schemes = ['BayesOptDesign', 'BayesActDesign', 'VarOptDesign', 
    #                        'alphabetic', 'Space-filling'] 
    exploitation_schemes = ['BayesActDesign', 'alphabetic', 'Space-filling'] 
    
    for tradeoff in tradeoff_schemes:
        for exploration in exploration_schemes:
            for exploitation in exploitation_schemes:
                if exploration == 'dual annealing':
                    if exploitation not in ['BayesOptDesign','VarOptDesign']:
                        continue
                
                # Utility function depends on exploitation type
                if exploitation == 'BayesOptDesign':
                    # 1) MI (Mutual information) 2) ALC (Active learning McKay)
                    # 2)DKL (Kullback-Leibler Divergence) 3)DPP (D-Posterior-percision)
                    # 4)APP (A-Posterior-percision)  # ['DKL', 'BME', 'infEntropy']
                    util_func_list = ['MI', 'ALC', 'DKL', 'DPP', 'APP', 
                                      'BayesRisk', 'infEntropy']
                elif exploitation == 'BayesActDesign':
                    # 1) BME (Bayesian model evidence) 
                    # 2) infEntropy (Information entropy)
                    # 2)DKL (Kullback-Leibler Divergence)
                    util_func_list = ['BME', 'DKL', 'infEntropy', 'BIC', 
                                      'AIC', 'DIC']
                elif exploitation == 'VarOptDesign':
                    # 1)ALM 2)EIGF, 3)LOOCV
                    # or a combination as a list
                    util_func_list = ['ALM', 'EIGF', 'LOOCV']
                elif exploitation == 'alphabetic':
                    # 1)D-Opt (D-Optimality) 2)A-Opt (A-Optimality)
                    # 3)K-Opt (K-Optimality) or a combination as a list
                    util_func_list = ['D-Opt', 'A-Opt', 'K-Opt']
                else:
                    util_func_list = ['']
                
                for util in util_func_list:
                    # Stop the not working ones
                    if exploitation == 'BayesActDesign' and util == 'DIC':
                        continue
                    # General setting reset and update
                    engine.exp_design.n_max_samples = engine.exp_design.x.shape[0]+1
                    
                    # Iteration-specific settings
                    engine.exp_design.tradeoff_scheme = tradeoff
                    engine.exp_design.sampling_method = 'latin_hypercube'
                    engine.exp_design.explore_method = exploration
                    engine.exp_design.exploit_method = exploitation
                    engine.exp_design.util_func = util
                
                    # Do the Sequential training
                    print('')
                    print('*'*50)
                    print('Current settings:')
                    print(f' - tradeoff: {tradeoff}')
                    print(f' - exploration: {exploration}')
                    print(f' - exploitation: {exploitation}')
                    print(f' - util_func: {util}')
                    engine.train_sequential()
    
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Mon Sep 12 2022

"""
import numpy as np

def borehole(xx, *args):
    """
    BOREHOLE FUNCTION

    Authors: Sonja Surjanovic, Simon Fraser University
             Derek Bingham, Simon Fraser University
    Questions/Comments: Please email Derek Bingham at dbingham@stat.sfu.ca.

    Copyright 2013. Derek Bingham, Simon Fraser University.

    THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    derivative works, such modified software should be clearly marked.
    Additionally, this program is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; version 2.0 of the License.
    Accordingly, this program is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    For function details and reference information, see:
        https://www.sfu.ca/~ssurjano/ishigami.html

    Parameters
    ----------
    xx : array of shape (n_samples, n_params)
        Input parameter sets. [rw, L, kw, Tu, Tl, Hu, Hl, r]

    Returns
    -------
    output: dict
        Water flow rate.

    """
    rw = xx[:, 0]
    L = xx[:, 1]
    Kw = xx[:, 2]
    Tu = xx[:, 3]
    Tl = xx[:, 4]
    Hu = xx[:, 5]
    Hl = xx[:, 6]
    r = xx[:, 7]
    

    frac1 = 2 * np.pi * Tu * (Hu-Hl)

    frac2a = 2*L*Tu / (np.log(r/rw)*(rw**2)*Kw)
    frac2b = Tu / Tl
    frac2 = np.log(r/rw) * (1 + frac2a + frac2b)

    y = frac1 / frac2

    # Prepare output dict using standard bayesvalidrox format
    output = {'x_values': np.zeros(1), 'flow rate': y}

    return output

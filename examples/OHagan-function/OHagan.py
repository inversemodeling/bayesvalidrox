#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 08:56:21 2018

@author: farid
"""
import numpy as np


def OHagan(xx, *args):
    """
    Oakley & O'Hagan (2004) Function

    This function's a-coefficients are chosen so that 5 of the input variables
    contribute significantly to the output variance, 5 have a much smaller
    effect, and the remaining 5 have almost no effect on the output variance.

    O'Hagan, 2004, Probabilistic sensitivity analysis of complex models: a
    Bayesian approach J. R. Statist. Soc. B (2004) 66, Part 3, pp. 751-769.

    Parameters
    ----------
    xx : array of shape (n_samples, n_params)
        Input parameter sets.

    Returns
    -------
    output: dict
        Ourput value.

    """
    n_samples, n_params = xx.shape

    # Read M
    M = np.loadtxt('data/M.csv', delimiter=',')

    a_1 = np.array([0.0118, 0.0456, 0.2297, 0.0393, 0.1177, 0.3865, 0.3897,
                    0.6061, 0.6159, 0.4005, 1.0741, 1.1474, 0.7880, 1.1242,
                    1.1982])
    a_2 = np.array([0.4341, 0.0887, 0.0512, 0.3233, 0.1489, 1.0360, 0.9892,
                    0.9672, 0.8977, 0.8083, 1.8426, 2.4712, 2.3946, 2.0045,
                    2.2621])
    a_3 = np.array([0.1044, 0.2057, 0.0774, 0.2730, 0.1253, 0.7526, 0.8570,
                    1.0331, 0.8388, 0.7970, 2.2145, 2.0382, 2.4004, 2.0541,
                    1.9845])

    # Compute response
    xx_M = np.dot(xx, M)
    last_term = np.sum(xx_M * xx, axis=1)

    y = np.dot(xx, a_1) + np.dot(np.sin(xx), a_2) + np.dot(np.cos(xx), a_3)
    y += last_term

    # Prepare output dict using standard bayesvalidrox format
    output = {'x_values': np.zeros(1), 'Z': y.T}

    return output

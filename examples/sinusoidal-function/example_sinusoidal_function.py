#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows a surrogate-assisted Bayesian calibration of a sinusoidal function.

Author: Alina Lacheim

Created on Fri Sep 13 2024

"""

import numpy as np
import pandas as pd
import sys
import matplotlib
matplotlib.use('agg')

sys.path.append("../../src/")

from bayesvalidrox import PyLinkForwardModel, Input, ExpDesigns, PCE, PostProcessing, Discrepancy, Engine

if __name__ == "__main__":
    
    # =====================================================
    # =============   COMPUTATIONAL MODEL  ================
    # =====================================================
    model = PyLinkForwardModel()

    model.link_type = 'Function'
    model.py_file = 'sinusoidal_function'
    model.name = 'SinusoidalFunc'

    model.output.names = ['Z']

    # =====================================================
    # =========   PROBABILISTIC INPUT MODEL  ==============
    # =====================================================
    # Define the uncertain parameters with their mean and
    # standard deviation
    inputs = Input()

    inputs.add_marginals()
    inputs.marginals[0].name = 'X'
    inputs.marginals[0].dist_type = 'uniform'
    inputs.marginals[0].parameters = [0,10]

    # =====================================================
    # ==========  DEFINITION OF THE METAMODEL  ============
    # =====================================================
    meta_model = PCE(inputs)

    meta_model.meta_model_type = 'aPCE'

    # ------------------------------------------------
    # ------------- PCE Specification ----------------
    # ------------------------------------------------
    meta_model._pce_reg_method = 'FastARD'

    meta_model.bootstrap_method = 'fast'
    meta_model.n_bootstrap_itrs = 1

    meta_model.pce_deg = 10

    meta_model.pce_q_norm = 1

    # ------------------------------------------------
    # ------ Experimental Design Configuration -------
    # ------------------------------------------------
    exp_design = ExpDesigns(inputs)

    #ExpDesign.method = 'sequential'
    exp_design.n_init_samples = 200

    # -------- Exploitation ------
    exp_design.exploit_method = 'Space-filling'

    #BayesOptDesign -> when data is available 
    # VarBasedOptDesign -> when data is not available
    exp_design.util_func = 'ALM'

    exp_design.valid_samples = np.load("./data/valid_samples.npy")
    exp_design.valid_model_runs = {'Z': np.load("./data/valid_samples.npy")}

    #perform active learning
    exp_design.method = 'sequential'

    #set the sampling parameters 
    exp_design.n_max_samples = 14
    exp_design.n_new_samples = 1
    exp_design.mod_LOO_threshold = 1e-16

    #set tradeoff scheme 
    exp_design.tradeoff_scheme = None

    # -------- Exploration ------
    exp_design.explore_method = 'random'

    #necessary because explore moethod is random 
    exp_design.n_candidate = 1000
    exp_design.n_cand_groups = 4

    # -------- Exploitation ------
    exp_design.exploit_method = 'VarOptDesign'
    exp_design.util_func = 'EIGF'

    #plot the posterior snapshots for SeqDesign
    exp_design.post_snapshot = False 
    exp_design.step_snapshot = 1
    exp_design.max_a_post = [0]

    # >>>>>>>>>>>>>>>>>>>>>> Build Surrogate <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    meta_model.ExpDesign = exp_design

    engine = Engine(meta_model, model, exp_design)
    engine.start_engine()

    #chose the right method either for normal training or active learning
    engine.train_normal()
    #engine.train_sequential()

    # =====================================================
    # =========  POST PROCESSING OF METAMODELS  ===========
    # =====================================================
    post = PostProcessing(engine)

    #Plot to check validation visually
    post.valid_metamodel(n_samples=1)

    #Check the quality of your regression model 
    post.check_reg_quality()

    #Compute and print RMSE error
    post.check_accuracy(n_samples=1000)

    post.plot_moments()
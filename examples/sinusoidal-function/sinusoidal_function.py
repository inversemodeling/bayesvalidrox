#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fr Sep 13 2024

@author: alina
"""
import numpy as np
import scipy.stats as stats
import scipy.stats as st
import seaborn as sns

def sinusoidal_function(xx, t=None):
    
    x = xx[:, 0]
    y = x * np.sin(x) / 10.0

    #prepare output dict using standard bayesvalidrox dict
    output = {'x_values': np.zeros(1), 'Z': y}

    return output
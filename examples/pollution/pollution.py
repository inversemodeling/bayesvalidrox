#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 14:48:43 2019

@author: farid
"""
import numpy as np
import scipy.stats as stats
import seaborn as sns


def pollution(xx, s=None, t=None):
    """
    ENVIRONMENTAL MODEL FUNCTION

    Authors: Sonja Surjanovic, Simon Fraser University
             Derek Bingham, Simon Fraser University
    Questions/Comments: Please email Derek Bingham at dbingham@stat.sfu.ca.

    Copyright 2013. Derek Bingham, Simon Fraser University.

    THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    derivative works, such modified software should be clearly marked.
    Additionally, this program is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; version 2.0 of the License.
    Accordingly, this program is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    For function details and reference information, see:
        http://www.sfu.ca/~ssurjano/

    Parameters
    ----------
    xx : array of shape (n_samples, n_params)
        [M, D, L, tau].
    s : array, optional
        Vector of locations. The default is None, which mean np.array([3.0]).
    t : array, optional
        Vector of time steps. The default is None, which means
        [40+5*i for i in range(21)].

    Returns
    -------
    output : TYPE
        DESCRIPTION.

    """
    n_samples = xx.shape[0]

    if t is None:
        t = np.array([40+5*i for i in range(21)])

    if s is None:
        s = np.array([3.0])  # np.array([0.5, 1, 1.5, 2, 2.5])

    ds = len(s)
    dt = len(t)
    dY = ds * dt
    Y = np.zeros((ds, dt))
    output = np.zeros((n_samples, dY))

    for idx in range(n_samples):
        M, D, L, tau = xx[idx]
        # Create matrix Y, where each row corresponds to si and each column
        # corresponds to tj.
        for ii in range(ds):
            si = s[ii]
            for jj in range(dt):
                tj = t[jj]

                term1a = M / np.sqrt(4*np.pi*D*tj)
                term1b = np.exp(-si**2 / (4*D*tj))
                term1 = term1a * term1b

                term2 = 0
                if tau < tj:
                    term2a = M / np.sqrt(4*np.pi*D*(tj-tau))
                    term2b = np.exp(-(si-L)**2 / (4*D*(tj-tau)))
                    term2 = term2a * term2b

                C = term1 + term2
                Y[ii, jj] = np.sqrt(4*np.pi) * C

        # Convert the matrix into a vector (by rows).
        Yrow = Y.T
        output[idx] = Yrow.flatten('F')

    # Prepare output dict using standard bayesvalidrox format
    output = {'x_values': t, 'C': output[0]}

    return output


if __name__ == "__main__":

    MCSize = 500000
    validSize = 100
    ndim = 4
    sigma_noise = 0.15
    sigma_error = 0.5

    # -------------------------------------------------------------------------
    # ----------------------- Synthetic data generation -----------------------
    # -------------------------------------------------------------------------
    MAP = np.array([[10, 0.07, 1.0, 30.16]])
    output_MAP = pollution(MAP, s=np.array([3]))  # Calibration
    # output_MAP = pollution(MAP, s=np.array([6]))  # Validation

    # Noise
    noise = np.random.normal(0, sigma_noise, len(output_MAP['C']))
    synthethicData = output_MAP['C'] + noise

    import matplotlib.pyplot as plt
    plt.scatter(output_MAP['C'], synthethicData,
                ls='-', marker='*', label='Obs. Data')
    plt.legend(loc='best')
    plt.show()
    np.save("data/MeasuredData_valid.npy", synthethicData)

    # -------------------------------------------------------------------------
    # ---------------------- Generate Prior distribution ----------------------
    # -------------------------------------------------------------------------
    valid_set = np.zeros((validSize, ndim))
    xx = np.zeros((MCSize, ndim))

    params = [(7, 13), (0.02, 0.12), (0.01, 3), (30.01, 30.295)]

    for idxDim in range(ndim):
        lower, upper = params[idxDim]
        xx[:, idxDim] = stats.uniform(loc=lower, scale=upper-lower).rvs(
            size=MCSize)
        valid_set[:, idxDim] = stats.uniform(loc=lower, scale=upper-lower).rvs(
            size=validSize)

    # run validation set
    valid_outputs = pollution(valid_set)
    # -------------------------------------------------------------------------
    # ------------- BME and Kullback-Leibler Divergence -----------------------
    # -------------------------------------------------------------------------
    outputs = pollution(xx)

    cov_matrix = np.diag(np.repeat(sigma_error**2, synthethicData.shape))

    Likelihoods = stats.multivariate_normal.pdf(
        outputs[1:], mean=synthethicData, cov=cov_matrix)

    sns.kdeplot(np.log(Likelihoods[Likelihoods > 0]),
                shade=True, color="g", label='Ref. Likelihood')

    normLikelihood = Likelihoods / np.nanmax(Likelihoods)
    # Random numbers between 0 and 1
    unif = np.random.rand(1, MCSize)[0]

    # Reject the poorly performed prior
    accepted = normLikelihood >= unif

    # Prior-based estimation of BME
    logBME = np.log(np.nanmean(Likelihoods))
    print(f'\nThe Naive MC-Estimation of BME is {logBME:.5f}.')

    # Posterior-based expectation of likelihoods
    postExpLikelihoods = np.mean(np.log(Likelihoods[accepted]))

    # Calculate Kullback-Leibler Divergence
    KLD = postExpLikelihoods - logBME
    print("The Kullback-Leibler divergence estimation is {KLD:.5f}.")

    # -------------------------------------------------------------------------
    # ----------------- Save the arrays as .npy files -------------------------
    # -------------------------------------------------------------------------
    np.save("data/ParamSets.npy", xx)
    np.save("data/refBME_KLD.npy", (logBME, KLD))
    np.save("data/mean.npy", np.mean(outputs['C'], axis=0))
    np.save("data/std.npy", np.std(outputs['C'], axis=0))
    np.save("data/validLikelihoods.npy", Likelihoods)

    # Save validation set
    np.save("data/validSet.npy", valid_set)
    np.save("data/validSetOutput.npy", valid_outputs['C'])

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This test shows a surrogate-assisted Bayesian calibration of a time dependent
    pollution function. Here, the noise will be jointly inferred with the input
    parameters.

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Fri Aug 9 2019

"""

import numpy as np
import pandas as pd
import joblib

# import bayesvalidrox
# Add BayesValidRox path
import sys
sys.path.append("../../src/bayesvalidrox/")

from bayesvalidrox import PyLinkForwardModel, Input, PCE, PostProcessing, BayesInference, Discrepancy, ExpDesigns, Engine


if __name__ == "__main__":

    # =====================================================
    # =============   COMPUTATIONAL MODEL  ================
    # =====================================================
    model = PyLinkForwardModel()

    # Define model options
    model.link_type = 'Function'
    model.py_file = 'pollution'
    model.name = 'pollution'

    model.output.names = ['C']

    # Synthetic data for Bayesian inversion.
    # MAP = (10, 0.07, 1.0, 30.16)
    model.observations = {}
    model.observations['Time [s]'] = [40+5*i for i in range(21)]
    model.observations['C'] = np.load("data/MeasuredData.npy")

    # For Checking with the MonteCarlo refrence
    model.mc_reference = {}
    model.mc_reference['Time [s]'] = [40+5*i for i in range(21)]
    model.mc_reference['mean'] = np.load("data/mean.npy")
    model.mc_reference['std'] = np.load("data/std.npy")

    # =====================================================
    # =========   PROBABILISTIC INPUT MODEL  ==============
    # =====================================================
    # Define the uncertain parameters with their mean and
    # standard deviation
    inputs = Input()

    # mass of pollutant spilled at each location
    inputs.add_marginals()
    inputs.marginals[0].name = 'M'
    inputs.marginals[0].dist_type = 'uniform'
    inputs.marginals[0].parameters = [7, 13]

    # diffusion rate in the channel
    inputs.add_marginals()
    inputs.marginals[1].name = 'D'
    inputs.marginals[1].dist_type = 'uniform'
    inputs.marginals[1].parameters = [0.02, 0.12]

    # location of the second spill
    inputs.add_marginals()
    inputs.marginals[2].name = 'L'
    inputs.marginals[2].dist_type = 'uniform'
    inputs.marginals[2].parameters = [0.01, 3]

    # time of the second spill
    inputs.add_marginals()
    inputs.marginals[3].name = '$\\tau$'
    inputs.marginals[3].dist_type = 'uniform'
    inputs.marginals[3].parameters = [30.01, 30.295]

    # =====================================================
    # ======  POLYNOMIAL CHAOS EXPANSION METAMODELS  ======
    # =====================================================
    meta_model = PCE(inputs)

    # Select if you want to preserve the spatial/temporal depencencies
    # meta_model.dim_red_method = 'PCA'
    # meta_model.var_pca_threshold = 99.999
    # meta_model.n_pca_components = 12

    # Select your metamodel method
    # 1) PCE (Polynomial Chaos Expansion) 2) aPCE (arbitrary PCE)
    # 3) GPE (Gaussian Process Emulator)
    meta_model.meta_model_type = 'aPCE'

    # ------------------------------------------------
    # ------------- PCE Specification ----------------
    # ------------------------------------------------
    # Select the sparse least-square minimization method for
    # the PCE coefficients calculation:
    # 1)OLS: Ordinary Least Square  2)BRR: Bayesian Ridge Regression
    # 3)LARS: Least angle regression  4)ARD: Bayesian ARD Regression
    # 5)FastARD: Fast Bayesian ARD Regression
    # 6)BCS: Bayesian Compressive Sensing
    # 7)OMP: Orthogonal Matching Pursuit
    # 8)VBL: Variational Bayesian Learning
    # 9)EBL: Emperical Bayesian Learning
    meta_model.pce_reg_method = 'VBL'

    # Bootstraping
    # 1) normal 2) fast
    meta_model.bootstrap_method = 'fast'
    meta_model.n_bootstrap_itrs = 100

    # Specify the max degree to be compared by the adaptive algorithm:
    # The degree with the lowest Leave-One-Out cross-validation (LOO)
    # error (or the highest score=1-LOO)estimator is chosen as the final
    # metamodel. pce_deg accepts degree as a scalar or a range.
    meta_model.pce_deg = 8

    # q-quasi-norm 0<q<1 (default=1)
    # meta_model.pce_q_norm = 0.75

    # Print summary of the regression results
    # meta_model.verbose = True

    # ------ Experimental Design --------
    exp_design = ExpDesigns(inputs)

    # One-shot (normal) or Sequential Adaptive (sequential) Design
    exp_design.n_init_samples = 150

    # Sampling methods
    # 1) random 2) latin_hypercube 3) sobol 4) halton 5) hammersley 6) korobov
    # 7) chebyshev(FT) 8) grid(FT) 9) nested_grid(FT) 10)user
    exp_design.sampling_method = 'latin_hypercube'

    # Sequential experimental design (needed only for sequential ExpDesign)
    exp_design.n_new_samples = 1
    exp_design.n_max_samples = 15  # 150
    exp_design.mod_loo_threshold = 1e-16

    # ------------------------------------------------
    # ------- Sequential Design configuration --------
    # ------------------------------------------------
    # meta_model.adapt_verbose = True

    # -------- Tradeoff scheme ------
    # 1) 'None' 2) 'equal' 3)'epsilon-decreasing' 4) 'adaptive'
    exp_design.tradeoff_scheme = 'epsilon-decreasing'
    # meta_model.ExpDesign.n_replication = 20

    # -------- Exploration ------
    # 1)'Voronoi' 2)'random' 3)'latin_hypercube' 4)'LOOCV' 5)'dual annealing'
    exp_design.explore_method = 'random'

    # Use when 'dual annealing' chosen
    exp_design.max_func_itr = 200

    # Use when 'Voronoi' or 'random' or 'latin_hypercube' chosen
    exp_design.n_canddidate = 5000
    exp_design.n_cand_groups = 8

    # -------- Exploitation ------
    # 1)'BayesOptDesign' 2)'BayesActDesign' 3)'VarOptDesign' 4)'alphabetic'
    # 5)'Space-filling'
    exp_design.exploit_method = 'VarOptDesign'

    # BayesOptDesign -> when data is available
    # 1)DKL (Kullback-Leibler Divergence) 2)DPP (D-Posterior-percision)
    # 3)APP (A-Posterior-percision)  # ['DKL', 'BME', 'infEntropy']
    # meta_model.ExpDesign.util_func = ['DKL', 'BME']

    # VarBasedOptDesign -> when data is not available
    # Only with Vornoi >>> 1)Entropy 2)EIGF, 3)LOOCV
    exp_design.util_func = 'Entropy'

    # alphabetic
    # 1)D-Opt (D-Optimality) 2)A-Opt (A-Optimality)
    # 3)K-Opt (K-Optimality)
    # meta_model.ExpDesign.util_func = 'D-Opt'

    # For colculation of validation error for SeqDesign
    exp_design.valid_samples = np.load("data/validSet.npy")
    exp_design.valid_model_runs = {'C': np.load("data/validSetOutput.npy")}
    exp_design.valid_likelihoods = np.load("data/validLikelihoods.npy")


    # Defining the measurement error, if it's known a priori
    obsData = pd.DataFrame(0.5*np.ones(21), columns=model.output.names)
    discrepancy = Discrepancy('')
    discrepancy.type = 'Gaussian'
    discrepancy.parameters = obsData ** 2

    # >>>>>>>>>>>>>>>>>>>>>> Build Surrogate <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # Adaptive sparse arbitrary polynomial chaos expansion
    engine = Engine(meta_model, model, exp_design)
    engine.discrepancy = discrepancy
    engine.train_normal()

    # =====================================================
    # =========  POST PROCESSING OF METAMODELS  ===========
    # =====================================================
    post = PostProcessing(engine)

    # Plot to check validation visually.
    post.valid_metamodel(n_samples=3)

    # Compute the moments and compare with the Monte-Carlo reference
    post.plot_moments()

    # Plot the sobol indices
    if meta_model.meta_model_type != 'GPE':
        total_sobol = post.sobol_indices()

    # Compute and print RMSE error
    valid_out_dict = dict()
    valid_out_dict['Time [s]'] = [40+5*i for i in range(21)]
    valid_out_dict['C'] = np.load("data/validSetOutput.npy")
    post.check_accuracy(samples=engine.exp_design.valid_samples,
                           outputs=valid_out_dict)

    # =====================================================
    # ========  Bayesian inference with Emulator ==========
    # =====================================================
    bayes = BayesInference(engine)
    bayes.use_emulator = True
    bayes.n_prior_samples = 100

    # Select the inference method
    bayes.inference_method = "MCMC"
    bayes.mcmc_params = {
        'n_steps': 1e5,
        'n_walkers': 30,
        'multiprocessing': False,
        'verbose': False
        }

    bayes.plot = True
    bayes.discrepancy = discrepancy
    posterior = bayes.run_inference()

    # Save class objects
    with open(f'Bayes_{model.name}.pkl', 'wb') as output:
        joblib.dump(bayes, output, 2)

# -*- coding: utf-8 -*-
"""
Runs the umbridge command for the tsunami model.
"""
import umbridge
import numpy as np

def tsunami_model(params):
    model = umbridge.HTTPModel('http://localhost:4242', 'forward')
    out = np.array(model(np.ndarray.tolist(params), {'level':0}))
    
    return {'T1':out[:,0], 'T2':out[:,1], 'H1':out[:,2], 'H2':out[:,3], 
            'x_values':[0]}
    
    
    
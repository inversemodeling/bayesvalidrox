# -*- coding: utf-8 -*-
"""
Example and test for using UM-Bridge with bayesvalidrox.
Example model is the tsunami model described here:
    https://um-bridge-benchmarks.readthedocs.io/en/docs/inverse-benchmarks/exahype-tsunami.html
It needs to run separately while starting this example. 
    docker run -it -p 4242:4242 -v ~/tsunami_output:/output linusseelinger/model-exahype-tsunami
    
This example uses models that are exmplicitly defined as functions outside of
PyLink. An example for the link_type 'umbdridge' can be found in 
`example_umbridge_testmodel.py`.

"""

import copy
import joblib
import numpy as np 
import pandas as pd

from bayesvalidrox import PCE, PostProcessing, Engine, ExpDesigns, Input, PyLinkForwardModel, Discrepancy, BayesInference,BayesModelComparison
from tsunami_model import tsunami_model

if __name__ == '__main__':
    # This model has 2 inputs and four outputs
    n_prior_sample = 1000
    inputs = Input()
    inputs.add_marginals()
    inputs.marginals[0].name = 'x'
    inputs.marginals[0].input_data = np.random.uniform(50,150,n_prior_sample)  
    inputs.add_marginals()
    inputs.marginals[1].name = 'y'
    inputs.marginals[1].input_data = np.random.uniform(50,150,n_prior_sample)
    
    # Define the model - level 0
    model0 = PyLinkForwardModel()
    model0.link_type = 'function'
    model0.py_file = 'tsunami_model'
    model0.name = 'tsunami_model'
    model0.output.names = ['T1', 'T2', 'H1', 'H2']
    
    # Define the model - level 1
    model1 = PyLinkForwardModel()
    model1.link_type = 'function'
    model1.py_file = 'tsunami_model1'
    model1.name = 'tsunami_model1'
    model1.output.names = ['T1', 'T2', 'H1', 'H2']
    
    # Create the surrogate
    meta_model0 = PCE(inputs, model0)
    
    # Select your metamodel method
    meta_model0.meta_model_type = 'aPCE'
    meta_model0.pce_reg_method = 'FastARD'
    meta_model0.pce_deg = np.arange(1, 5)
    meta_model0.pce_q_norm = 0.4#1.0

    # Define ExpDesign - this is the same for both models       
    exp_design0 = ExpDesigns(inputs)
    exp_design0.method = 'normal'
    exp_design0.n_init_samples = 50
    exp_design0.sampling_method = 'latin-hypercube'
    exp_design1 = copy.deepcopy(exp_design0)
    
    # Start and run the engine
    engine0 = Engine(meta_model0, model0, exp_design0)
    engine0.start_engine()
    engine0.train_normal(parallel = False)
    print('Surrogate 0 completed')
    print('')
    
    # Create the surrogate
    meta_model1 = PCE(inputs, model1)
    
    # Select your metamodel method
    meta_model1.meta_model_type = 'aPCE'
    meta_model1.pce_reg_method = 'FastARD'
    meta_model1.pce_deg = np.arange(1, 5)
    meta_model1.pce_q_norm = 0.4#1.0

    # Start and run the engine
    engine1 = Engine(meta_model1, model1, exp_design1)
    engine1.start_engine()
    engine1.train_normal(parallel = False)
    print('Surrogate 1 completed')
    print('')

    # Save surrogate
    with open('tsunami0.pk1', 'wb') as output:
        joblib.dump(engine0, output, 2)
        
    # Save surrogate
    with open('tsunami1.pk1', 'wb') as output:
        joblib.dump(engine1, output, 2)
    
    # Post processing on model 1
    post = PostProcessing(engine1)
    post.plot_moments(plot_type='line')
    # Plot to check validation visually.
    post.valid_metamodel(n_samples=1)
    
    # Compute and print RMSE error
    post.check_accuracy(n_samples=300)
    total_sobol = post.sobol_indices()

    # Get reference evaluation as 'true data'
    true_data = tsunami_model(np.array([[90.0,60.0]]))
    model0.observations = true_data
    model1.observations = true_data
    true_data_nox = copy.deepcopy(true_data)
    true_data_nox.pop('x_values')
    
    # Direct surrogate evaluation
    mean, std = engine1.eval_metamodel(np.array([[90.0,60.0]]))
    
    # Bayesian Inference
    # Set uncertainty
    obsData = pd.DataFrame(true_data_nox, columns=model0.output.names)
    discrepancy = Discrepancy('')
    discrepancy.type = 'Gaussian'
    discrepancy.parameters = (obsData*0.15)**2
    
    # Parameter estimation / single model validation via TOM for model 1
    BayesOpts = BayesInference(engine1)
    BayesOpts.use_emulator= True
    BayesOpts.discrepancy = discrepancy
    BayesOpts.bootstrap_method = 'normal'
    BayesOpts.n_bootstrap_itrs = 10
    BayesOpts.bootstrap_noise = 0.05
    BayesOpts.out_dir = ''
    
    # Set the MCMC parameters
    import emcee
    BayesOpts.inference_method = "MCMC"
    BayesOpts.mcmc_params = {
        'n_steps': 1e3,
        'n_walkers': 30,
        'moves': emcee.moves.KDEMove(),
        'multiprocessing': False,
        'verbose': False
        }
     
    Bayes = BayesOpts.run_inference()
    
    # Pkl the inference results
    with open(f'Bayes_{model0.name}.pkl', 'wb') as output:
        joblib.dump(Bayes, output, 2)
            
    # Model Comparison
    # Set the models
    meta_models = {
        "M0": engine0,
        "M1": engine1,
        }

    # BME Bootstrap options
    opts_bootstrap = {
        "bootstrap": True,
        "n_samples": 1000,
        "Discrepancy": discrepancy,
        "emulator": True,
        "plot_post_pred": False
        }

    # Run model comparison
    bmc = BayesModelComparison(meta_models,opts_bootstrap)
    output_dict = bmc.create_model_comparison()
            
    # save model comparison results
    with open('ModelComparison.pkl', 'wb') as f:
        joblib.dump(bmc, f)
    
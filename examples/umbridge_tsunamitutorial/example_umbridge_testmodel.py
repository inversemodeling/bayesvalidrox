# -*- coding: utf-8 -*-
"""
Example and test for using UM-Bridge with bayesvalidrox.
This example uses the `testmodel` and sets the model type explicitly as 
UM-Bridge for the pylink model.

An example for using UM-Bridge with an explicitly defined model can be found in 
`example_umbridge_tsunamimodel.py`

"""

import joblib
import numpy as np 

from bayesvalidrox import PyLinkForwardModel, Input, PCE, PostProcessing, Engine, ExpDesigns


if __name__ == '__main__':
    
    # This model has 2 inputs and four outputs
    n_prior_sample = 1000
    inputs = Input()
    inputs.add_marginals()
    inputs.marginals[0].name = 'x'
    inputs.marginals[0].input_data = np.random.uniform(50,150,n_prior_sample)  
    
    # Define the model directly via Pylink
    model = PyLinkForwardModel()
    model.link_type = 'umbridge'
    model.host = 'http://testmodel.linusseelinger.de'
    model.modelparams = {}
    model.name = 'testmodel'
    model.output.names = ['y']
    model.x_values = np.array([0])
    
    # Create the surrogate
    meta_model = PCE(inputs)
    
    # Select the surrogate type and properties
    meta_model.meta_model_type = 'aPCE'
    meta_model.pce_reg_method = 'FastARD'
    meta_model.pce_deg = np.arange(1, 5)
    meta_model.pce_q_norm = 0.4

    # Define the experimental design and sampling methods
    exp_design = ExpDesigns(inputs)     
    exp_design.n_init_samples = 50
    exp_design.sampling_method = 'latin-hypercube'
    
    # Start and run the engine
    engine = Engine(meta_model, model, exp_design)
    engine.start_engine()
    engine.train_normal(parallel = False)
    print('Surrogate completed')
    print('')

    # Save surrogate
    with open('testmodel.pk1', 'wb') as output:
        joblib.dump(engine, output, 2)
        
    # Post processing
    post = PostProcessing(engine)
    post.plot_moments(plot_type='line')
    
    # Plot to check validation visually.
    post.valid_metamodel(n_samples=1)
    
    # Compute and print RMSE error
    post.check_accuracy(n_samples=300)
    total_sobol = post.sobol_indices()

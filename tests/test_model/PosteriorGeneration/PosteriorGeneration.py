#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 2019

This script generates the posterior distribution of the CO2Benchmark model
for a synthetic data for three given uncertain parameters using 10000 MC-Simulations
of the original model.

@author: Farid Mohammadi
"""
import pandas as pd
import numpy as np
import os
from tqdm import tqdm
import corner
from scipy.stats import multivariate_normal
import scipy.stats as st




def BeamModel(theta):
        #### write an input file
        global beam_span, i
        Beam_width, Beam_height, Youngs_modulus,Uniform_load = theta
        Youngs_modulus = 1e9 * Youngs_modulus
        Uniform_load = 1000 * Uniform_load
        # Get the number of function runs each 100 NOT necessary, just checking
        i +=1 
        if i%100 ==0:
            print("number of evaluations {}".format(i))
        
        #### Run your surrogate model here.
        I = Beam_width * Beam_height**3 / 12;
        output = np.zeros((11))
        for i in range(1,10):
            X = (i/10.)*beam_span;
            output[i] = -1*Uniform_load*X*(beam_span**3 -2* X**2 * beam_span + X**3)/(24*Youngs_modulus*I);

        return output


def RejectionSampling(mcParametersets, origModelOutput, Data, InputNames):
    
    
    MCSamples = mcParametersets
    NofMeasurements = len(Data)
    NrofSamples = len(MCSamples)
    Sigma2 = (0.01*synthData)**2
    Sigma2[0], Sigma2[-1]= 1e-10, 1e-10
    
    # Covariance Matrix 
    covMatrix = np.zeros((NofMeasurements, NofMeasurements), float)
    np.fill_diagonal(covMatrix, Sigma2)

    # Likelihood
    Likelihoods = multivariate_normal.pdf(origModelOutput, mean=Data, cov=covMatrix)

    # Rejection
    # Take the first column of Likelihoods (Observation data without noise)
    NormLikehoods = Likelihoods / np.max(Likelihoods)
    
    
    # Random numbers between 0 and 1
    unif = np.random.rand(1, NrofSamples)[0]
    
    # Reject the poorly performed prior
    acceptedSamples = MCSamples[NormLikehoods >= unif]
    
    # Output the Posterior
        
    Posterior_df = pd.DataFrame(acceptedSamples, columns=InputNames)
    
    # Save the posterior data frame
    Posterior_df.to_csv('./Posterior.csv', columns=InputNames, index=False)
    
    return Posterior_df

def posteriorPlot(Posterior, MAP, InputNames, nParams, figsize=(10,10)):
    
    figPosterior = corner.corner(Posterior, labels=InputNames,
                       show_titles=True, title_kwargs={"fontsize": 12})

    # This is the true mean of the second mode that we used above:
    value1 = MAP
    
    # This is the empirical mean of the sample:
    value2 = np.mean(Posterior, axis=0)

    # Extract the axes
    axes = np.array(figPosterior.axes).reshape((nParams, nParams))
    
    # Loop over the diagonal
    for i in range(nParams):
        ax = axes[i, i]
        ax.axvline(value1[i], color="g")
        ax.axvline(value2[i], ls='--', color="r")
    
    # Loop over the histograms
    for yi in range(nParams):
        for xi in range(yi):
            ax = axes[yi, xi]
            ax.axvline(value1[xi], color="g")
            ax.axvline(value2[xi], ls='--', color="r")
            ax.axhline(value1[yi], color="g")
            ax.axhline(value2[yi], ls='--', color="r")
            ax.plot(value1[xi], value1[yi], "sg")
            ax.plot(value2[xi], value2[yi], "sr")
    
    figPosterior.set_size_inches(figsize) 
    figPosterior.savefig('Posterior.svg',bbox_inches='tight')
    
    return



if __name__ == '__main__':
    
    # --------------------------------------
    # ---------- Initialization -----------
    # --------------------------------------
    np.random.seed(42)
    beam_span = 5
    i = 0    
    
    InputNames = ['Beam width', 'Beam height', 'Youngs modulus', 'Uniform load']
    
    # --------------------------------------
    # ---------- Data generation -----------
    # --------------------------------------
    
    xdata = np.linspace(0, 5, num=11, endpoint=True)
    theta_true = (0.150064, 0.299698, 30.763206, 10.164872)
    model_true = BeamModel(theta_true)
    synthData =  model_true
    
    #plt.plot(xdata, synthData, 'ok')
    #plt.xlabel('x')
    #plt.ylabel('y');
    
    # -------------------------------------------------------------------------
    # ------------------ Naive Monte-Carlo with rejection sampling ------------
    # -------------------------------------------------------------------------
    ndim = 4
    mcSize = 500000
    nofmeasurement = 11
    Likelihood = np.zeros((mcSize))
    mcParametersets = np.zeros((mcSize,ndim))
    
    # Beam_width 
    mcParametersets[:,0] = st.lognorm(s=0.0075, scale=0.15).rvs(size=mcSize)
    # Beam_height 
    mcParametersets[:,1] = st.lognorm(s=0.015, scale=0.30).rvs(size=mcSize)
    #Youngs_modulus 
    mcParametersets[:,2] = st.norm(loc=30, scale=4.5).rvs(size=mcSize)
    #Uniform_load
    mcParametersets[:,3] = st.norm(loc=10, scale=2).rvs(size=mcSize)
    #sigma
    #mcParametersets[:,4] = st.halfcauchy(loc=0, scale = 10).rvs(size=mcSize)
    
    # origModelOutput
    origModelOutput = np.zeros((mcSize, nofmeasurement))
    for idx, theta in enumerate(mcParametersets):
        origModelOutput[idx] = BeamModel(theta)
    
    
    # ------- Generate & plot the posterior -------
    # Rejection sampling
    Posterior = RejectionSampling(mcParametersets, origModelOutput, synthData, InputNames)
    
    # Plot the posterior distributions
    posteriorPlot(Posterior, theta_true, InputNames, nParams=ndim)

